import { Font, AppLoading } from "expo";
import React, { Component } from "react";
import { StyleProvider } from "native-base";
import App from "./src/AppNavigator";
import getTheme from "./src/theme/components";
import variables from "./src/theme/variables/commonColor";
import PushNotification from "./src/components/NotificationComponent";
import { Text, View } from "react-native";
import registerForPushNotificationsAsync from "./src/FCM_Notifications/index";
//import { Notifications } from "expo";

export default class Root extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      isReady: false,
      //notification: {}
    };
  }
  async componentWillMount() {
    await Font.loadAsync({
      aller_bold: require("./Fonts/aller_bold.ttf"),
      aller_bolditalic: require("./Fonts/aller_bolditalic.ttf"),
      aller_display: require("./Fonts/aller_display.ttf"),
      aller_italic: require("./Fonts/aller_italic.ttf"),
      aller_light: require("./Fonts/aller_light.ttf"),
      aller_lightitalic: require("./Fonts/aller_lightitalic.ttf"),
      aller_reg: require("./Fonts/aller_reg.ttf"),
      // Bariol_Regular: require("../../../Fonts/Bariol_Regular.ttf"),
      // Bariol_Regular_Italic: require("../../../Fonts/Bariol_Regular_Italic.ttf"),
      // din_bold: require("../../../Fonts/din_bold.ttf"),
      din_bold: require("./Fonts/din_bold.ttf")
      // din_heavy: require("../../../Fonts/din_heavy.ttf"),
      // din_light: require("../../../Fonts/din_light.ttf"),
      // din_medium: require("../../../Fonts/din_medium.ttf"),
      // din_regular: require("../../../Fonts/din_regular.ttf")
    });
    this.setState({ isReady: true });
    //registerForPushNotificationsAsync();
  }

 // componentDidMount() {
   // registerForPushNotificationsAsync();
    //this._notificationSubscription = Notifications.addListener(
      //this._handleNotification
    //);
  //}

  //_handleNotification = notification => {
    //this.setState({ notification: notification });
  //};

render() {
    //console.log("My Notification is", this.state.notification.origin);
    //console.log(
    //"My Notification is",
    //JSON.stringify(this.state.notification.data)
    //);
    const { isReady } = this.state;
    return isReady
      ? <StyleProvider style={getTheme(variables)}>
          <App />
        </StyleProvider>
      : <AppLoading />;
  }
}
