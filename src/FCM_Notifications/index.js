import { Permissions, Notifications } from "expo";
import { Store } from "../redux/store";

const PUSH_ENDPOINT = "http://www.clicky.pk/api/AppToken/";
export const NOTIFICATION_TOKEN = "NOTIFICATION_TOKEN";

export default async function registerForPushNotificationsAsync() {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;

  // only ask if permissions have not already been determined, because
  // iOS won't necessarily prompt the user a second time.
  if (existingStatus !== "granted") {
    // Android remote notification permissions are granted during the app
    // install, so this will only ask on iOS
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  // Stop here if the user did not grant permissions
  if (finalStatus !== "granted") {
    return;
  }

  // Get the token that uniquely identifies this device
  let token = await Notifications.getExpoPushTokenAsync();

  //console.log("My Token is", token.substring(17));
  Store.dispatch({
    type: NOTIFICATION_TOKEN,
    payload: { notificationToken: token }
  });

  // POST the token to your backend server from where you can retrieve it to send push notifications.
  return fetch(PUSH_ENDPOINT, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization:
        "Basic " +
        "YWRtaW5AYWxuYXNzZXIucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    },
    body: JSON.stringify({
      devicetoken: token.substring(17),
      device_type: "app"
    })
  })
    .then(res => res.json())
    .then(json => {
      //console.log('My Token addedd successfully',json)
      if(json.msg === "_token_added"){
        console.log('My Token addedd successfully')
      }
    })
    .catch(error => {
      console.log("Signup Error: ", error);
    });
}
