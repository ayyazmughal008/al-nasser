import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import reducer from "./reducer";
import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import {
  fetchHomePage,
  loadCategoryListing,
  fetchCategoryUrl,
  LOAD_PRODUCTS,
  LOAD_HOMEPAGE,
  loadHomePage
} from "./actions";

const persistConfig = {
  key: "root",
  storage
};

const persistedReducer = persistReducer(persistConfig, reducer);
export const Store = createStore(persistedReducer, applyMiddleware(thunk));
export const persistor = persistStore(Store);

//const categoryUrlPrefix = "https://www.alnasser.pk/api/Extendedproducts/?cid=1217";
//const middleware = applyMiddleware(thunk);
//const store = createStore(reducer, middleware);
Store.dispatch(dispatch => {
  fetchHomePage().then(res => {
    dispatch(loadHomePage(res));
  });
});

export const loadMoreProducts = url => {
  Store.dispatch(dispatch => {
    fetchCategoryUrl(url).then(res => {
      dispatch(loadCategoryListing(res));
    });
  });
};

//export default Store;
