import { combineReducers } from "redux";
import {
  LOAD_HOMEPAGE,
  LOAD_PRODUCTS,
  UPDATE_BROWSE_POSITION,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILED,
  UPDATE_USER_GID,
  USER_FETCH_ORDERS,
  USER__ORDERS_DETAIL,
  USER_FETCH_PROFILE,
  USER_FETCH_CART,
  USER_SIGNUP_SUCCESS,
  USER_SIGNUP_FAILED,
  SHOW_SCREEN_LOADER,
  FETCH_PAYMENT_METHODS,
  HIDE_SCREEN_LOADER,
  USER_CHECKOUT_COMPLETE,
  USER_CHECKOUT_STARTED,
  FETCH_POPALR_SEARCH_METHODS,
  USER_LAST_ORDER,
  CHANGE_ADDRESS_USER_INFO,
  FETCH_WISHLIST_DETAIL,
  LOG_ME_OUT,
  WISHLIST_ITEMS,
  HOMEPAGE_CATEGORY_BANNER,
  SCREEN_LOADER,
  SCREEN_LOADER_HIDE,
  FETCH_ENCODED_VALUES,
  FETCH_SHIPPING_CHARGES,
  FETCH_NOTIFICATION,
} from "./actions";

import {NOTIFICATION_TOKEN} from '../FCM_Notifications/index' 

const initialUserState = {
  isLoggedIn: false,
  loginError: null,
  signupError: null,
  userId: null,
  gId: null,
  profileId: null,
  cartItems: {},
  customerEmail: null,
  firstName: null,
  lastName: null,
  email: null,
  customerPicUrl: null,
  wishListLength: null,
  wishList: {},
  userOrders: null,
  orderDetail: null,
  orders_qty: null,
  userHomeAddress: null,
  userOfficeAddress: null,
  wishList: null,
  checkOutStarted: false,
  lastOrder: null,
  wishItems: null,
  profileDP:null,
  userProfile:null,
  shippingCharges:null,
};
const appStateReducer = (state = { showScreenLoader: false }, action) => {
  if (action.type === SHOW_SCREEN_LOADER) {
    console.log("Setting screen loader to true");
    return {
      ...state,
      showScreenLoader: true
    };
  }
  if (action.type === HIDE_SCREEN_LOADER) {
    console.log("Setting screen loader to false");
    return {
      ...state,
      showScreenLoader: false
    };
  }
  return state;
};
const loginstatereducer = (state = { showLoader: false }, action) => {
  if (action.type === SCREEN_LOADER) {
    console.log("Setting screen loader to true");
    return {
      ...state,
      showLoader: true
    };
  }
  if (action.type === SCREEN_LOADER_HIDE) {
    console.log("Setting screen loader to false");
    return {
      ...state,
      showLoader: false
    };
  }
  return state;
};

const homePageDataReducer = (state = {}, action) => {
  if (action.type === LOAD_HOMEPAGE) {
    state = {
      ...state,
      appVersion: action.payload.appVersion,
      categoryicons: action.payload.categories,
      bannerslider: action.payload.bannerslider,
      minibanner: action.payload.minibanner,
      sliderwidget: action.payload.sliderwidget,
      subCategories: action.payload.subCategories,
      isLoaded: action.payload.isLoaded
    };
    return state;
  }

  if(action.type === HOMEPAGE_CATEGORY_BANNER){
    state = {
      ...state,
      topSlider: action.payload.topSlider,
      bottomBanner: action.payload.bottomBanner,
    };
    return state;
  }

  return state;
};
const productsReducer = (
  state = { productList: [], isLoaded: false, isLoadingMore: false },
  action
) => {
  if (action.type === LOAD_PRODUCTS)
    return {
      ...state,
      productList: action.payload.products,
      isLoaded: action.payload.isLoaded
      // isLoadingMore: action.payload.isLoadingMore
    };
  return state;
};
const browsePositionReducer = (
  state = {
    categoryId: 4,
    currentPage: 0,
    isCategoryListingPage: true,
    topCategoryId: 4
  },
  action
) => {
  if (action.type === UPDATE_BROWSE_POSITION) {
    return {
      ...state,
      topCategoryId: action.payload.topCategoryId,
      category: action.payload.category,
      categoryId: action.payload.categoryId,
      isCategoryListingPage: action.payload.isCategoryListingPage,
      currentPage: action.payload.currentPage
    };
  }
  return state;
};
const userReducer = (state = initialUserState, action) => {

  if (action.type === NOTIFICATION_TOKEN) {
    return {
      ...state,
      notificationToken: action.payload.notificationToken,
    };
  }

  if (action.type === USER_FETCH_PROFILE) {
    return {
      ...state,
      userHomeAddress: action.payload.homeAddress,
      userOfficeAddress: action.payload.officeAddress,
      userProfile: action.payload.userProfile
    };
  }
  if (action.type === FETCH_SHIPPING_CHARGES) {
    return {
      ...state,
      shippingCharges: action.payload.shippingCharges,
    };
  }
  if (action.type === LOG_ME_OUT) {
    return {
      ...state,
      isLoggedIn: false,
      loginError: null,
      signupError: null,
      userId: null,
      gId: null,
      profileId: null, 
      cartItems: {},
      customerEmail: null,
      firstName: null,
      lastName: null,
      email: null,
      customerPicUrl: null,
      wishList: {},
      userOrders: null,
      orderDetail: null,
      orders_qty: null,
      userHomeAddress: null,
      wishList: null,
      userOfficeAddress: null,
      wishListLength: null,
      checkOutStarted: false,
      lastOrder: null,
      profileDP:null,
      userProfile:null,
      shippingCharges:null,
    };
  }
  if (action.type === CHANGE_ADDRESS_USER_INFO) {
    return {
      ...state,
      firstName: action.payload.firstName,
      lastName: action.payload.lastName
    };
  }
  if (action.type === USER_FETCH_ORDERS) {
    return {
      ...state,
      userOrders: action.payload.orders,
      orders_qty: action.payload.orders_qty
    };
  }
  if (action.type === USER__ORDERS_DETAIL) {
    return {
      ...state,
      orderDetail: action.payload.ordersDetail,
    };
  }
  if (action.type === FETCH_WISHLIST_DETAIL) {
    return {
      ...state,
      wishList: action.payload.wishList,
      wishListLength: action.payload.wishListLength,
    };
  }
  if (action.type === USER_LOGIN_SUCCESS) {
    return {
      ...state,
      isLoggedIn: true,
      email: action.payload.email,
      firstName: action.payload.firstName,
      lastName: action.payload.lastName,
      profileDP: action.payload.profileDP,
      userId: action.payload.userId
    };
  }
  if (action.type === USER_LOGIN_FAILED) {
    return {
      ...state,
      isLoggedIn: false,
      loginError: action.payload.loginError
    };
  }
  if (action.type === USER_FETCH_CART) {
    console.log("Reducer Action:", USER_FETCH_CART);
    return {
      ...state,
      cartItems: action.payload
    };
  }
  if (action.type === UPDATE_USER_GID) {
    return {
      ...state,
      gId: action.payload.gid
    };
  }
  if (action.type === USER_SIGNUP_SUCCESS) {
    return {
      ...state,
      isLoggedIn: true,
      userId: action.payload.userId
    };
  }
  if (action.type === USER_SIGNUP_FAILED) {
    return {
      ...state,
      signupError: action.payload.error
    };
  }
  if (
    action.type === USER_CHECKOUT_STARTED ||
    action.type === USER_CHECKOUT_COMPLETE
  ) {
    return {
      ...state,
      checkOutStarted: action.payload.checkout_state
    };
  }
  if (action.type === USER_LAST_ORDER) {
    return {
      ...state,
      lastOrder: action.payload
    };
  }
  if (action.type === FETCH_ENCODED_VALUES) {
    return {
      ...state,
      encodedValue: action.payload.encodedValue,
    };
  }
  if (action.type === FETCH_NOTIFICATION) {
    return {
      ...state,
      notifications: action.payload.notifications,
    };
  }
  
  return state;
};
const paymentsReducer = (
  state = { recommendedMethods: null, otherMethods: null },
  action
) => {
  if (action.type === FETCH_PAYMENT_METHODS) {
    return {
      ...state,
      recommendedMethods: action.payload.recommended,
      otherMethods: action.payload.other
    };
  }
  
  return state;
};

const reducer = combineReducers({
  homepage: homePageDataReducer,
  products: productsReducer,
  browsePosition: browsePositionReducer,
  user: userReducer,
  appState: appStateReducer,
  payments: paymentsReducer,
  loader: loginstatereducer
});
export default reducer;
