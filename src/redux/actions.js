import { Store } from "./store";
import NavigationService from "../NavigationService";
import { Dimensions, Alert } from "react-native";
import base64 from "base-64";
import { Facebook } from "expo";
import ToastModal from "../components/ToastModal";
export const LOAD_HOMEPAGE = "LOAD_HOMEPAGE";
export const LOAD_PRODUCTS = "LOAD_PRODUCTS";
export const UPDATE_BROWSE_POSITION = "UPDATE_BROWSE_POSITION";
export const USER_LOGIN_SUCCESS = "USER_LOGIN_SUCCESS";
export const USER_LOGIN_FAILED = "USER_LOGIN_FAILED";
export const USER_SIGNUP_SUCCESS = "USER_SIGNUP_SUCCESS";
export const USER_SIGNUP_FAILED = "USER_SIGNUP_FAILED";
export const USER_FETCH_CART = "USER_FETCH_CART";
export const UPDATE_USER_GID = "UPDATE_USER_GID";
export const SHOW_SCREEN_LOADER = "SHOW_SCREEN_LOADER";
export const HIDE_SCREEN_LOADER = "HIDE_SCREEN_LOADER";
export const SCREEN_LOADER_HIDE = "SCREEN_LOADER_HIDE";
export const SCREEN_LOADER = "SCREEN_LOADER";
export const USER_FETCH_ORDERS = "USER_FETCH_ORDERS";
export const USER__ORDERS_DETAIL = "USER__ORDERS_DETAIL";
export const USER_FETCH_PROFILE = "USER_FETCH_PROFILE";
export const FETCH_PAYMENT_METHODS = "FETCH_PAYMENT_METHODS";
export const USER_CHECKOUT_STARTED = "USER_CHECKOUT_STARTED";
export const USER_CHECKOUT_COMPLETE = "USER_CHECKOUT_COMPLETE";
export const USER_LAST_ORDER = "USER_LAST_ORDER";
export const CHANGE_ADDRESS_USER_INFO = "CHANGE_ADDRESS_USER_INFO";
export const LOG_ME_OUT = "DESTORING_YOU_ALL";
export const WISHLIST_ITEMS = "WISHLIST_ITEMS";
export const FETCH_WISHLIST_DETAIL = "FETCH_WISHLIST_DETAIL";
export const HOMEPAGE_CATEGORY_BANNER = "HOMEPAGE_CATEGORY_BANNER";
export const FETCH_ENCODED_VALUES = "FETCH_ENCODED_VALUES";
export const FETCH_SHIPPING_CHARGES = "FETCH_SHIPPING_CHARGES";
export const FETCH_NOTIFICATION = "FETCH_NOTIFICATION";

const deviceWidth = Dimensions.get("window").width;
const headers = new Headers();
headers.append(
  "Authorization",
  "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");
const homeURL = "https://www.alnasser.pk/api/homepage1";

export function fetchPaymentMethods() {
  return dispatch => {
    fetchUrl = "https://www.alnasser.pk/api/payments?status=A&for_app=1";
    fetch(fetchUrl, { headers: headers })
      .then(res => res.json())
      .then(json => {
        //console.log(json);
        dispatch({
          type: FETCH_PAYMENT_METHODS,
          payload: {
            recommended: json.payments.filter(
              payment => payment.recommended === "1"
            ),
            other: json.payments.filter(payment => payment.recommended === "0")
          }
        });
      })
      .catch(error => {
        console.log("Fetch Payment Error: ", error);
      });
  };
}

export const Logout = () => {
  Store.dispatch({
    type: LOG_ME_OUT
  });
};

export const setCheckOutStarted = value => {
  //Value === ture  ... checkoutstarted
  //Value === false ... nocheckout/checkoutcomplete
  action_type = null;
  value === true
    ? (action_type = USER_CHECKOUT_STARTED)
    : (action_type = USER_CHECKOUT_COMPLETE);
  Store.dispatch({
    type: action_type,
    payload: {
      checkout_state: value
    }
  });
};

function fetchUserProfile(userId) {
  if (userId == null || userId === undefined) return;
  fetchProfileUrl = `https://www.alnasser.pk/api//MultipleProfiles/?user_id=${userId}`;
  return dispatch => {
    fetch(fetchProfileUrl, {
      headers: {
        method: "GET",
        Authorization: "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg=",
        "Cache-Control": "no-cache"
      }
    })
      .then(res => res.json())
      .then(json => {
        // console.log("User Home Profile: \n", json.users.filter(address=> address.profile_name === 'Home Address'))
        dispatch({
          type: USER_FETCH_PROFILE,
          payload: {
            homeAddress: json.users.filter(
              address => address.profile_name === "Home Address"
            ),
            officeAddress: json.users.filter(
              address => address.profile_name === "Office Address"
            ),
            userProfile: json.users[0]
          }
        });
      });
  };
}

export const fetchShippingCharges = (
  profileId,
  firstName,
  lastName,
  city,
  phone,
  state,
  address,
  userId
) => {
  Store.dispatch({ type: SCREEN_LOADER });
  const orderUrl = `https://www.alnasser.pk/api/Extendedcheckout/${userId}`;
  console.log(
    "Going to fetch url",
    orderUrl,
    profileId,
    city,
    phone,
    state,
    address
  );
  fetch(orderUrl, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    },
    body: JSON.stringify({
      company_id: "0",
      user_type: "C",
      view: "checkout",
      gc: "",
      profile_id: profileId,
      update_user_data: "Y",
      ship_to_another: 0,
      user_data: {
        b_firstname: firstName,
        b_lastname: lastName,
        b_phone: phone.substring(3),
        b_address: address,
        b_city: city,
        b_state: state.charAt(0),
        b_zipcode: "57400",
        b_country: "PK"
      }
    })
  })
    .then(res => res.json())
    .then(json => {
      Store.dispatch({ type: SCREEN_LOADER_HIDE });
      Store.dispatch({
        type: FETCH_SHIPPING_CHARGES,
        payload: {
          shippingCharges: json.all_shippings.total_shipping_cost
        }
      });
    });
};

export const fetchUserOrders = (userId, pageNo = 1) => {
  if (!userId) return;
  const orderUrl = `https://www.alnasser.pk/api/extendedorders/?user_id=${userId}&page=${pageNo};`;
  console.log("fetch User Order API", orderUrl);
  return dispatch => {
    fetch(orderUrl, {
      headers: {
        method: "GET",
        //Authorization: "Basic "+ "YWRtaW5AYWxuYXNzZXIucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg=",
        Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
      }
    })
      .then(res => res.json())
      .then(json => {
        dispatch({
          type: USER_FETCH_ORDERS,
          payload: { orders: json.orders, orders_qty: json.params.total_items }
        });
      });
  };
};

export const fetchEncodedValues = (amount, orderId, email, phone) => {
  Store.dispatch({ type: SCREEN_LOADER });
  const encoderUrl = `http://www.clicky.pk/api/EnDe?orderRefNum=${orderId}&amount=${amount}&emailAddr=${email}&mobileNum=${phone}`;
  console.log("fetch encoded values", encoderUrl);

  fetch(encoderUrl, {
    headers: {
      method: "GET",
      //Authorization: "Basic "+ "YWRtaW5AYWxuYXNzZXIucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg=",
      Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    }
  })
    .then(res => res.json())
    .then(json => {
      Store.dispatch({ type: SCREEN_LOADER_HIDE });
      Store.dispatch({
        type: FETCH_ENCODED_VALUES,
        payload: { encodedValue: json }
      });
    });
};

export const fetchNotification = () => {
  Store.dispatch({ type: SCREEN_LOADER });
  const notificationUrl = `https://www.alnasser.pk/api/Notification`;
  console.log("fetch Notification Data", notificationUrl);
  fetch(notificationUrl, {
    headers: {
      method: "GET",
      Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    }
  })
    .then(res => res.json())
    .then(json => {
      Store.dispatch({ type: SCREEN_LOADER_HIDE });
      console.log(json);
      let obj = json;
      var result = Object.values(obj);
      Store.dispatch({
        type: FETCH_NOTIFICATION,
        payload: { notifications: result }
      });
    });
};

export const fetchUserOrderswithoutAnyDispatch = async (userId, pageNo) => {
  if (!userId) return;
  const orderUrl = `https://www.alnasser.pk/api/extendedorders/?user_id=${userId}&page=${pageNo};`;
  console.log("fetch User Order API", orderUrl);
  let orders = [];
  let orders_qty = 0;
  await fetch(orderUrl, {
    headers: {
      method: "GET",
      //Authorization: "Basic "+ "YWRtaW5AYWxuYXNzZXIucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg=",
      Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    }
  })
    .then(res => res.json())
    .then(json => {
      orders = json.orders;
      orders_qty = json.params.total_items;
    });
  return {
    orders: orders,
    orders_qty: orders_qty
  };
};

export const fetchOrdersDetail = OrderId => {
  fetchProfileUrl = `https://www.alnasser.pk/api/extendedorders/${OrderId}`;
  console.log("going to fetch URL => \n", fetchProfileUrl);
  fetch(fetchProfileUrl, {
    headers: {
      method: "GET",
      Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    }
  })
    .then(res => res.json())
    .then(json => {
      console.log("User Orders: \n", json);
      Store.dispatch({
        type: USER__ORDERS_DETAIL,
        payload: { ordersDetail: json }
      });
    });
};

export const fetchCategoryBanner = catId => {
  fetchCatBanner = `https://www.alnasser.pk/api/ExtendedBanners1?position=category&cat_id=${catId}`;
  console.log("going to fetch URL => \n", fetchCatBanner);
  fetch(fetchCatBanner, {
    headers: {
      method: "GET",
      Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    }
  })
    .then(res => res.json())
    .then(json => {
      //console.log("User Orders: \n", json);
      Store.dispatch({
        type: HOMEPAGE_CATEGORY_BANNER,
        payload: { topSlider: json.top_slider, bottomBanner: json.banners }
      });
    });
};

export function fetchWishListData(userId, gId) {
  if (userId) {
    fetchWishList = `https://www.alnasser.pk/api/Getwishlistproduct/?user_id=${userId}`;
  } else if (gId) {
    fetchWishList = `https://www.alnasser.pk/api/Getwishlistproduct/?user_id=${gId}`;
  } else {
    fetchWishList = `https://www.alnasser.pk/api/Getwishlistproduct/?user_id=`;
  }
  fetch(fetchWishList, {
    headers: {
      method: "GET",
      Authorization:
      "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    }
  })
    .then(res => res.json())
    .then(json => {
      console.log("WishList Data inside WishList", json);
      Store.dispatch({
        type: FETCH_WISHLIST_DETAIL,
        payload: {
          wishList: json.wishlist_data,
          wishListLength: json.wishlist_data.length
        }
      });
      // this.setState({
      //   searchMethods: json.wishlist_data
      //   //isDelete:false
      // });
    });
}

export const fetchHomePage = () =>
  fetch(homeURL, {
    headers: headers
  })
    .then(res => res.json())
    .then(json => {
      return json;
    });

export const fetchCartItems = id => {
  //id is either userID (for logged in user) or gId (for non logged in user)
  return dispatch => {
    var cartUrl;
    id !== null && id !== undefined
      ? (cartUrl = `https://www.alnasser.pk/api/viewcart/${id}/?view=cart&width=${deviceWidth}&gc=`)
      : (cartUrl = `https://www.alnasser.pk/api/viewcart/?view=cart&width=${deviceWidth}&gc=`);
    console.log("Cart URL:", cartUrl);
    //console.log('gID:',gId)
    fetch(cartUrl, {
      headers: {
        method: "GET",
       // Authorization: "Basic " + "YWRtaW5AYWxuYXNzZXIucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg=",
       Authorization: "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg=",
        "Cache-Control": "no-cache"
      }
    })
      .then(res => res.json())
      .then(json => {
        //console.log("Cart Items: \n", json);
        dispatch({
          type: USER_FETCH_CART,
          payload: json
        });
      });
  };
};

export const FBlogIn = async (g_id, checkOut, notificationToken) => {
  try {
    const {
      type,
      token,
      expires,
      permissions,
      declinedPermissions
    } = await Facebook.logInWithReadPermissionsAsync("1689424238051599", {
      permissions: ["public_profile", "email"]
    });
    if (type === "success") {
      // Get the user's name using Facebook's Graph API

      console.log("" + JSON.stringify(token));
      const response = await fetch(
        `https://graph.facebook.com/v3.2/me?fields=id,email,name,first_name,last_name,picture.type(large)&access_token=${token}`
      );
      let DATA = await response.json();

      console.log(JSON.stringify(DATA));
      checkEmailForFacebook(
        DATA.email,
        DATA.first_name,
        DATA.last_name,
        DATA.picture.data.url,
        g_id,
        checkOut,
        notificationToken
      );
      //Alert.alert("Logged in!", `Hi ${DATA.name}!`);
    } else {
      // type === 'cancel'
    }
  } catch ({ message }) {
    alert(`Facebook Login Error: ${message}`);
  }
};

export function signup(data) {
  Store.dispatch({
    type: SCREEN_LOADER
  });
  console.log("signup started ....", data);
  signupUrl = "https://www.alnasser.pk/api/login/";

  console.log("Going to fetch url", signupUrl);
  const methodType = "POST";
  return dispatch => {
    fetch(signupUrl, {
      method: methodType,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
      },
      body: JSON.stringify({
        email: data.email,
        password1: data.password,
        password2: data.password,
        user_type: "C",
        company_id: 1,
        g_id: data.g_id,
        app_token: data.app_token
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log("Signup Response: \n", json);
        if (json.error !== undefined) {
          console.log("signup failed 1");
          alert(json.msg);
          dispatch({
            type: USER_SIGNUP_FAILED,
            payload: {
              error: json.msg || json.message
            }
          });
          dispatch({ type: SCREEN_LOADER_HIDE });
        } else if (json.message === "The password field is required") {
          alert(json.message);
          console.log("signup failed 2");
          dispatch({
            type: USER_SIGNUP_FAILED,
            payload: {
              error: json.message
            }
          });
          dispatch({ type: SCREEN_LOADER_HIDE });
        } else if (
          json.msg ===
          "The username or email you have chosen already exists. Please try another one."
        ) {
          alert(json.msg);
          console.log("signup failed 2");
          dispatch({
            type: USER_SIGNUP_FAILED,
            payload: {
              error: json.msg
            }
          });
          dispatch({ type: SCREEN_LOADER_HIDE });
        } else {
          console.log("signup success");
          dispatch({
            type: USER_SIGNUP_SUCCESS,
            payload: {
              userId: json.user_id
            }
          });
          dispatch({ type: SCREEN_LOADER_HIDE });
          dispatch(fetchCartItems(json.user_id));
          dispatch(fetchUserOrders(json.user_id));
          dispatch(fetchUserProfile(json.user_id));
          dispatch(fetchWishListData(json.user_id));
          // <ToastModal
          // Toast = {json.msg}
          // />
          NavigationService.navigate("HomePage");
        }
      })
      .then(
        Store.dispatch({
          type: HIDE_SCREEN_LOADER
        })
      )
      .catch(error => {
        console.log("Signup Error: ", error);
      });
  };
}

export function signup2(data) {
  // Store.dispatch({
  //   type: SHOW_SCREEN_LOADER
  // });
  console.log("signup started ....", data);
  signupUrl = "https://www.alnasser.pk/api/users";
  console.log("Going to fetch url", signupUrl);
  var methodType = "POST";
  fetch(signupUrl, {
    method: methodType,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization:
      "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    },
    body: JSON.stringify({
      email: data.email,
      g_id: data.g_id,
      user_type: data.user_type,
      company_id: 1,
      status: data.status,
      g_id: data.g_id,
      firstname: data.firstName,
      lastname: data.lastName,
      app_token: data.app_token
    })
  })
    .then(res => res.json())
    .then(json => {
      console.log("Signup Response: \n", json);
      if (json.error !== undefined) {
        console.log("signup failed 1");
        Store.dispatch({
          type: USER_SIGNUP_FAILED,
          payload: {
            error: json.msg || json.message
          }
        });
      } else if (json.message === "The password field is required") {
        console.log("signup failed 2");
        Store.dispatch({
          type: USER_SIGNUP_FAILED,
          payload: {
            error: json.message
          }
        });
      } else {
        console.log("signup success");
        Store.dispatch({
          type: USER_SIGNUP_SUCCESS,
          payload: {
            userId: json.user_id
          }
        });
        Store.dispatch(fetchCartItems(json.user_id));
        Store.dispatch(fetchUserOrders(json.user_id));
        Store.dispatch(fetchUserProfile(json.user_id));
        Store.dispatch(fetchWishListData(json.user_id));
        
        NavigationService.navigate("HomePage");
      }
    })
    .then(
      Store.dispatch({
        type: HIDE_SCREEN_LOADER
      })
    )
    .catch(error => {
      console.log("Signup Error: ", error);
    });
}

export function checkEmailForFacebook(
  email,
  firstName,
  lastName,
  profileImage,
  g_id,
  checkOut,
  notificationToken
) {
  if (g_id !== null) {
    checkUrl = `https://www.alnasser.pk/api/users?email=${email}&g_id=${g_id}`;
  } else {
    checkUrl = `https://www.alnasser.pk/api/users?email=${email}&g_id=`;
  }

  fetch(checkUrl, { headers: headers }).then(res => res.json()).then(json => {
    console.log("Going to fetch url", checkUrl);
    console.log(json, "\n", email);
    if (json.users.length < 1) {
      console.log("True");
      const signupData = {
        email: email,
        g_id: g_id !== null ? g_id : "",
        user_type: "C",
        company_id: 0,
        status: "A",
        firstName: firstName,
        lastName: lastName,
        isNew: "new",
        app_token: notificationToken
      };
      // if (g_id !== null) signupData.g_id = g_id;
      signup2(signupData);
    } else {
      console.log(email + g_id + checkOut);
      //login(email, "", g_id, checkOut);

      Store.dispatch({
        type: USER_LOGIN_SUCCESS,
        payload: {
          email: json.users[0].email,
          firstName: json.users[0].firstname,
          lastName: json.users[0].lastname,
          profileDP: profileImage,
          userId: json.users[0].user_id
        }
      });
      Store.dispatch(fetchCartItems(json.users[0].user_id));
      Store.dispatch(fetchUserOrders(json.users[0].user_id));
      Store.dispatch(fetchUserProfile(json.users[0].user_id));
      Store.dispatch(fetchWishListData(json.users[0].user_id, null));
      if (checkOut) {
        NavigationService.navigate("SaveAddress", {
          fromLogin: true
        });
      } else {
        NavigationService.navigate("Profile");
      }
    }
  });
}

export function login(email, password, gid, checkOutStarted) {
  return dispatch => {
    dispatch({
      type: SCREEN_LOADER
    });
    var loginUrl;
    gid === null || gid === undefined
      ? (loginUrl = `https://www.alnasser.pk/api/login/?user_login=${email}&password=${password}&g_id=`)
      : (loginUrl = `https://www.alnasser.pk/api/login/?user_login=${email}&password=${password}&g_id=${gid}`);
    console.log("login url:", loginUrl);
    fetch(loginUrl, { headers: headers })
      .then(res => res.json())
      .then(json => {
        console.log(json);
        if (!json.error) {
          dispatch({
            type: USER_LOGIN_SUCCESS,
            payload: {
              email: json.email,
              firstName: json.firstname,
              lastName: json.lastname,
              userId: json.user_id
            }
          });
          dispatch({ type: SCREEN_LOADER_HIDE });
          dispatch(fetchCartItems(json.user_id));
          dispatch(fetchUserOrders(json.user_id));
          dispatch(fetchUserProfile(json.user_id));
          dispatch(fetchWishListData(json.user_id, null));
          if (checkOutStarted) {
            dispatch({
              type: SCREEN_LOADER_HIDE
            });
            NavigationService.navigate("BAG", {
              fromLogin: true
            });
          } else {
            dispatch({
              type: SCREEN_LOADER_HIDE
            });
            NavigationService.navigate("Profile");
          }
        } else {
          alert(json.message);
          dispatch({
            type: SCREEN_LOADER_HIDE
          });
          dispatch({
            type: USER_LOGIN_FAILED,
            payload: {
              loginError: json.message
            }
          });
        }
      })
      .catch(error => {
        console.log("Login Error: ", error);
      });
  };
}

export const fetchCategoryUrl = url =>
  fetch(url, {
    headers: headers
  })
    .then(res => res.json())
    .then(json => {
      return json;
    });

export const loadHomePage = res => {
  return {
    type: LOAD_HOMEPAGE,
    payload: {
      appVersion: res.base_version,
      categories: res.categories_icons[0].image_paths,
      bannerslider: res.top_slider[0].image_paths,
      minibanner: res.banners.filter(obj => obj.banner_type === "mini_banner"),
      sliderwidget: res.banners.filter(
        obj => obj.banner_type === "slider_widget"
      ),
      subCategories: res.categories[0].subcategories,
      isLoaded: true
    }
  };
};

export const loadCategoryListing = update => {
  return {
    type: LOAD_PRODUCTS,
    payload: {
      products: update.products,
      isLoaded: true
    }
  };
};

const createUpdateBrowsePosition = update => {
  return {
    type: UPDATE_BROWSE_POSITION,
    payload: {
      topCategoryId: update.topCategoryId,
      category: update.category,
      categoryId: update.categoryId,
      currentPage: update.currentPage,
      isCategoryListingPage: update.isCategoryListingPage
    }
  };
};

export const updateBrowsePosition = browsePosition => {
  Store.dispatch(createUpdateBrowsePosition(browsePosition));
};

export const updateUserGid = gid => {
  Store.dispatch({
    type: UPDATE_USER_GID,
    payload: {
      gid: gid
    }
  });
};

export const updateAddrss = (
  profileId,
  firstName,
  lastName,
  city,
  phone,
  state,
  address,
  userId,
  addressType,
  isProfile
) => {
  Store.dispatch({ type: SCREEN_LOADER });
  let strPhone = phone.replace(/\s/g, "");
  console.log("change address initated with user profile id \n: ", profileId);
  const orderUrl = `https://www.alnasser.pk/api/MultipleProfiles/${profileId}`;
  fetch(orderUrl, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    },
    body: JSON.stringify({
      b_lastname: lastName,
      profile_type: "P",
      b_city: city,
      b_address: address,
      profile_name: addressType,
      ship_to_another: "1",
      b_phone: strPhone,
      b_state: state.charAt(0),
      s_firstname: firstName,
      s_lastname: lastName,
      s_address: address,
      b_country_descr: strPhone.substring(3),
      s_country_descr: strPhone.substring(3),
      s_phone: strPhone,
      b_firstname: firstName,
      user_id: userId,
      s_city: city,
      s_state: state.charAt(0)
    })
  })
    .then(res => res.json())
    .then(json => {
      console.log("Update Address result: \n", json);
      Store.dispatch({
        type: CHANGE_ADDRESS_USER_INFO,
        payload: {
          firstName: firstName,
          lastName: lastName
        }
      });
      Store.dispatch({ type: SCREEN_LOADER_HIDE });
      Store.dispatch(fetchUserProfile(json.user_id));
      if (!isProfile) {
        if (json.user_id) {
          NavigationService.navigate("SaveAddress");
        }
      } else {
        if (json.user_id) {
          NavigationService.navigate("Profile");
        }
      }
    })
    .catch(error => {
      console.log("Update address Error: ", error);
    });
};

export const placeCodOrder = (
  paymentId,
  user,
  firstName,
  lastName,
  address,
  city,
  phoneNo
) => {
  Store.dispatch({ type: SCREEN_LOADER });
  console.log("COD order initated with user address\n: ", address);
  const orderUrl = "https://www.alnasser.pk/api/Extendedcheckout/";
  console.log("starting place order with POST method");
  fetch(orderUrl, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    },
    body: JSON.stringify({
      gc: "",
      company_id: "1",
      guestCheckoutData: {
        gc: "",
        user_data: {
          profile_mailing_list_1: "false",
          b_firstname: firstName,
          b_lastname: lastName,
          b_country: "PK",
          b_city: city,
          b_address: address,
          email: user.email,
          user_type: "C",
          ship_to_another: "0",
          user_id: user.userId,
          b_state: "P",
          b_phone: phoneNo
        },
        update_user_data: "Y",
        view: "checkout",
        lang_code: "en",
        ship_to_another: "0"
      },
      shipping_id: "[1]",
      user_id: user.userId,
      payment_id: paymentId
    })
  })
    .then(res => res.json())
    .then(json => {
      //Store.dispatch({type:USER_FETCH_CART})
      Store.dispatch({ type: SCREEN_LOADER_HIDE });
      //Store.dispatch(fetchCartItems(user.userId));
      console.log("Order plancement result: \n", json);
      if (json.order_id) {
        //dispatch(getSingleOrderDetails(json.order_id))
        //console.log('order details',orderDetails)
        NavigationService.navigate("PaymentSuccess", {
          id: json.order_id,
          isSuccess: true
        });
      } else {
        NavigationService.navigate("PaymentSuccess", {
          error: "There was some error"
        });
      }
    });
};

export const placeEasyPaisaOrder = (
  paymentId,
  user,
  firstName,
  lastName,
  address,
  city,
  phoneNo,
  totalAmount,
  email
) => {
  Store.dispatch({ type: SCREEN_LOADER });
  console.log("EasyPaisa order initated with user address\n: ", address);
  const orderUrl = "https://www.alnasser.pk/api/Extendedcheckout/";
  console.log("starting place order with POST method");
  fetch(orderUrl, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
    },
    body: JSON.stringify({
      gc: "",
      guestCheckoutData: {
        gc: "",
        user_data: {
          profile_mailing_list_1: "false",
          company_id: "27",
          b_firstname: firstName,
          b_lastname: lastName,
          b_country: "PK",
          b_city: city,
          b_address: address,
          email: user.email,
          user_type: "C",
          ship_to_another: "0",
          user_id: user.userId,
          b_state: "P",
          b_phone: phoneNo
        },
        company_id: "27",
        update_user_data: "Y",
        view: "checkout",
        lang_code: "en",
        ship_to_another: "0"
      },
      shipping_id: "[6]",
      user_id: user.userId,
      payment_id: paymentId
    })
  })
    .then(res => res.json())
    .then(json => {
      console.log("Order plancement result: \n", json);
      //Store.dispatch(fetchCartItems(user.userId));
      Store.dispatch({ type: SCREEN_LOADER_HIDE });
      if (json.wk_random_order_id) {
        Store.dispatch({ type: SCREEN_LOADER_HIDE });
        NavigationService.navigate("EasyPay", {
          id: json.wk_random_order_id,
          orderId: json.order_id,
          amount: totalAmount,
          phone: phoneNo,
          email: email
        });
      } else {
        Store.dispatch({ type: SCREEN_LOADER_HIDE });
        NavigationService.navigate("EasyPay", {
          error: "There was some error"
        });
      }
    });
};

function getSingleOrderDetails(orderId) {
  singleOrderUrl = `https://www.alnasser.pk/api/extendedorders/${orderId}`;
  console.log("fetch Order Detail API", singleOrderUrl);
  return dispatch => {
    fetch(singleOrderUrl, {
      headers: {
        method: "GET",
        Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
      }
    })
      .then(res => res.json())
      .then(json => {
        console.log("order detail data", json);
        dispatch({
          type: USER_LAST_ORDER,
          payload: json
        });
      });
  };
}
