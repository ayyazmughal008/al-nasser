import React, { Component } from "react";
import { connect } from "react-redux";
import { signup } from "../../redux/actions";
import { StyleSheet, TouchableHighlight,Image } from "react-native";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";

import ToastModal from "../../components/ToastModal";
import {
  Container,
  Content,
  Button,
  Input,
  Text,
  Form,
  Item,
  Spinner,
  Header,
  Left,
  Right,
  Body,
  Title,
  Icon,
  View
} from "native-base";
// import { observer, inject } from "mobx-react/native";
import ThemeHeader from "../../components/Header/index.js";

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      email: null,
      password1: null,
      password2: null,
      phone: "",
      phoneError: null,
      firstNameError: null,
      lastNameError: null,
      emailError: null,
      isFormValid: false,
      passwordError: null,
      signupStarted: false,
      text: "",
      genderError: null,
      InternationalFormatcell: "",
      NumberValidation: true
    };
  }

  logData(identifier, data) {
    console.log("Logger - ", identifier, data);
  }
  onPressSignup() {
    const signupData = {
      email: this.state.email,
      password: this.state.password1,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      phone: this.state.InternationalFormatcell
    };
    if (this.props.user.gId) signupData.g_id = this.props.user.g_id;

    this.validateForm();
    if (this.state.isFormValid) {
      console.log("Valid Form. Triggering singup");
      this.props.signup(signupData);
      this.setState({ signupStarted: true });
    }
  }
  validateForm() {
    if (!this.validateEmail()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validatePassword()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateFirstname()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateLastname()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validatePhone()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateAddressType()) {
      this.setState({ isFormValid: false });
      return;
    }
    this.setState({ isFormValid: true });
  }
  validatePassword() {
    var { passwordError } = this.state;
    const { password1, password2 } = this.state;
    if (password1 !== password2 || !password1 || !password2) {
      console.log("Passwords dont match");
      this.setState({ passwordError: "Passwords do not match" });
      alert("Passwords dont match");
      // <ToastModal
      //     Toast = "Passwords dont match"
      //     />
      return false;
    } else if (password1 === password2) {
      console.log("Passwords match");
      this.setState({ passwordError: null });

      return true;
    }
  }
  validateEmail() {
    var { emailError } = this.state;
    console.log(this.state.email);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.email) === false) {
      console.log("Email is Not Correct");
      this.setState({ emailError: "Please enter a valid email" });
      alert("Email is Not Correct");
      // <ToastModal
      //     Toast = "Email is Not Correct"
      //     />
      return false;
    } else {
      this.setState({ emailError: null });
      //this.setState({email:text})
      console.log("Email is Correct");
      return true;
    }
  }
  toggleSingupStarted(n) {
    this.setState({ signupStarted: n });
  }

  validateFirstname() {
    var { firstNameError } = this.state;
    const { firstName } = this.state;
    if (!firstName) {
      console.log("First Name is not Valid");
      alert("First Name is not Valid");
      this.setState({ firstNameError: "First Name is not Valid" });

      // <ToastModal
      //     Toast = "First Name is not Valid"
      //     />
      return false;
    }
    return true;
  }

  validateLastname() {
    var { lastNameError } = this.state;
    const { lastName } = this.state;
    if (!lastName) {
      console.log("Last Name is not Valid");
      alert('"Last Name is not Valid"');
      this.setState({ lastNameError: "Last Name is not Valid" });

      //  <ToastModal
      //       Toast = "Last Name is not Valid"
      //       />
      return false;
    }
    return true;
  }

  validatePhone() {
    const { phone } = this.state;
    if (
      this.state.NumberValidation &&
      this.state.InternationalFormatcell != "" &&
      this.state.phone != ""
    ) {
      return true;
    } else {
      console.log("Phone Number is not Valid");
      <ToastModal Toast="Phone Number is not Valid" />;
      alert("Phone Number is not Valid\n write in that format: 3011234567");
      this.setState({ phoneError: "Phone Number is not Valid" });
    }
    return false;
  }

  validateAddressType() {
    var { genderError } = this.state;
    const { text } = this.state;
    if (!text) {
      console.log("Gender not Selected");
      //alert("Gender not Selected")
      this.setState({ genderError: "Gender not Selected" });

      <ToastModal Toast="Gender not Selected" />;
      return false;
    }
    return true;
  }

  onSelect(index, value) {
    this.setState({
      text: value
    });
  }

  render() {
    const navigation = this.props.navigation;
    const { showScreenLoader } = this.props.screenState;
    const { signupStarted, isFormValid } = this.state;
    //if(showScreenLoader){this.toggleSingupStarted(true)} else this.toggleSingupStarted(false)
    const { signupError } = this.props.user;
    console.log("Screenloader, ", showScreenLoader);
    console.log("isFormValid,", isFormValid);
    return (
      <Container>
        {/* <ThemeHeader
          PageTitle="SIGN UP"
          IconLeft="arrow-back"
          route="loginHome"
          navigation={navigation}
        /> */}
        <Header
          style={{
            marginTop: 5,
            marginLeft: 5,
            marginRight: 5,
            borderRadius: 5,
            backgroundColor: "white",
            marginBottom: 6
          }}
        >
          <Left>
            {
              <Button
                transparent
                //onPress={() => navigation.navigate("HomePage")}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image
                      source={require("../../../assets/arrow_black.png")}
                      resizeMode = "contain"
                      style={{height:23,width:40,marginRight:10}}
                    />
              </Button>
            }
          </Left>

          <Body>
            <Title
              style={{
                color: "black",
                fontSize: 16,
                fontWeight: "400",
                alignSelf: "flex-start"
              }}
            >
              SIGN UP
            </Title>
          </Body>
        </Header>

        <Content
          padder
          style={{ backgroundColor: "#fff", marginBottom: null }}
          bounces={false}
        >
          {/* {
          showScreenLoader===true &&
            <Spinner color='green' />
        } */}
          <Form>
            <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder="Email address"
                keyboardType= "email-address"
                autoCapitalize="none"
                onChangeText={email => this.setState({ email })}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder="Password"
                secureTextEntry
                onChangeText={password1 => this.setState({ password1 })}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder="Confirm password"
                secureTextEntry
                onChangeText={password2 => this.setState({ password2 })}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder="First Name"
                onChangeText={firstName => this.setState({ firstName })}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder="Last Name"
                onChangeText={lastName => this.setState({ lastName })}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                ref="mobileNo"
                maxLength={10}
                keyboardType="numeric"
                placeholder="phone"
                onChangeText={phone => {
                  try {
                    const NumberWeWant = parsePhoneNumber(phone, "PK");
                    this.setState({
                      NumberValidation: NumberWeWant.isValid(),
                      phone,
                      InternationalFormatcell: NumberWeWant.isValid()
                        ? NumberWeWant.formatInternational()
                        : ""
                    });
                  } catch (e) {
                    this.setState({
                      NumberValidation: false,
                      phone,
                      InternationalFormatcell: ""
                    });
                  }

                  // this.setState({ phone })
                }}
              />
            </Item>
            <RadioGroup
              onSelect={(index, value) => this.onSelect(index, value)}
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                marginTop: 10
              }}
            >
              <RadioButton value={"Male"}>
                <Text>Male</Text>
              </RadioButton>

              <RadioButton value={"Female"}>
                <Text>Female</Text>
              </RadioButton>
              <RadioButton value={"N/A"}>
                <Text>N/A</Text>
              </RadioButton>
            </RadioGroup>

            <Button
              primary
              block
              //onPress={() => navigation.navigate("Profile")}
              onPress={() => this.onPressSignup()}
              style={{ marginTop: 30 }}
            >
              <Text> CREATE ACCOUNT </Text>
              {showScreenLoader === true &&
                <Spinner color="white" size="small" />}
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}
//export default SignUp;
const mapStateToProps = state => ({
  user: state.user,
  screenState: state.appState
});
export default connect(mapStateToProps, { signup })(SignUp);

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: "red"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
