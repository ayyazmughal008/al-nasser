import React, { Component } from "react";
import { Container, Content } from "native-base";

import { View, Text, FlatList } from "react-native";
import { updateBrowsePosition } from "../../redux/actions";

import RoundImageButton from "../../components/RoundImageButton/index.js";
import NavigationService from "../../NavigationService";

import { connect } from "react-redux";

import ThemeHeader from "../../components/Header/index.js";
import MyFooter from "../../components/Footer";

class CategoryPage extends Component {
  handleTopCategoryIconClick(categoryID) {
    updateBrowsePosition({
      categoryId: categoryID,
      currentPage: 0,
      topCategoryId: categoryID,
      isCategoryListingPage: true
    });
    NavigationService.navigate("Category");
  }

  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const { navigation } = this.props.navigation;

    var { categoryicons, subCategories } = this.props.homePageState;

    return (
      <Container>
        <ThemeHeader
          navigation={this.props.navigation}
          drawerOpen="DrawerOpen"
          IconLeft="ios-menu"
          textInput="dd"
          PageTitle="Category"
          //IconRight="search"
        />
        <Content>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <FlatList
              data={categoryicons}
              numColumns={2}
              keyExtractor={this._keyExtractor}
              renderItem={({ item }) =>
                <RoundImageButton
                  navigation={navigation}
                  roundImageText={item.cname}
                  roundImageSource={item.image_path}
                  clickHandler={() =>
                    this.handleTopCategoryIconClick(item.target_id)}
                  categoryID={item.target_id}
                  subCat={subCategories.filter(
                    cat => cat.category_id === item.target_id
                  )}
                  style={{
                    resizeMode: "contain",
                    flex: 1
                  }}
                />}
            />
          </View>
        </Content>
        <MyFooter
          navigation={this.props.navigation}
          selected={"grid"}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  homePageState: state.homepage
});

export default connect(mapStateToProps)(CategoryPage);
