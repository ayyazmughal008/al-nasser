import React, { Component } from "react";
import { Container, Content, List } from "native-base";
import { View, FlatList, ActivityIndicator, ScrollView } from "react-native";
import ThemeHeader from "../../components/Header/index.js";
import NotifyContent from "../../components/NotifyContent";
import MyFooter from "../../components/Footer";
import moment from "moment";
import { connect } from "react-redux";
import { fetchNotification } from "../../redux/actions";
import { notification } from "expo/build/Haptic/Haptic";

class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchMethods: null,
      isFetching: false
    };
  }
  componentWillMount() {
    fetchNotification();
  }

  convertDate(num) {
    var newDate = moment(new Date(num * 1000)).format("DD MM YYYY hh:MM A");
    return newDate;
  }

  clickHandler(type, typeData, targetUrl, productName) {
    if (type === "C") {
      this.props.navigation.navigate("ProductList", {
        cid: typeData,
        productName: productName
      });
    } else if (type === "U") {
      this.props.navigation.navigate("ProductList", {
        targetUrl: targetUrl,
        productName: productName
      });
    } else {
      this.props.navigation.navigate("ProductPage", {
        TargetId: typeData,
        productName: title
      });
      console.log("no other types" + TargetId);
    }
  }

  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const { navigation } = this.props.navigation;
    const { notifications } = this.props.user;
    console.log("My Notificatios is", notification);
    return (
      <Container>
        <ThemeHeader
          navigation={this.props.navigation}
          drawerOpen="DrawerOpen"
          IconLeft="ios-menu"
          textInput="dd"
          PageTitle="NOTIFICATIONS"
          //IconRight="search"
        />
        <Content
          contentContainerStyle={{ paddingBottom: 10 }}
          padder
          showsVerticalScrollIndicator={false}
          style={{ marginBottom: 50 }}
        >
          {!this.props.loader.showLoader
            ? <FlatList
                scrollEnabled={false}
                data={notifications}
                keyExtractor={this._keyExtractor}
                numColumns={1}
                renderItem={({ item }) =>
                  <NotifyContent
                    navigation={navigation}
                    tag={item.title}
                    description={item.content}
                    time={this.convertDate(item.time)}
                    clickHandler={() =>
                      this.clickHandler(
                        item.type,
                        item.type_data,
                        item.target_url,
                        item.title
                      )}
                  />}
              />
            : <View style={{ justifyContent: "center", alignItems: "center" }}>
                <ActivityIndicator size="large" color="#0000ff" />
              </View>}
        </Content>
        <MyFooter
          navigation={this.props.navigation}
          selected={"notifications"}
        />
      </Container>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user,
  loader: state.loader
});

export default connect(mapStateToProps, { fetchNotification })(Notifications);
