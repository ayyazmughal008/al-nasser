import React, { Component } from "react";
import { WebView, ActivityIndicator, Dimensions } from "react-native";
import { Header, Left, Body, Title, Icon, View, Button } from "native-base";
import { connect } from "react-redux";
import { fetchEncodedValues } from "../../redux/actions";

var height = Dimensions.get("window").width;
var width = Dimensions.get("window").height;

class Easypay extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: true, url: "" };
  }

  componentWillMount() {
    const navigation = this.props.navigation;
    const orderId = navigation.getParam("orderId", "1234");
    const amount = navigation.getParam("amount", "1234");
    const phone = navigation.getParam("phone", "1234");
    const email = navigation.getParam("email", "abc@gmail.com");
    var formatAmount = amount + ".0";

    fetchEncodedValues(
      formatAmount,
      orderId,
      encodeURIComponent(email),
      phone.substring(2)
    );
  }

  pad2 = n => {
    return n < 10 ? "0" + n : n;
  };

  hideSpinner() {
    this.setState({ visible: false });
  }

  _onNavigationStateChange(webViewState) {
    this.setState({ url: webViewState.url });
    console.log(this.state.url);
  }

  pageFinished = () => {
    const navigation = this.props.navigation;
    const randomId = navigation.getParam("id", -1);
    // if (
    //   this.state.url !==
    //   "https://easypay.easypaisa.com.pk/easypay/faces/pg/site/AuthError.jsf"
    // ) {

    if (this.state.url === "https://www.clicky.pk/index.php/?success=true") {
      navigation.navigate("PaymentSuccess", {
        id: randomId,
        isSuccess: true
      });
    } else if (
      this.state.url === "https://www.clicky.pk/index.php/?success=false"
    ) {
      navigation.navigate("PaymentSuccess", {
        id: randomId,
        isSuccess: false
      });
    } else {
      navigation.navigate("PaymentSuccess", { id: randomId, isSuccess: false });
    }
    // }
    // else {
    //   navigation.navigate("PaymentSuccess", { id: randomId, isSuccess: false });
    // }
  };

  render() {
    //var {  } = this.state;
    const navigation = this.props.navigation;
    const randomId = navigation.getParam("id", -1);
    const orderId = navigation.getParam("orderId", "1234");
    const amount = navigation.getParam("amount", "1234");
    const phone = navigation.getParam("phone", "1234");
    const email = navigation.getParam("email", "abc@gmail.com");
    const { encodedValue } = this.props.user;
    console.log("Random order id", encodedValue[0]);

    var date = new Date();
    var datetime =
      date.getFullYear().toString() +
      this.pad2(date.getMonth() + 1) +
      this.pad2(date.getDate() + 1) +" " 

      console.log('My Easy Paisa Data: \n',
      `amount=${amount}.0&autoRedirect=1&emailAddr=${encodeURIComponent(
        email
      )}&expiryDate=${datetime}235959&mobileNum=${phone.substring(
        2
      )}&orderRefNum=${orderId}&paymentMethod=CC_PAYMENT_METHOD&postBackURL=https%3A%2F%2Fwww.clicky.pk%2Findex.php%2F%3Fdispatch%3Dtelenor.verify&storeId=5503&merchantHashedReq=${encodedValue[0]}`
      )

    return (
      <View style={{ flex: 1 }}>
        <Header
          style={{
            marginTop: 5,
            backgroundColor: "black",
            marginBottom: 6
          }}
        >
          <Left>
            {
              <Button
                transparent
                onPress={() => this.pageFinished()}
                //onPress={() => this.props.navigation.navigate('HomePage')}
              >
                <Icon
                  style={{ padding: 5, marginLeft: -5, color: "white" }}
                  name="ios-arrow-back"
                />
              </Button>
            }
          </Left>

          <Body>
            <Title
              style={{
                color: "white",
                fontSize: 16,
                fontWeight: "400",
                alignSelf: "flex-start"
              }}
            >
              Credit/Debit by EasyPaisa
            </Title>
          </Body>
        </Header>

        {/* <View style={{flex:0.5}}>
        <WebView
          ref="webview"
          source={{
            uri: "https://easypay.easypaisa.com.pk/easypay/Index.jsf",
            body: `amount=449.0&autoRedirect=1&emailAddr=.ayyazmughal%40yahoo.com&expiryDate=20190125 235959&mobileNum=18440137&orderRefNum=5618671&paymentMethod=CC_PAYMENT_METHOD&postBackURL=https%3A%2F%2Fwww.clicky.pk%2Findex.php%2F%3Fdispatch%3Dtelenor.verify&storeId=5503&merchantHashedReq=w2qKnZXEW5nMud%2FiTZvZvnq9DK9ur5IE4H7Hhph1GGCHNzmfB%2BYSukkcitWDrDrpBy1CDKt9wO1ZIjDl9bZ3tAnS4QXif2%2FDTkHxUMxmwgOwkPlw6YdRHwxmiSyT%2BXzhJ0ShV4xD6D62oWGaiQV4rHROfgfaE10z826MGgOskR7Nw3OWWFO0xLwtNSgBALegnou6MKq1Rxft8DaZ1c%2B%2FI3tXp5gcBtejhVKYWO82ffbPejna9YSNfPYqhLdNMjqk1yiBivuVA%2F8a5EXjhuMXU1OlDBigvW7%2FPy3QeuhwOaLdiLSEJdDV2WrTH%2F0wRyOYEvxQSl%2FMSN1SzalDtt%2BW%2Fg%3D%3D`,
            method: "POST"
          }}
          
          onLoad={() => this.hideSpinner()}
          onNavigationStateChange={this._onNavigationStateChange.bind(this)}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          startInLoadingState={false}
          // onLoadEnd={this.pageFinished()}
        />
        {this.state.visible &&
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              position: "absolute",
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              opacity: 0.7,
              backgroundColor: "transparent"
            }}
          >
            <ActivityIndicator size="large" color="green" />
          </View>}
          </View> */}

        {this.props.loader.showLoader
          ? <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                opacity: 0.7,
                backgroundColor: "transparent"
              }}
            >
              <ActivityIndicator size="large" color="green" />
            </View>
          : <WebView
              source={{
                uri: "https://easypay.easypaisa.com.pk/easypay/Index.jsf",
                body: `amount=${amount}.0&autoRedirect=1&emailAddr=${encodeURIComponent(
                  email
                )}&expiryDate=${datetime}235959&mobileNum=${phone.substring(
                  2
                )}&orderRefNum=${orderId}&paymentMethod=CC_PAYMENT_METHOD&postBackURL=https%3A%2F%2Fwww.clicky.pk%2Findex.php%2F%3Fdispatch%3Dtelenor.verify&storeId=5503&merchantHashedReq=${encodedValue[0]}`,
                //body: `amount=449.0&autoRedirect=1&emailAddr=m.ayyazmughal%40yahoo.com&expiryDate=20190125 235959&mobileNum=18440137&orderRefNum=5618671&paymentMethod=CC_PAYMENT_METHOD&postBackURL=https%3A%2F%2Fwww.clicky.pk%2Findex.php%2F%3Fdispatch%3Dtelenor.verify&storeId=5503&merchantHashedReq=w2qKnZXEW5nMud%2FiTZvZvnq9DK9ur5IE4H7Hhph1GGCHNzmfB%2BYSukkcitWDrDrpBy1CDKt9wO1ZIjDl9bZ3tAnS4QXif2%2FDTkHxUMxmwgOwkPlw6YdRHwxmiSyT%2BXzhJ0ShV4xD6D62oWGaiQV4rHROfgfaE10z826MGgOskR7Nw3OWWFO0xLwtNSgBALegnou6MKq1Rxft8DaZ1c%2B%2FI3tXp5gcBtejhVKYWO82ffbPejna9YSNfPYqhLdNMjqk1yiBivuVA%2F8a5EXjhuMXU1OlDBigvW7%2FPy3QeuhwOaLdiLSEJdDV2WrTH%2F0wRyOYEvxQSl%2FMSN1SzalDtt%2BW%2Fg%3D%3D`,

                method: "POST"
              }}
              style={{ flex: 1, marginTop: 10 }}
              onNavigationStateChange={this._onNavigationStateChange.bind(this)}
            />}
      </View>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user,
  loader: state.loader
});
export default connect(mapStateToProps, { fetchEncodedValues })(Easypay);

//amount,emailAddr,mobileNum,orderRefNum,
