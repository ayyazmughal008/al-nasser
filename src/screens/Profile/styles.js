export default {
  profileCoverPic: {
    resizeMode: "cover",
    alignSelf: "center",
    height: 180
  },
  contentView: {
    height: 500,
    borderColor: "#fff",
    borderWidth: 1,
  },
  contentTabView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#FFFFFF"
  },
  contentTabBtn: {
    flex: 1,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  seperator: {
    alignItems: "center",
    height: 15,
    width: 1.5,
    backgroundColor: "#888"
  }
};
