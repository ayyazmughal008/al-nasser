import React, { Component } from "react";
import { connect } from "react-redux";
import { Image, Text, TouchableHighlight, Dimensions } from "react-native";
import {
  Button,
  Icon,
  Thumbnail,
  Grid,
  Col,
  View,
  CardItem,
  Card
} from "native-base";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import MyFooter from "../../components/Footer";
import commonColor from "../../theme/variables/commonColor.js";
import LoginHome from "../LoginHome";
import styles from "./styles";
import { fetchWishListData } from "../../redux/actions";
import { LinearGradient } from "expo";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countCollections: 0,
      countOrders: 0,
      countFollowing: 0,
      countFollowers: 0
    };
  }
  componentWillMount() {
    const { userId, gId } = this.props.user;
    if (userId || gId) {
      fetchWishListData(userId, gId);
    }
  }
  // componentWillMount() {
  //   var navigation = this.props.navigation;
  //   var isLoggedIn = this.props.user.isLoggedIn;
  //   // if (!isLoggedIn) navigation.navigate("LoginHome");
  // }

  handleOnPressTargetCID() {
    console.log("handleOnPressTargetCID");
    this.props.navigation.navigate("Orders");
  }

  handleOnPressAddress() {
    // console.log('handleOnPressTargetCID')
    this.props.navigation.navigate("UpdateAddress");
  }

  handleOnPressWishlist() {
    // console.log('handleOnPressTargetCID')
    this.props.navigation.navigate("WishList");
  }

  render() {
    // var firstName = this.props.user.firstName
    // var lastName = this.props.user.lastName
    var {
      firstName,
      lastName,
      orders_qty,
      userHomeAddress,
      wishListLength,
      userOfficeAddress,
      profileDP
    } = this.props.user;
    var navigation = this.props.navigation;
    var isLoggedIn = this.props.user.isLoggedIn;
    console.log("Profile Screen\n", userHomeAddress);
    if (!isLoggedIn) {
      return <LoginHome nav={this.props.navigation} />;
    }
    return (
      <View style={{ flex: 1 }}>
        <View style={{ alignItems: "center" }}>
          <LinearGradient
            //colors={['#00FFFF', '#17C8FF', '#329BFF', '#4C64FF', '#6536FF', '#8000FF']}
            colors={["#642B73", "#c6426e"]}
            start={{ x: 0.0, y: 1.0 }}
            end={{ x: 1.0, y: 1.0 }}
            style={{
              width: deviceWidth * 2 / 1.1,
              height: deviceHeight * 2 / 4 - 120,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image
              resizeMode="cover"
              style={{
                height: 100,
                width: 100,
                borderRadius: 80,
                backgroundColor: "#fff"
              }}
              source={
                profileDP
                  ? { uri: profileDP }
                  : require("../../../assets/profile_png.png")
              }
            />

            <Text
              style={{
                backgroundColor: "transparent",
                fontSize: 17,
                fontWeight: "700",
                color: "#fff",
                textAlign: "center",
                marginTop: 20
              }}
            >
              Welcome {firstName} {lastName}
            </Text>
          </LinearGradient>
        </View>

        <View style={{ flexDirection: "row" }}>
          <Card transparent>
            <TouchableHighlight onPress={() => this.handleOnPressWishlist()}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  height: 60
                }}
              >
                <Thumbnail
                  style={{
                    height: 45,
                    width: 45,
                    left: "0%"
                  }}
                  square
                  source={require("../../../assets/navwishlist.png")}
                />
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginRight: 5
                  }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "400",
                      color: "#333",
                      marginBottom: 2
                    }}
                    numberOfLines={2}
                    ellipsizeMode="tail"
                  >
                    {"Wish List"}
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "300",
                      color: "#333",
                      textAlign: "right",
                      marginTop: 3
                    }}
                    ellipsizeMode="tail"
                  >
                    {"items("}{wishListLength !== null ? wishListLength : "0"}{")"}
                  </Text>
                </View>
              </View>
            </TouchableHighlight>
          </Card>
        </View>
        <View style={{ flexDirection: "row" }}>
          <Card transparent>
            <TouchableHighlight onPress={() => this.handleOnPressTargetCID()}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  height: 60
                }}
              >
                <Thumbnail
                  style={{
                    height: 50,
                    width: 50,
                    left: "0%"
                  }}
                  square
                  source={require("../../../assets/nav_orders.png")}
                />
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginRight: 5
                  }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "400",
                      color: "#333",
                      marginBottom: 2
                    }}
                    numberOfLines={2}
                    ellipsizeMode="tail"
                  >
                    {"Orders"}
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: "300",
                      color: "#333",
                      marginTop: 3
                    }}
                    ellipsizeMode="tail"
                  >
                     {orders_qty}{" "}{"orders"} 
                  </Text>
                </View>
              </View>
            </TouchableHighlight>
          </Card>
        </View>
        <View style={{ flexDirection: "row" }}>
          <Card transparent>
            <TouchableHighlight onPress={() => navigation.navigate("Address")}>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  height: 60
                }}
              >
                <Thumbnail
                  style={{
                    height: 50,
                    width: 50,
                    left: "2%"
                  }}
                  square
                  source={require("../../../assets/address.png")}
                />

                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: "400",
                    color: "#333",
                    marginBottom: 2
                  }}
                  numberOfLines={2}
                  ellipsizeMode="tail"
                >
                  {"Address"}
                </Text>
              </View>
            </TouchableHighlight>
          </Card>
        </View>

        <MyFooter navigation={this.props.navigation} selected={"loginHome"} />
      </View>
    );
  }
}

//export default Profile;
const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps, { fetchWishListData })(Profile);

{
  /* <ParallaxScrollView
          showsVerticalScrollIndicator={false}
          backgroundColor="transparent"
          contentBackgroundColor="#F6F6F7"
          parallaxHeaderHeight={180}
          renderBackground={() =>
            <Image
              source={require("../../../assets/profile-cover.png")}
              style={styles.profileCoverPic}
            />}
          renderForeground={() =>
            <Grid style={{ marginTop: 35, padding: 20 }}>
              {/* <Col size={0.8}>
                <Thumbnail
                  large
                  source={require("../../../assets/sanket.png")}
                />
              </Col> */
}
//       <Col size={2.2}>
//         <View style={{ flex: 1 }}>
//           <Text style={{ fontSize: 20, padding: 7 }}>
//             {firstName} {lastName}
//           </Text>
//           <Button small transparent style={{ paddingLeft: 5 }}>
//             <Text
//               style={{
//                 fontStyle: "italic",
//                 color: commonColor.brandPrimary
//               }}
//             >
//               Add Bio
//             </Text>
//           </Button>
//         </View>
//         <View style={{ flexDirection: "row" }}>
//           <Button
//             transparent
//             style={{ paddingLeft: 0, marginLeft: 0, marginTop: -16 }}
//           >
//             <Text>
//               {" "}{this.state.countFollowing} Following
//             </Text>
//           </Button>
//           <View style={styles.seperator} />
//           <Button
//             transparent
//             style={{ paddingLeft: 0, marginLeft: 10, marginTop: -15 }}
//           >
//             <Text>
//               {" "}{this.state.countFollowers} Followers
//             </Text>
//           </Button>
//         </View>
//       </Col>
//     </Grid>}
// >
//   <View style={styles.contentView}>
//     <View style={styles.contentTabView}>
//       <TouchableHighlight transparent style={styles.contentTabBtn}>
//         <Text
//           style={styles.wcsText}
//           onPress={() =>
//             navigation.navigate(
//               "Address"
//               //   ,{
//               //   isProfile: true
//               // }
//             )}
//         >
//           Address
//         </Text>
//       </TouchableHighlight>
//       <View style={styles.seperator} />
//       <TouchableHighlight transparent style={styles.contentTabBtn}>
//         <Text
//           style={styles.wcsText}
//           onPress={() => this.handleOnPressWishlist()}
//         >
//           {wishListLength !== null ? wishListLength+" WishList" : "0 WishList" }
//         </Text>
//       </TouchableHighlight>
//       <View style={styles.seperator} />
//       <TouchableHighlight transparent style={styles.contentTabBtn}>
//         <Text
//           style={styles.wcsText}
//           onPress={() => this.handleOnPressTargetCID()}
//         >
//           {" "}{orders_qty} Order
//         </Text>
//       </TouchableHighlight>
//     </View>
//     {/* <View style={{ margin: 20 }}>
//       <Button block bordered primary>
//         <Icon name="ios-add" />
//         <Text style={{ color: commonColor.brandPrimary }}>POST</Text>
//       </Button>
//     </View> */}
//   </View>
// </ParallaxScrollView> */}
