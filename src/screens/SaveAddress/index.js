import React, { Component } from "react";
import { Container, Button, Content, View, Footer, Text } from "native-base";
import { FlatList, ActivityIndicator } from "react-native";
import BaseHeader from "../../components/BaseHeader/index.js";
import { connect } from "react-redux";
import { fetchShippingCharges } from "../../redux/actions";
import AddressList from "../../components/AddressList";
import ReviewCartList from "../../components/ReviewCartList";
import Price from "../../components/Price";

class SaveAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    //this.fetchuserProfile();
    const { userProfile, userId } = this.props.user;
    fetchShippingCharges(
      userProfile.profile_id,
      userProfile.s_firstname,
      userProfile.s_lastname,
      userProfile.s_city,
      userProfile.s_phone,
      userProfile.s_country_descr,
      userProfile.s_address,
      userId
    );
  }

  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const navigation = this.props.navigation;
    const cartData = this.props.user.cartItems;
    const { userProfile, shippingCharges } = this.props.user;

    const fromlogin = navigation.getParam("fromLogin");

    let totalAmount = 0;
    if (shippingCharges) {
      let temShip = parseInt(shippingCharges.substring(3));
      let temTotal = cartData.cart.total;
      totalAmount = temShip + temTotal;
      //console.log("User Profile is \n", userProfile);
    }

    return (
      <Container>
        <BaseHeader
          PageTitle="Payment Review"
          IconLeft="arrow-back"
          //route="confirmAddress"
          navigation={navigation}
        />
        <Content>
          <AddressList
            navigation={navigation}
            userAddressType="Shipping Detail"
            firstName={userProfile.s_firstname}
            lastName={userProfile.s_lastname}
            userAddress={userProfile.s_address}
            userCity={userProfile.s_city}
            userCountery={userProfile.s_country_descr}
            userState={userProfile.s_state}
            userPhone={userProfile.s_phone}
            isProfile={false}
          />

          <FlatList
            //scrollEnabled = {false}
            data={cartData.cart.products}
            keyExtractor={this._keyExtractor}
            numColumns={1}
            renderItem={({ item }) =>
              <ReviewCartList
                imageSource={item.image_path}
                product={item.product}
                soldby={item.company}
                price={item.format_display_price}
                quantity={item.amount}
                subTotal={item.format_subtotal}
              />}
          />

          <Price
            subTotal={cartData.cart.format_display_subtotal}
            tax={cartData.cart.format_tax_subtotal}
            shipping={shippingCharges}
            total={cartData.cart.format_total}
          />
          {this.props.loader.showLoader &&
            <ActivityIndicator
              size="large"
              color="#0000ff"
              style={{
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                alignItems: "center",
                justifyContent: "center"
              }}
            />}
        </Content>
        <Footer style={{ position: "absolute", bottom: 0, height: 60 }}>
          <View style={{ flex: 1, padding: 5 }}>
            <Button
              primary
              block
              style={{ marginLeft: 10, marginRight: 10, marginBottom: 10 }}
              onPress={() =>
                navigation.navigate("PaymentOptions", {
                  ShippingCharges: shippingCharges
                })}
              //onPress={()=>this.onLogin()}
              style={{ marginTop: 5 }}
            >
              <Text>
                {" "}PAY {cartData.cart.format_total} SECURELY{" "}
              </Text>
            </Button>
          </View>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  loader: state.loader
});

export default connect(mapStateToProps, { fetchShippingCharges })(SaveAddress);
