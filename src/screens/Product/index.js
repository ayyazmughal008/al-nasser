import React, { Component } from "react";
import { View, Dimensions, TouchableOpacity } from "react-native";
import {
  Container,
  Content,
  List,
  Header,
  Button,
  Left,
  Right,
  Body,
  Title,
  Icon
} from "native-base";
// import { observer, inject } from "mobx-react/native";
import ThemeHeader from "../../components/Header/index.js";
import SaleThumb from "../../components/SaleThumb/index.js";
import SaleTitle from "../../components/SaleTitle/index.js";
import SaleFlex from "../../components/SaleFlex/index.js";
import SaleBrandThumb from "../../components/SaleBrandThumb/index.js";
import MyFooter from "../../components/Footer";
import styles from "./styles.js";

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

// @inject("view.app", "domain.user", "app", "routerActions")
// @observer
class Product extends Component {
  render() {
    const navigation = this.props.navigation;
    var imageFlex = [
      require("../../../assets/login-cover.png"),
      require("../../../assets/login-cover.png")
    ];

    var dataSaleThumb = [
      {
        id: 1,
        imageSaleThumb: require("../../../assets/1.png"),
        text: "30 - 50% Off"
      },
      {
        id: 2,
        imageSaleThumb: require("../../../assets/2.png"),
        text: "Up to 60% Off"
      },
      {
        id: 3,
        imageSaleThumb: require("../../../assets/3.png"),
        text: "30 - 50% Off"
      },
      {
        id: 4,
        imageSaleThumb: require("../../../assets/4.png"),
        text: "Up to 30% Off"
      },
      {
        id: 5,
        imageSaleThumb: require("../../../assets/b1.png"),
        text: "30 - 50% Off"
      },
      {
        id: 6,
        imageSaleThumb: require("../../../assets/13.png"),
        text: "40 - 70% Off"
      },
      {
        id: 7,
        imageSaleThumb: require("../../../assets/b2.png"),
        text: "30 - 50% Off"
      },
      {
        id: 8,
        imageSaleThumb: require("../../../assets/5.png"),
        text: "Up to 60% Off"
      },
      {
        id: 9,
        imageSaleThumb: require("../../../assets/4.png"),
        text: "30 - 50% Off"
      }
    ];
    var dataSaleBrand = [
      {
        id: 1,
        imageSaleThumb: require("../../../assets/brand-logo1.png"),
        text: "Min. 50% Off"
      },
      {
        id: 2,
        imageSaleThumb: require("../../../assets/brand-logo2.png"),
        text: "Flat 60% Off"
      },
      {
        id: 3,
        imageSaleThumb: require("../../../assets/brand-logo1.png"),
        text: "Min. 50% Off"
      },
      {
        id: 4,
        imageSaleThumb: require("../../../assets/brand-logo2.png"),
        text: "Flat 60% Off"
      },
      {
        id: 5,
        imageSaleThumb: require("../../../assets/brand-logo1.png"),
        text: "Min. 50% Off"
      },
      {
        id: 6,
        imageSaleThumb: require("../../../assets/brand-logo2.png"),
        text: "Flat 60% Off"
      },
      {
        id: 7,
        imageSaleThumb: require("../../../assets/brand-logo1.png"),
        text: "Min. 50% Off"
      },
      {
        id: 8,
        imageSaleThumb: require("../../../assets/brand-logo2.png"),
        text: "Flat 60% Off"
      },
      {
        id: 9,
        imageSaleThumb: require("../../../assets/brand-logo1.png"),
        text: "Flat 60% Off"
      }
    ];
    return (
      <Container>
        {/* <ThemeHeader
          PageTitle="PRODUCT"
          IconLeft="arrow-back"
          route="homepage"
          IconRight="search"
          navigation={navigation}
        /> */}

        <Header
          style={{
            marginTop: 5,
            marginLeft: 5,
            marginRight: 5,
            borderRadius: 5,
            backgroundColor: "white",
            marginBottom: 6
          }}
        >
          <Left>
            {
              <Button
                transparent
                //onPress={() => navigation.navigate("HomePage")}
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon
                  style={{ padding: 5, marginLeft: -5, color: "black" }}
                  name="arrow-back"
                />
              </Button>
            }
          </Left>

          <Body>
            <Title
              style={{
                color: "black",
                fontSize: 16,
                fontWeight: "400",
                alignSelf: "flex-start"
              }}
            >
              PRODUCT
            </Title>
          </Body>
        </Header>

        <Content
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 10 }}
          style={{ marginBottom: 50 }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("ProductList")}>
            <SaleFlex imageFlexSource={imageFlex[0]} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate("ProductList")}>
            <SaleTitle saleTitle="Last Chance of Stock Up" />
          </TouchableOpacity>
          <View>
            <List
              bounces={false}
              contentContainerStyle={styles.saleThumb}
              dataArray={dataSaleThumb}
              renderRow={item =>
                <SaleThumb
                  navigation={navigation}
                  blockHeight={deviceHeight / 3 - 45}
                  blockWidth={deviceWidth / 3 - 10}
                  onPress={() => navigation.navigate("ProductList")}
                  saleData={item.text}
                  imageSource={item.imageSaleThumb}
                />}
            />
          </View>
          <SaleFlex imageFlexSource={imageFlex[1]} />
          <SaleTitle saleTitle="Shop Categories" saleSubTitle="30 - 70% Off" />
          <View>
            <List
              bouces={false}
              contentContainerStyle={styles.saleThumb}
              dataArray={dataSaleThumb}
              renderRow={item =>
                <SaleThumb
                  navigation={navigation}
                  blockHeight={deviceHeight / 3 - 45}
                  blockWidth={deviceWidth / 3 - 10}
                  imageSource={item.imageSaleThumb}
                  saleData={item.text}
                />}
            />
          </View>
          <SaleTitle saleTitle="Shop More Brands" />
          <View>
            <List
              bounces={false}
              contentContainerStyle={styles.saleThumb}
              dataArray={dataSaleBrand}
              renderRow={item =>
                <SaleBrandThumb
                  saleData={item.text}
                  imageSource={item.imageSaleThumb}
                  navigation={navigation}
                />}
            />
          </View>
        </Content>
        <MyFooter navigation={navigation} selected={"home"} />
      </Container>
    );
  }
}

export default Product;
