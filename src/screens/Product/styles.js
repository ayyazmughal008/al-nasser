import { StyleSheet } from "react-native";

export default StyleSheet.create({
  saleThumb: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
    padding: 5,
    paddingTop: 0,
    marginLeft: 4,
    marginRight: 4
  }
});
