import React, { Component } from "react";
import { ScrollView, Image, FlatList } from "react-native";
import { connect } from "react-redux";
import { updateBrowsePosition } from "../../redux/actions";
import { View, Text, Container, Content, Card, CardItem } from "native-base";
import BannerSlider from "../../components/BannerSlider/index.js";
import ListDropdown from "../../components/ListDropdown";
import CaetgoryHeader from "../../components/CaetgoryHeader";
import styles from "./styles.js";
import data from "../HomePage/data";
import { fetchWishListData, fetchCategoryBanner } from "../../redux/actions";

const headers = new Headers();
headers.append(
  "Authorization",
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

// @inject("view.app", "domain.user", "app", "routerActions")
// @observer
class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryDrop: null,
      searchMethods: null,
      isRefresh: false
    };
  }

  componentWillMount() {
    var topId = this.props.browsePosition.topCategoryId.toString();
    const { userId, gId } = this.props.user;
    if (userId || gId) {
      fetchWishListData(userId, gId);
    }
    fetchCategoryBanner(topId);
  }

  handleClick = (id, topId, category) => {
    updateBrowsePosition({
      categoryId: id,
      category: category,
      topCategoryId: topId,
      isCategoryListingPage: false,
      currentPage: 0
    });
    NavigationService.navigate("Listing");
  };
  categoryDropdown(id) {
    if (this.state.categoryDrop === id) {
      this.setState({ categoryDrop: null });
      this.setState({ arrowDirection: "ios-arrow-dropdown-outline" });
      return;
    }
    this.setState({ categoryDrop: id });
  }
  handleOnPressTargetUrlListing(targetUrl, pName) {
    this.props.navigation.navigate("ProductList", {
      targetUrl: targetUrl,
      productName: pName
    });
  }
  handleOnPressTargetCID(id, pName) {
    console.log("handleOnPressTargetCID,", id);
    this.props.navigation.navigate("ProductList", {
      cid: id,
      productName: pName
    });
  }
  handleOnPressProductPage(targetId, imagePath) {
    console.log("handleOnPressProductPage,", targetId);
    this.props.navigation.navigate("ProductPage", {
      Name: "New Product",
      TargetId: targetId,
      ProductImage: imagePath,
      senderPage: "home"
    });
  }
  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    let allCategories = [];
    const navigation = this.props.navigation;
    allCategories = this.props.homepage.subCategories;
    const { topSlider, bottomBanner } = this.props.homepage;
    var topId = this.props.browsePosition.topCategoryId.toString();
    const subCategories = allCategories.filter(
      cat => cat.category_id === topId
    );

    const { cartItems, wishListLength } = this.props.user;
    let amount = cartItems.cart ? cartItems.cart.amount + "" : "0";
    var { searchMethods, isRefresh } = this.state;

    return (
      <Container>
        <CaetgoryHeader
          navigation={this.props.navigation}
          textInput="SearchProducts"
          PageTitle="Search on AL NASSER"
          IconRight="ios-heart-empty"
          IconRight2="ios-cart"
          itemCount={amount === "undefined" ? "0" : amount}
          itemWish={wishListLength !== null ? wishListLength : "0"}
        />
        <ScrollView>
          <View style={{ flex: 1 }}>
            {topSlider !== undefined
              ? topSlider.length !== 0
                ? <Card transparent>
                    <FlatList
                      //key = {widget.name}
                      removeClippedSubviews={false}
                      directionalLockEnabled={false}
                      horizontal={true}
                      keyExtractor={this._keyExtractor}
                      showsHorizontalScrollIndicator={false}
                      bounces={false}
                      data={topSlider[0].image_paths}
                      renderItem={({ item }) =>
                        <BannerSlider
                          imageStyle={styles.bannerSliderImage}
                          onPress={() => navigation.navigate("ProductList")}
                          onPress={
                            // () => this.handleOnPressTargetUrlListing(item.target_url)
                            item.destination_type === "C" ||
                            item.destination_type === "P"
                              ? item.destination_type === "C"
                                ? () =>
                                    this.handleOnPressTargetCID(
                                      item.target_id,
                                      topSlider[0].name
                                    )
                                : () =>
                                    this.handleOnPressProductPage(
                                      item.target_id,
                                      item.absolute_path
                                    )
                              : () =>
                                  this.handleOnPressTargetUrlListing(
                                    item.target_url,
                                    topSlider[0].name
                                  )
                          }
                          bannerImageSource={item.image_path}
                          //bannerSmallText={"dummy"}
                        />}
                    />
                  </Card>
                : <View />
              : <View />}

            <Card>
              <CardItem >
                <ListDropdown
                  navigation={navigation}
                  data={subCategories[0].subcategories}
                  topId={topId}
                />
              </CardItem>
            </Card>

            {bottomBanner !== undefined
              ? bottomBanner.length !== 0
                ? <Card transparent>
                    <FlatList
                      //key = {widget.name}
                      removeClippedSubviews={false}
                      directionalLockEnabled={false}
                      horizontal={true}
                      keyExtractor={this._keyExtractor}
                      showsHorizontalScrollIndicator={false}
                      bounces={false}
                      data={bottomBanner[0].image_paths}
                      renderItem={({ item }) =>
                        <BannerSlider
                          imageStyle={styles.bannerSliderImage}
                          onPress={() => navigation.navigate("ProductList")}
                          onPress={
                            // () => this.handleOnPressTargetUrlListing(item.target_url)
                            item.destination_type === "C" ||
                            item.destination_type === "P"
                              ? item.destination_type === "C"
                                ? () =>
                                    this.handleOnPressTargetCID(
                                      item.target_id,
                                      bottomBanner[0].name
                                    )
                                : () =>
                                    this.handleOnPressProductPage(
                                      item.target_id,
                                      item.absolute_path
                                    )
                              : () =>
                                  this.handleOnPressTargetUrlListing(
                                    item.target_url,
                                    topSlider[0].name
                                  )
                          }
                          bannerImageSource={item.image_path}
                          //bannerSmallText={"dummy"}
                        />}
                    />
                  </Card>
                : <View />
              : <View />}
          </View>
        </ScrollView>
      </Container>
    );
  }
}
const mapStateToProps = state => ({
  homepage: state.homepage,
  browsePosition: state.browsePosition,
  user: state.user
});
export default connect(mapStateToProps, {
  fetchWishListData,
  fetchCategoryBanner
})(Category);
