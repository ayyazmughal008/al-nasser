import commonColor from "../../theme/variables/commonColor";

export default {
  clearAll: {
    fontSize: 12,
    fontWeight: "400",
    color: commonColor.brandDanger
  },
  contactListItem: {
    color: commonColor.brandPrimary,
    fontSize: 18
  }
};
