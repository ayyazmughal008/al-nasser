import React, { Component } from "react";
import {
  Text,
  ImageBackground,
  Image,
  Linking,
  Platform,
  FlatList,
  Alert
} from "react-native";
import {
  Container,
  Button,
  List,
  ListItem,
  Content,
  Thumbnail,
  Card,
  View,
  CardItem,
  Left,
  Right,
  Body,
  Header,
  Icon,
  Title,
  Grid,
  Footer,
  Col
} from "native-base";
// import { observer, inject } from "mobx-react/native";
import BaseHeader from "../../components/BaseHeader";

// @inject("view.app", "domain.user", "app", "routerActions")
// @observer
class PaymentSuccess extends Component {
  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const navigation = this.props.navigation;
    const order_id = navigation.getParam("id", "-1");
    const isSuccess = navigation.getParam("isSuccess", false);
    console.log("Order ID received in success page", order_id);
    var userfeadbackimages = [
      {
        id: 1,
        img: require("../../../assets/smily_1.png")
      },
      {
        id: 2,
        img: require("../../../assets/smily_2.png")
      },
      {
        id: 3,
        img: require("../../../assets/smily_3.png")
      },
      {
        id: 4,
        img: require("../../../assets/smily_4.png")
      },
      {
        id: 5,
        img: require("../../../assets/smily_5.png")
      }
    ];
  

    return (
      <Container>
        <Header
          noShadow
          style={{
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
            borderRadius: 0,
            backgroundColor: "black",
            marginBottom: 0
          }}
        >
          <Left>
            {
              <Button
                transparent
                onPress={() => navigation.navigate("HomePage")}
                //onPress={() => this.props.navigation.goBack()}
              >
                {/* <Image
                      source={require("../../../assets/arrow_white.png")}
                      resizeMode = "contain"
                      style={{height:23,width:40,marginRight:10}}
                    /> */}
                <Icon name="ios-arrow-back" style={{ color: "white" }} />
              </Button>
            }
          </Left>
          <Body>
            <Title
              style={{
                color: "white",
                fontSize: 16,
                fontWeight: "400",
                alignSelf: "flex-start"
              }}
            >
              Order Confirmation
            </Title>
          </Body>
        </Header>

        <Content>
          <Card
            style={{
              justifyContent: "center",
              alignItems: "center",
              paddingTop: 15,
              paddingBottom: 15
            }}
            transparent
          >
            <Image
              style={{ width: 130, height: 130 }}
              resizeMode="cover"
              source={require("../../../assets/base_image_4.png")}
            />
            {/* <Thumbnail large square size={100} source={require('../../../assets/base_image_4.png')} /> */}
            {isSuccess
              ? <Text
                  style={{ color: "green", fontSize: 18, fontWeight: "400" }}
                >
                  CONGRATULATIONS!
                </Text>
              : <Text style={{ color: "red", fontSize: 18, fontWeight: "400" }}>
                  Payment Failed
                </Text>}
            <Text>Your order is placed successfully. Thank you for </Text>
            <Text>shopping at AL NASSER.pk</Text>
          </Card>

          <Card transparent>
            <CardItem>
              <Text>How likely are you to recommend AL NASSER to others? </Text>
            </CardItem>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <FlatList
                contentContainerStyle={{ flexDirection: "row" }}
                directionalLockEnabled={false}
                scrollEnabled={false}
                horizontal={true}
                keyExtractor={this._keyExtractor}
                showsHorizontalScrollIndicator={false}
                data={userfeadbackimages}
                renderItem={({ item }) =>
                  // <CardItem >
                  //     <Image source={item.img} style = {{height:40,width:40,}} />
                  // </CardItem>
                  <Button
                    transparent
                    onPress={() =>
                      Alert.alert(
                        "Thanks for the shopping",
                        "Please rate on play store further.",
                        [
                          {
                            text: "Cancel",
                            onPress: () => console.log("Cancel Pressed"),
                            style: "cancel"
                          },
                          {
                            text: "OK",
                            onPress: () => {
                              Platform.OS === "ios"
                                ? Linking.openURL(`https://itunes.apple.com/sa/app/clicky-online-shopping/id1448160644?mt=8`)
                                : Linking.openURL(
                                    `https://play.google.com/store/apps/details?id=com.clicky.pk`
                                  );
                            }
                          }
                        ],
                        { cancelable: false }
                      )}
                  >
                    <Image
                      source={item.img}
                      style={{ height: 40, width: 40 }}
                    />
                  </Button>}
              />
            </View>

            <CardItem
              style={{
                flex: 1,
                justifyContent: "space-between",
                flexDirection: "row"
              }}
            >
              <Text>LOW</Text>
              <Text>HIGH</Text>
            </CardItem>
          </Card>
        </Content>
        <Footer style={{ position: "absolute", bottom: 0, height: 100 }}>
          <Card transparent style={{ flex: 1, padding: 5 }}>
            <Grid>
              <Col size={3}>
                <Text style={{ marginBottom: 5, marginTop: 5 }}>
                  ORDER SUMMARY
                </Text>
                <Text style={{ marginBottom: 5, marginTop: 5 }}>Order ID</Text>
              </Col>
              <Col size={2}>
                {/* <Button
                  transparent
                  small
                  style={{justifyContent:'space-between'}}
                  //onPress={() => navigation.navigate("CashonDelivery")}
                > */}
                <Text style={{ marginBottom: 5, marginTop: 5 }}>DETAILS</Text>
                <Text style={{ marginBottom: 5, marginTop: 5 }}>
                  {order_id}
                </Text>
                {/* </Button> */}
              </Col>
            </Grid>
          </Card>
        </Footer>
      </Container>
    );
  }
}
export default PaymentSuccess;
