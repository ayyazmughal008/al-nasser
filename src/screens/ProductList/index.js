import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  FlatList,
  ActivityIndicator,
  Dimensions,
  
} from "react-native";
import {
  Container,
  
  Text,
  
} from "native-base";
import ListThumb from "../../components/ListThumb";
import SortRefine from "../../components/SortRefine";
import CaetgoryHeader from "../../components/CaetgoryHeader";
import { fetchWishListData } from "../../redux/actions";

const headers = new Headers();
headers.append(
  "Authorization",
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");
var category_id = "0";
var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

//@inject("routerActions")
class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clear: true,
      loading: true,
      cid: null,
      data: [],
      filter: [],
      variant: [],
      page: 1,
      seed: 1,
      error: null,
      refereshing: false,
      isLoadingCustomUrl: false,
      customUrl: null,
      sorting: [],
      modalVisible: false,
      sortby: "",
      orderby: "",
      featureHashes: "",
      searchMethods: null,
      isRefresh: false,
      SavedData: "",
      SelectedRefine: "",
      param:null
    };
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  DATAToBeSave = (AllString, RefineType) =>
    this.setState({
      SavedData: AllString,
      SelectedRefine: RefineType
    });
  clearSavedData = () =>{
    this.setState({
      SavedData: "",
      SelectedRefine: "",
      featureHashes:""
    },);setTimeout(()=>{ this.makeRemoteRequest(); }, 500);
  };
  componentWillMount() {
    const { userId, gId } = this.props.user;
    const { navigation } = this.props;
    var targetUrl = navigation.getParam("targetUrl");
    var targetCid = navigation.getParam("cid");
    if (targetUrl)
      this.setState({ isLoadingCustomUrl: true, customUrl: targetUrl });
    if (targetCid) this.setState({ isLoadingCustomUrl: true, cid: targetCid });
    if (userId || gId) {
      fetchWishListData(userId,gId);
    }

    
  }
  componentDidMount() {
    this.makeRemoteRequest();
    this.fetchFiltersData();
  }

  makeHashesFilter = (sortby, orderby, featureHashes) => {
    // console.log(
    //   "Inside Product Listing: category id " + this.props.browsePosition
    // );
    let { page, isLoadingCustomUrl, customUrl, seed, cid } = this.state;
    var url = null;
    page = 1;
    if (isLoadingCustomUrl) {
      if (customUrl)
        url = `https://www.alnasser.pk/api/Extendedproducts/?${customUrl}&sort_by=${sortby}&sort_order=${orderby}&features_hash=${featureHashes}`;
      else if (cid)
        url = `https://www.alnasser.pk/api/Extendedproducts/?cid=${cid}&page=${page}&sort_by=${sortby}&sort_order=${orderby}&features_hash=${featureHashes}`;
    } else {
      url = `https://www.alnasser.pk/api/Extendedproducts/?cid=${category_id}&page=${page}&sort_by=${sortby}&sort_order=${orderby}&features_hash=${featureHashes}`;
    }

    console.log("Going to fecth URL = " + url);
    this.setState({ loading: true });
    fetch(url, {
      headers: headers
    })
      .then(res => res.json())
      .then(res => {
        // console.log(res);
        this.setState({
          data: res.products,
          sorting: res.sorting,
          error: res.error || null,
          param: res.params.total_items,
          loading: false,
          refereshing: false,
          page,
          orderby,
          sortby,
          featureHashes
        });
      })
      .catch(error => {
        this.setState({ error, loading: false, refereshing: false });
      });
  };

  fetchFiltersData = () => {
    const { isLoadingCustomUrl, cid } = this.state;
    var url = null;

    if (isLoadingCustomUrl) {
      if (cid)
        url = `https://www.alnasser.pk/api/Filters?scope=category_product_filter&cid=${cid}`;
    } else {
      url = `https://www.alnasser.pk/api/Filters?scope=category_product_filter&cid=${category_id}`;
    }

    console.log("Going to fecth URL = " + url);
    this.setState({ loading: true });
    fetch(url, {
      headers: headers
    })
      .then(res => res.json())
      .then(res => {
        // let obj = res.filters;
        // var result = Object.values(obj);
        // console.log(result);
        //console.log(res)
        this.setState({
          filter: res.filters
          // variant:result.variants
        });
      })
      .catch(error => {
        this.setState({ error });
      });
  };

  makeRemoteRequest = () => {
    // console.log(
    //   "Inside Product Listing: category id " + this.props.browsePosition
    // );
    const {
      page,
      isLoadingCustomUrl,
      customUrl,
      seed,
      cid,
      sortby,
      orderby,
      featureHashes
    } = this.state;
    var url = null;
    if (isLoadingCustomUrl) {
      if (customUrl)
        url = `https://www.alnasser.pk/api/Extendedproducts/?${customUrl}&page=${page}&sort_by=${sortby}&sort_order=${orderby}&features_hash=${featureHashes}`;
      else if (cid)
        url = `https://www.alnasser.pk/api/Extendedproducts/?cid=${cid}&page=${page}&sort_by=${sortby}&sort_order=${orderby}&features_hash=${featureHashes}`;
    } else {
      url = `https://www.alnasser.pk/api/Extendedproducts/?cid=${category_id}&page=${page}&sort_by=${sortby}&sort_order=${orderby}&features_hash=${featureHashes}`;
    }

    console.log("Going to fecth URL = " + url);
    this.setState({ loading: true });
    fetch(url, {
      headers: headers
    })
      .then(res => res.json())
      .then(res => {
        // console.log(res);
        this.setState({
          data: [...this.state.data, ...res.products],
          sorting: [...this.state.sorting, ...res.sorting],
          param: res.params.total_items,
          error: res.error || null,
          loading: false,
          refereshing: false
        });
      })
      .catch(error => {
        this.setState({ error, loading: false, refereshing: false });
      });
  };

  makeRemotSortingRequest = (sortby, orderby) => {
    // console.log(
    //   "Inside Product Listing: category id " + this.props.browsePosition
    // );
    const { featureHashes } = this.state;
    let { page, isLoadingCustomUrl, customUrl, seed, cid } = this.state;
    var url = null;
    page = 1;
    if (isLoadingCustomUrl) {
      if (customUrl)
        url = `https://www.alnasser.pk/api/Extendedproducts/?${customUrl}&sort_by=${sortby}&sort_order=${orderby}&features_hash=${featureHashes}`;
      else if (cid)
        url = `https://www.alnasser.pk/api/Extendedproducts/?cid=${cid}&page=${page}&sort_by=${sortby}&sort_order=${orderby}&features_hash=${featureHashes}`;
    } else {
      url = `https://www.alnasser.pk/api/Extendedproducts/?cid=${category_id}&page=${page}&sort_by=${sortby}&sort_order=${orderby}&features_hash=${featureHashes}`;
    }

    //console.log("Going to fecth URL = " + url);
    this.setState({ loading: true });
    fetch(url, {
      headers: headers
    })
      .then(res => res.json())
      .then(res => {
        // console.log(res);
        this.setState({
          data: res.products,
          sorting: res.sorting,
          error: res.error || null,
          param: res.params.total_items,
          loading: false,
          refereshing: false,
          page,
          orderby,
          sortby
        });
      })
      .catch(error => {
        this.setState({ error, loading: false, refereshing: false });
      });
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  handleReferesh = () => {
    this.setState(
      {
        page: 1,
        refereshing: true,
        seed: this.state.seed + 1,
        sortby: "",
        orderby: "",
        featureHashes: ""
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  renderFooter = () => {
    if (!this.state.loading) return null;
    return (
      <View
        style={{ paddingVertical: 20,}}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
  renderSeparator() {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "black"
        }}
      />
    );
  }
  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    let { customUrl, SavedData, SelectedRefine,param } = this.state;
    const navigation = this.props.navigation;
    const { cartItems, wishListLength } = this.props.user;
    let amount = cartItems.cart ? cartItems.cart.amount + "" : "0";
    var { searchMethods, isRefresh } = this.state;

    var productName = navigation.getParam("productName", "Product Listing");
    let pName;
    if (productName === undefined) {
      pName = "Product Listing";
    } else {
      pName = productName;
    }
    // console.log("Product name", productName);
    category_id = this.props.browsePosition.categoryId;
    //category_name = this.props.browsePosition.category.category;

    return (
      <Container>
       <CaetgoryHeader
              navigation={this.props.navigation}
              // drawerOpen="DrawerOpen"
              // IconLeft="ios-menu"
              textInput="SearchProducts"
              PageTitle="Search on AL NASSER"
              IconRight="ios-heart-empty"
              IconRight2="ios-cart"
              itemCount={amount === "undefined" ? "0" : amount}
              itemWish={wishListLength !== null ? wishListLength : "0"}
            />

        {/* {!customUrl &&
          <SortRefine
              sortingVeriable={this.state.sorting}
              sortingFunction={this.makeRemotSortingRequest}
              filterVariable={this.state.filter}
              hashesVariable={this.makeHashesFilter}
              DATAToBeSave={this.DATAToBeSave}
              clearSavedData={this.clearSavedData}
              RefineType={SelectedRefine}
              RefineData={SavedData}
            />}
            {param &&
            <View style={{justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:10,fontWeight:'200'}}> Total products: {param}</Text>
            </View>
            } */}

        {/* <List bounces={false} contentContainerStyle={styles.listThumb}> */}
          <FlatList
            data={this.state.data}
            numColumns={2}
            keyExtractor={this._keyExtractor}
            renderItem={({ item }) =>
              <ListThumb
                navigation={navigation}
                brand={item.brand}
                price={item.list_price}
                discount={item.discount}
                discountedPrice={item.formatprice}
                description={item.description}
                imageSource={item.images[0]}
                clickHandler={() =>
                  navigation.navigate("ProductPage", {
                    Name: item.product,
                    TargetId: item.product_id,
                    senderPage: 'notHome'
                  })}
              />}
            // keyExtractor={item => item.product_id}
            ListFooterComponent={this.renderFooter}
            refreshing={this.state.refereshing}
            onRefresh={this.handleReferesh}
            onEndReached={this.handleLoadMore}
            onEndThreshold={0}
          />
        {/* </List> */}
      </Container>
    );
  }
}

//export default ProductList;
const mapStateToProps = state => ({
  browsePosition: state.browsePosition,
  user: state.user
});

export default connect(mapStateToProps,{fetchWishListData})(ProductList);
