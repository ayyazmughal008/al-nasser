import React, { Component } from "react";
import { connect } from "react-redux";
import { updateAddrss, addNewAddress } from "../../redux/actions";
import { StyleSheet,Image,ActivityIndicator } from "react-native";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import ToastModal from "../../components/ToastModal";
import { parsePhoneNumber } from "libphonenumber-js";
import {
  Container,
  Content,
  Button,
  Input,
  Text,
  Form,
  Item,
  Spinner,
  Header,
  Left,
  Right,
  Body,
  Title,
  Icon,
  View
} from "native-base";
// import { observer, inject } from "mobx-react/native";
import ThemeHeader from "../../components/Header/index.js";

const headers = new Headers();
headers.append(
  "Authorization",
  "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

class UpdateAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      phone: "",
      city: "",
      cityState: "",
      country: "Pakistan",
      address: "",
      isFormValid: false,
      signupStarted: false,
      firstNameError: null,
      lastNameError: null,
      phoneError: null,
      cityError: null,
      cityStateError: null,
      addressError: null,
      text: "",
      profileId: null,
      spinner: false,
      InternationalFormatcell: "",
      NumberValidation: true
    };
  }

  logData(identifier, data) {
    console.log("Logger - ", identifier, data);
  }

  componentWillMount() {
    console.log("Cash on Deleivery Screen");
    this.fetchuserProfile();
  }

  fetchuserProfile() {
    const { userId } = this.props.user;
    fetchProfileUrl = `https://www.alnasser.pk/api//MultipleProfiles/?user_id=${userId}`;
    console.log("going to fetch URL => \n", fetchProfileUrl);
    fetch(fetchProfileUrl, { headers: headers })
      .then(res => res.json())
      .then(json => {
        let obj = json.users;
        obj.map((item, i) => {
          if (i == 0) {
            this.setState({
              profileId: item.profile_id
            });
          }
        });
      });
  }

  onPressSignup(isProfile) {
    if (!this.validateFirstname()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateLastname()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateAddress()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateCity()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateState()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validatePhone()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateAddressType()) {
      this.setState({ isFormValid: false });
      return;
    }
    
      const {
        firstName,
        lastName,
        address,
        city,
        cityState,
        InternationalFormatcell,
        text,
        profileId
      } = this.state;
      const { userId } = this.props.user;
      console.log("Valid Form. Triggering Update Address");
      updateAddrss(
        profileId,
        firstName,
        lastName,
        city,
        InternationalFormatcell,
        cityState,
        address,
        userId,
        text,
        isProfile
      );
      this.setState({ signupStarted: true });
    
  }

  // onPressNewAddress(isProfile) {
  //   this.validateForm();
  //   if (this.state.isFormValid) {
  //     const {firstName,lastName,address,city,cityState,phone,text,profileId} = this.state;
  //     const { userId } = this.props.user;
  //     console.log(firstName+lastName+address+city+cityState+phone+text+profileId);

  //     addNewAddress(profileId,firstName,lastName,city,phone,cityState,address,userId,text,isProfile)
  //       this.setState({ signupStarted: true });
  //   }
  // }

  validateForm() {
    const { phone } = this.state;
    if (!this.validateFirstname()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateLastname()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateAddress()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateCity()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateState()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validatePhone()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validateAddressType()) {
      this.setState({ isFormValid: false });
      return;
    }
    this.setState({ isFormValid: true });
  }

  mobilevalidate(text) {
    const reg = /^[0]?[789]\d{9}$/;
    if (reg.test(text) === false) {
      return false;
    } else {
      this.setState({
        phone: text
      });
      return true;
    }
  }

  validateFirstname() {
    const { firstName } = this.state;
    if (!firstName) {
      console.log("First Name is not Valid");
      alert("First Name is not Valid");

      <ToastModal Toast="First Name is not Valid" />;
      this.setState({ firstNameError: "First Name is not Valid" });
      return false;
    }
    return true;
  }

  validateLastname() {
    const { lastName } = this.state;
    if (!lastName) {
      console.log("Last Name is not Valid");

      <ToastModal Toast="Last Name is not Valid" />;
      alert('"Last Name is not Valid"');
      this.setState({ lastNameError: "Last Name is not Valid" });
      return false;
    }
    return true;
  }

  validateAddress() {
    const { address } = this.state;
    if (!address) {
      console.log("Address is not Valid");

      <ToastModal Toast="Address is not Valid" />;
      alert("Address is not Valid");
      this.setState({ addressError: "Address is not Valid" });
      return false;
    }
    return true;
  }

  validateCity() {
    const { city } = this.state;
    if (!city) {
      console.log("City is not Valid");

      <ToastModal Toast="City is not Valid" />;
      alert("City is not Valid");
      this.setState({ cityError: "City name is not Valid" });
      return false;
    }
    return true;
  }
  validateState() {
    const { cityState } = this.state;
    if (!cityState) {
      console.log("State is not Valid");

      <ToastModal Toast="State is not Valid" />;
      alert("State is not Valid");
      this.setState({ cityStateError: "State name is not Valid" });
      return false;
    }
    return true;
  }
  validatePhone() {
    const { phone } = this.state;
    if (
      this.state.NumberValidation &&
      this.state.InternationalFormatcell != "" &&
      this.state.phone != ""
    ) {
      return true;
    } else {
      console.log("Phone Number is not Valid");
      <ToastModal Toast="Phone Number is not Valid" />;
      alert("Phone Number is not Valid\n write in that format: 3011234567");
      this.setState({ phoneError: "Phone Number is not Valid" });
    }
    return false;
  }
  validateAddressType() {
    const { text } = this.state;
    if (!text) {
      console.log("Address Type not Selected");

      <ToastModal Toast="Address Type not Selected" />;
      alert("Address Type not Selected");
      this.setState({ phoneError: "Address Type not Selected" });
      return false;
    }
    return true;
  }

  onSelect(index, value) {
    this.setState({
      text: value
    });
  }

  render() {
    const navigation = this.props.navigation;
    const isProfile = navigation.getParam("isProfile", false);
    const { showScreenLoader } = this.props.screenState;
    // const { userId,userProfile } = this.props.user;
    // let obj = userProfile;
    //     obj.map((item, i) => {
    //       if (i == 0) {
    //         this.setState({
    //           profileId: item.profile_id,
    //         });
    //       }
    //     });

    const {
      signupStarted,
      isFormValid,
      country,
      firstNameError,
      lastNameError,
      addressError,
      cityError,
      cityStateError,
      phoneError
    } = this.state;
    //if(showScreenLoader){this.toggleSingupStarted(true)} else this.toggleSingupStarted(false)
    const { signupError } = this.props.user;
    //console.log("Screenloader, ", showScreenLoader);
    //console.log("isFormValid,", isFormValid);
    return (
      <Container>
        {/* <ThemeHeader
          PageTitle="SIGN UP"
          IconLeft="arrow-back"
          route="loginHome"
          navigation={navigation}
        /> */}
        <Header noShadow
          style={{
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
            borderRadius: 0,
            backgroundColor: "white",
            marginBottom: 0
          }}
        >
          <Left>
            {
              <Button
                transparent
                //onPress={() => navigation.navigate("HomePage")}
                onPress={() => this.props.navigation.goBack()}
              >
                 {/* <Image
                      source={require("../../../assets/arrow_black.png")}
                      resizeMode = "contain"
                      style={{height:23,width:40,marginRight:10}}
                    /> */}
                    <Icon name="ios-arrow-back"/>
              </Button>
            }
          </Left>

          <Body>
            <Title
              style={{
                color: "black",
                fontSize: 16,
                fontWeight: "400",
                alignSelf: "flex-start"
              }}
            >
              Change Address
            </Title>
          </Body>
        </Header>

        <Content
          padder
          style={{ backgroundColor: "#fff", marginBottom: null }}
          bounces={false}
        >
          {/* {
          showScreenLoader===true &&
            <Spinner color='green' />
        } */}
          <Form>
            {/* <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder="Email address"
                autoCapitalize="none"
                onChangeText={email => this.setState({ email })}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder="Password"
                secureTextEntry
                onChangeText={password1 => this.setState({ password1 })}
              />
            </Item> */}
            <Item
              style={{
                marginLeft: 0,
                justifyContent: "space-between",
                flexDirection: "row"
              }}
            >
              <Input
                placeholder="First Name"
                onChangeText={firstName => this.setState({ firstName })}
              />
              <Input
                placeholder="Last Name"
                onChangeText={lastName => this.setState({ lastName })}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder="Address"
                onChangeText={address => this.setState({ address })}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder={this.state.country}
                //onChangeText={country => this.setState({ country })}
                editable={false}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder="City"
                onChangeText={city => this.setState({ city })}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                placeholder="State (e.g Punjab)"
                onChangeText={cityState => this.setState({ cityState })}
              />
            </Item>
            <Item style={{ marginLeft: 0 }}>
              <Input
                ref="mobileNo"
                maxLength={11}
                keyboardType="phone-pad"
                placeholder="Phone"
                prefix={"+92"}
                onChangeText={phone => {
                  try {
                    const NumberWeWant = parsePhoneNumber(phone, "PK");
                    this.setState({
                      NumberValidation: NumberWeWant.isValid(),
                      phone,
                      InternationalFormatcell: NumberWeWant.isValid()
                        ? NumberWeWant.formatInternational()
                        : ""
                    });
                  } catch (e) {
                    this.setState({
                      NumberValidation: false,
                      phone,
                      InternationalFormatcell: ""
                    });
                  }

                  // this.setState({ phone })
                }}
              />
            </Item>
            <RadioGroup
              onSelect={(index, value) => this.onSelect(index, value)}
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                marginTop: 10
              }}
            >
              <RadioButton value={"Home Address"}>
                <Text>Home Address</Text>
              </RadioButton>

              <RadioButton value={"Office Address"}>
                <Text>Office Address</Text>
              </RadioButton>
            </RadioGroup>
            <Button
              primary
              block
              //onPress={() => navigation.navigate("Profile")}
              onPress={() => this.onPressSignup(isProfile)}
              style={{ marginTop: 30 }}
            >
              <Text> UPDATE ADDRESS </Text>
              {this.props.loader.showLoader &&
              <ActivityIndicator
                style={{ position: "absolute", zIndex: 2, right: "2%" }}
                size="large"
                color="#fff"
              />}
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}
//export default SignUp;
const mapStateToProps = state => ({
  user: state.user,
  screenState: state.appState,
  loader: state.loader
});
export default connect(mapStateToProps, { updateAddrss, addNewAddress })(
  UpdateAddress
);

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: "red"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
