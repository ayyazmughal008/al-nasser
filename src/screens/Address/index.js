import React from "react";
import {
  Container,
  Content,
  List,
  Body,
  Header,
  Left,
  Title,
  Text,
  Button,
  Icon,
  Right
} from "native-base";
import { View, FlatList } from "react-native";
import ThemeHeader from "../../components/Header";
import { connect } from "react-redux";
import AddressList from "../../components/AddressList";
import { createStackNavigator, createAppContainer } from "react-navigation";

class Address extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const navigation = this.props.navigation;
    const {} = this.state;
    var { userHomeAddress, userOfficeAddress, userProfile } = this.props.user;
    //console.log("inside Addressess screen\n", userHomeAddress);

    return (
      <Container>
        {/* <ThemeHeader
          PageTitle={"Addresses"}
          IconLeft="arrow-back"
          myDiv={this.props.navigation}
        /> */}
        <Header
          style={{
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
            borderRadius: 0,
            backgroundColor: "white",
            marginBottom: 0
          }}
        >
          <Left>
            {
              <Button
                transparent
                //onPress={() => navigation.navigate('DrawerOpen')}
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon
                  style={{ padding: 5, marginLeft: -5, color: "black" }}
                  name="ios-arrow-back"
                />
              </Button>
            }
          </Left>
          <Body>
            <Title
              style={{
                color: "black",
                fontSize: 16,
                fontWeight: "400",
                alignSelf: "flex-start"
              }}
            >
              {/* {category_name} */}
              Addresses
            </Title>
          </Body>
        </Header>
        {userProfile !== null &&
        <AddressList
          navigation={navigation}
          userAddressType="Shipping Detail"
          firstName={userProfile.s_firstname}
          lastName={userProfile.s_lastname}
          userAddress={userProfile.s_address}
          userCity={userProfile.s_city}
          userCountery={userProfile.s_country_descr}
          userState={userProfile.s_state}
          userPhone={userProfile.s_phone}
          isProfile={true}
        />
        }
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps)(Address);
