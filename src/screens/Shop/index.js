import React, { Component } from "react";
import { Text, Image, TouchableOpacity } from "react-native";
import { Container, Content } from "native-base";
// import { observer, inject } from "mobx-react/native";
import ListDropdown from "../../components/ListDropdown";
import ThemeHeader from "../../components/Header/index.js";
import MyFooter from "../../components/Footer";
import styles from "./styles.js";

// @inject("view.app", "domain.user", "app", "routerActions")
// @observer
class Shop extends Component {
  render() {
    const navigation = this.props.navigation;
    var ListDropdownData = [
      {
        id: 1,
        value: "Topwear",
        sublist: ["T-Shirts", "Casual Shirts", "Formal Shirts"]
      },
      {
        id: 2,
        value: "Bottomwear",
        sublist: ["Jeans", "Casual Trousers", "Shorts", "Track Pants"]
      },
      {
        id: 3,
        value: "Sports & Active Wear",
        sublist: ["Active T-Shirts", "Track Pants", "Sport Pants"]
      },
      {
        id: 4,
        value: "Indian & Festive Wear",
        sublist: ["Active T-Shirts", "Track Pants", "Sport Pants"]
      },
      {
        id: 5,
        value: "Plus Size",
        sublist: ["Active T-Shirts", "Track Pants", "Sport Pants"]
      },
      {
        id: 6,
        value: "Footwear",
        sublist: ["Active T-Shirts", "Track Pants", "Sport Pants"]
      }
    ];
    return (
      <Container>
        <ThemeHeader
          PageTitle="SHOP MEN'S"
          IconLeft="arrow-back"
          IconRight="search"
          route="homepage"
          navigation={navigation}
        />
        <Content
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 10 }}
          style={{ backgroundColor: "#fff", marginBottom: 50 }}
        >
          <TouchableOpacity onPress={() => navigation.navigate("ProductList")}>
            <Image
              style={styles.bannerImage}
              source={require("../../../assets/18.png")}
            />
            <Text style={styles.heading}>Winter Style Sorted</Text>
            <Text style={styles.subHeading}>Hand-Picked Authenticity</Text>
          </TouchableOpacity>
          <ListDropdown datas={ListDropdownData} navigation={navigation} />
        </Content>
        <MyFooter navigation={navigation} selected={"home"} />
      </Container>
    );
  }
}
export default Shop;
