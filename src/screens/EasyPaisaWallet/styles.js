const commonColor = require("../../theme/variables/commonColor");
export default {
  heading: {
    fontSize: 15,
    fontWeight: "300",
    color: "#333"
  },
};