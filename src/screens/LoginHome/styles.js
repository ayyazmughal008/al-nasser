import { StyleSheet, StatusBar } from "react-native";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../components/MakeMeResponsive/MakeMeResponsive.js";
export default StyleSheet.create({
  LOGO: {
    height: "100%",
    width: "100%"
  },
  LOGOView: {
    height: "15%",
    width: widthPercentageToDP(100),
    backgroundColor: "#FFF"
  },
  cont: {
    flex: 1,
    backgroundColor: "#FFF",
    alignItems: "center"
  },
  Forms: {
    flex: 1,
    paddingVertical: "6%",
    paddingHorizontal: "5%",
    backgroundColor: "#FFF"
  },
  RegisterHeaderText: {
    fontSize: widthPercentageToDP(4),
    fontWeight: "bold",
    marginBottom: "5%",
    marginTop: "10%"
  },
  statusBarBackground: {
    height: StatusBar.currentHeight,
    backgroundColor: "#FFF",
    width: "100%"
  },
  otherThenStatusBarView: { flex: 1, backgroundColor: "#FFF", width: "100%" },
  OtherTHenLOGOView: {
    height: "85%",
    width: widthPercentageToDP(100),
    backgroundColor: "#FFF"
  }
});
