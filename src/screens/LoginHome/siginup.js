"use strict";
import React from "react";
import { View, Text, ScrollView,ActivityIndicator } from "react-native";
import Style from "./styles";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../components/MakeMeResponsive/MakeMeResponsive";
import { connect } from "react-redux";
import { signup } from "../../redux/actions";
import { Button, Icon } from "native-base";
import { TextField } from "react-native-material-textfield";

class SIGNUP extends React.Component {
  state = {
    SignUpEmail: "",
    SignUpPassword: "",
    ConFirmSignUpPassword: "",
    SignUpFirstName: "",
    SignUpLastName: "",
    SignUpPHONE: "",
    phoneError: null,
    firstNameError: null,
    lastNameError: null,
    emailError: null,
    isFormValid: false,
    passwordError: null,
    signupStarted: false,
    text: "",
    genderError: null,
    InternationalFormatcell: "",
    NumberValidation: true
  };

  onPressSignup = () => {
    const { notificationToken } = this.props.user;
    const signupData = {
      email: this.state.SignUpEmail,
      password: this.state.SignUpPassword,
      firstName: this.state.SignUpFirstName,
      lastName: this.state.SignUpLastName,
      phone: this.state.InternationalFormatcell,
      app_token: notificationToken
    };
      if (this.props.user.gId) signupData.g_id = this.props.user.g_id;

       if (!this.validateEmail()) {
        this.setState({ isFormValid: false });
        return;
      }
      if (!this.validatePassword()) {
        this.setState({ isFormValid: false });
        return;
      }
      if (!this.validateFirstname()) {
        this.setState({ isFormValid: false });
        return;
      }
      if (!this.validateLastname()) {
        this.setState({ isFormValid: false });
        return;
      }
        this.props.signup(signupData);
        this.setState({ signupStarted: true });
  }

  // validateForm = async () => {
  //   const VE = await this.validateEmail();
  //   const VP = await this.validatePassword();
  //   const VB = await this.validateFirstname();
  //   const VC = await this.validateLastname();
  //   return VE && VP && VB && VC;
  // };

  // validateForm =() => {
  //   if (!this.validateEmail()) {
  //       this.setState({ isFormValid: false });
  //       return;
  //     }
  //     if (!this.validatePassword()) {
  //       this.setState({ isFormValid: false });
  //       return;
  //     }
  //     if (!this.validateFirstname()) {
  //       this.setState({ isFormValid: false });
  //       return;
  //     }
  //     if (!this.validateLastname()) {
  //       this.setState({ isFormValid: false });
  //       return;
  //     }
  //     this.setState({ isFormValid: true });
  // }

  validatePassword =() => {
    var { passwordError } = this.state;
    const { SignUpPassword, ConFirmSignUpPassword } = this.state;
    if (SignUpPassword !== ConFirmSignUpPassword || !SignUpPassword || !ConFirmSignUpPassword) {
      console.log("Passwords dont match");
      this.setState({ passwordError: "Passwords do not match" });
      alert("Passwords dont match");
      // <ToastModal
      //     Toast = "Passwords dont match"
      //     />
      return false;
    } else if (SignUpPassword === ConFirmSignUpPassword) {
      console.log("Passwords match");
      this.setState({ passwordError: null });

      return true;
    }
  }

  validateEmail() {
    var { emailError } = this.state;
    console.log(this.state.SignUpEmail);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.SignUpEmail) === false) {
      console.log("Email is Not Correct");
      this.setState({ emailError: "Please enter a valid email" });
      alert("Email is Not Correct");
      // <ToastModal
      //     Toast = "Email is Not Correct"
      //     />
      return false;
    } else {
      this.setState({ emailError: null });
      //this.setState({email:text})
      console.log("Email is Correct");
      return true;
    }
  }

  toggleSingupStarted(n) {
    this.setState({ signupStarted: n });
  }

  validateFirstname() {
    var { firstNameError } = this.state;
    const { SignUpFirstName } = this.state;
    if (!SignUpFirstName) {
      console.log("First Name is not Valid");
      alert("First Name is not Valid");
      this.setState({ firstNameError: "First Name is not Valid" });

      // <ToastModal
      //     Toast = "First Name is not Valid"
      //     />
      return false;
    }
    return true;
  }

  validateLastname() {
    var { lastNameError } = this.state;
    const { SignUpLastName } = this.state;
    if (!SignUpLastName) {
      console.log("Last Name is not Valid");
      alert('"Last Name is not Valid"');
      this.setState({ lastNameError: "Last Name is not Valid" });

      //  <ToastModal
      //       Toast = "Last Name is not Valid"
      //       />
      return false;
    }
    return true;
  }

  validatePhone() {
    const { SignUpPHONE } = this.state;
    if (
      this.state.NumberValidation &&
      this.state.InternationalFormatcell != "" &&
      this.state.SignUpPHONE != ""
    ) {
      return true;
    } else {
      console.log("Phone Number is not Valid");
      alert("Phone Number is not Valid\n write in that format: 3011234567");
      this.setState({ phoneError: "Phone Number is not Valid" });
    }
    return false;
  }


  render() {
    const {
      SignUpEmail,
      SignUpPassword,
      ConFirmSignUpPassword,
      SignUpFirstName,
      SignUpLastName,
      SignUpPHONE
    } = this.state;
    const { signupError } = this.props.user;
    return (
      <ScrollView>
        <View style={Style.Forms}>
          <TextField
            value={SignUpEmail}
            keyboardType="email-address"
            labelPadding={1}
            autoCapitalize = "none"
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="EMAIL"
            maxLength={40}
            tintColor={"#111"}
            onChangeText={SignUpEmail =>
              this.setState({
                SignUpEmail
              })}
          />
          <TextField
            value={SignUpPassword}
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="PASSWORD"
            maxLength={40}
            tintColor={"#111"}
            onChangeText={SignUpPassword =>
              this.setState({
                SignUpPassword
              })}
            secureTextEntry={true}
          />
          <TextField
            value={ConFirmSignUpPassword}
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="CONFIRM PASSWORD"
            maxLength={40}
            tintColor={"#111"}
            onChangeText={ConFirmSignUpPassword =>
              this.setState({
                ConFirmSignUpPassword
              })}
            secureTextEntry={true}
          />
          <TextField
            value={SignUpFirstName}
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="FIRST NAME"
            maxLength={40}
            tintColor={"#111"}
            onChangeText={SignUpFirstName =>
              this.setState({
                SignUpFirstName
              })}
          />
          <TextField
            value={SignUpLastName}
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="LAST NAME"
            maxLength={40}
            tintColor={"#111"}
            onChangeText={SignUpLastName =>
              this.setState({
                SignUpLastName
              })}
          />
          <TextField
            value={SignUpPHONE}
            labelPadding={1}
            keyboardType={"numeric"}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="PHONE"
            maxLength={11}
            prefix={"+92"}
            tintColor={"#111"}
            onChangeText={SignUpPHONE => {
                try {
                  const NumberWeWant = parsePhoneNumber(SignUpPHONE, "PK");
                  this.setState({
                    NumberValidation: NumberWeWant.isValid(),
                    SignUpPHONE,
                    InternationalFormatcell: NumberWeWant.isValid()
                      ? NumberWeWant.formatInternational()
                      : ""
                  });
                } catch (e) {
                  this.setState({
                    NumberValidation: false,
                    SignUpPHONE,
                    InternationalFormatcell: ""
                  });
                }

                // this.setState({ phone })
              }}
          />
         
          <Button
            style={{
              width: "100%",
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center"
            }}
            onPress={() => this.onPressSignup()}
            medium
            dark
          >
            <Text style={{ fontSize: widthPercentageToDP(4), color: "#FFF" }}>
              {"SIGNUP"}
            </Text>
            {this.props.loader.showLoader &&
              <ActivityIndicator
                style={{ position: "absolute", zIndex: 2, right: "2%" }}
                size="large"
                color="#fff"
              />}
          </Button>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  screenState: state.appState,
  loader: state.loader
});
export default connect(mapStateToProps, { signup })(SIGNUP);
