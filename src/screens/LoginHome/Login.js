"use strict";
import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Modal,
  Dimensions,
  ActivityIndicator
} from "react-native";
import Style from "./styles";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../components/MakeMeResponsive/MakeMeResponsive";
import { connect } from "react-redux";
import { Button } from "native-base";
import { TextField } from "react-native-material-textfield";
import { login } from "../../redux/actions";

const headers = new Headers();
headers.append(
  "Authorization",
  "Basic " + "YWRtaW5AYWxuYXNzZXIucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

class LOGIN extends React.Component {
  state = {
    LoginEmail: "",
    LoginPassword: "",
    emailError: null,
    passwordError: null,
    isFormValid: false
  };

  onLogin = () => {
    var { LoginEmail, LoginPassword } = this.state;
    var { gId, checkOutStarted } = this.props.user;
    if (!this.validateEmail()) {
      this.setState({ isFormValid: false });
      return;
    }
    if (!this.validatePassword()) {
      this.setState({ isFormValid: false });
      return;
    }
        gId
          ? this.props.login(LoginEmail, LoginPassword, gId, checkOutStarted)
          : this.props.login(LoginEmail, LoginPassword, null, checkOutStarted);  
  };

  // validateForm = async () => {
  //   const VE = await this.validateEmail();
  //   const VP = await this.validatePassword();
  //   return VE && VP;
  // };

  validateEmail = () => {
    var { emailError } = this.state;
    console.log(this.state.LoginEmail);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.LoginEmail) === false) {
      console.log("Email is Not Correct");
      this.setState({ emailError: "Please enter a valid email" });
      alert("Email is Not Correct");
      // <ToastModal
      //     Toast = "Email is Not Correct"
      //     />
      return false;
    } else {
      this.setState({ emailError: null });
      //this.setState({email:text})
      console.log("Email is Correct");
      return true;
    }
  };
  validatePassword = () => {
    const { LoginPassword } = this.state;
    if (!LoginPassword) {
      console.log("Password is not Valid");
      alert("Password is not Valid");
      this.setState({ passwordError: "Password is not Valid" });
      return false;
    }
    return true;
  };

  render() {
    const { navigate } = this.props;
    const { LoginEmail, LoginPassword } = this.state;
    var { loginError, gId } = this.props.user;
    console.log(this.props.loader.showLoader)

    return (
      <ScrollView>
        <View style={Style.Forms}>
          <TextField
            value={LoginEmail}
            keyboardType="email-address"
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            autoCapitalize = "none"
            label="EMAIL"
            maxLength={40}
            tintColor={"#111"}
            onChangeText={LoginEmail =>
              this.setState({
                LoginEmail
              })}
          />

          <TextField
            value={LoginPassword}
            labelPadding={1}
            labelHeight={heightPercentageToDP(1)}
            autoCorrect={false}
            label="PASSWORD"
            maxLength={40}
            tintColor={"#111"}
            onChangeText={LoginPassword =>
              this.setState({
                LoginPassword
              })}
            secureTextEntry={true}
          />

          <View
            style={{
              height: "5%",
              width: "100%",
              alignItems: "center",
              flexDirection: "row-reverse",
              marginBottom: "5%"
            }}
          >
            <TouchableOpacity onPress={() => navigate("Register")}>
              <Text style={{ fontSize: widthPercentageToDP(3.5) }}>
                {"Forgot your Password?"}
              </Text>
            </TouchableOpacity>
          </View>

          <Button
            style={{
              width: "100%",
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center"
            }}
            onPress={() => this.onLogin()}
            medium
            dark
          >
            <Text style={{ fontSize: widthPercentageToDP(4), color: "#FFF" }}>
              {"LOGIN"}
            </Text>
            {this.props.loader.showLoader &&
              <ActivityIndicator
                style={{ position: "absolute", zIndex: 2, right: "2%" }}
                size="large"
                color="#fff"
              />}
          </Button>
        </View>
      </ScrollView>
    );
  }
}
//export default Login;
const mapStateToProps = state => ({
  user: state.user,
  appState: state.appState,
  loader: state.loader
});

export default connect(mapStateToProps, { login })(LOGIN);
