"use strict";
import React from "react";
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Dimensions,
  Animated
} from "react-native";
import Style from "./styles";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../components/MakeMeResponsive/MakeMeResponsive";
import { connect } from "react-redux";
import { TabView, TabBar, SceneMap } from "react-native-tab-view";
import SignUpSection from "./siginup";
import LogInSection from "./Login";
import { FBlogIn } from "../../redux/actions";
import { Facebook } from "expo";

import { FontAwesome, Entypo } from "@expo/vector-icons";
const initialLayout = {
  height: 0,
  width: Dimensions.get("window").width
};
const _renderTabBar = props =>
  <TabBar
    {...props}
    style={{ backgroundColor: "#FFF" }}
    indicatorStyle={{
      backgroundColor: "#111",
      width: 50,
      justifyContent: "space-around",
      marginLeft: 65
    }}
    labelStyle={{
      fontSize: widthPercentageToDP(4),
      fontWeight: "bold",
      color: "#111"
    }}
  />;
class LoginHome extends React.Component {
  state = {
    index: 0,
    routes: [
      { key: "first", title: "LOG IN" },
      { key: "second", title: "SIGN UP" }
    ],
    LoginEmail: "",
    LoginPassword: ""
  };
  LOGINRoute = () => <LogInSection navigate={this.props.nav.navigate} />;

  SIGNUPRoute = () => <SignUpSection navigate={this.props.nav.navigate} />;

  render() {
    const { gId,notificationToken } = this.props.user;
    let g_id;
    if (gId === "undefined") {
      g_id = "";
    } else {
      g_id = gId;
    }

    return (
      <View style={Style.cont}>
        <StatusBar
          animated={true}
          backgroundColor={"#FFF"}
          barStyle={"dark-content"}
        />

        <View style={Style.statusBarBackground} />
        <View style={Style.otherThenStatusBarView}>
          <TouchableOpacity
            style={{ position: "absolute", top: "0%", right: "5%", zIndex: 3 }}
            onPress={() => this.props.nav.goBack()}
          >
            <Entypo name="cross" size={widthPercentageToDP(10)} />
          </TouchableOpacity>
          <View style={Style.LOGOView}>
            <Image
              source={require("../../../assets/Login_logo.png")}
              style={Style.LOGO}
              resizeMode={"contain"}
            />
          </View>
          <View style={Style.OtherTHenLOGOView}>
            <View style={{ height: "80%" }}>
              <TabView
                swipeEnabled={false}
                navigationState={this.state}
                renderScene={SceneMap({
                  first: this.LOGINRoute,
                  second: this.SIGNUPRoute
                })}
                renderTabBar={_renderTabBar}
                onIndexChange={index => this.setState({ index })}
                initialLayout={initialLayout}
              />
            </View>
            <View style={{ flexDirection: "row",justifyContent:'center',marginBottom:2 }}>
              <View
                style={{
                  backgroundColor: "#D3D3D3",
                  height: 2,
                  width:30,
                  alignSelf: "center"
                }}
              />
              <Text
                style={{
                  alignSelf: "center",
                  paddingHorizontal: 5,
                  fontSize: 14,
                  color:'#D3D3D3'
                }}
              >
                Or Join with
              </Text>
              <View
                style={{
                  backgroundColor: "#D3D3D3",
                  height: 2,
                  width:30,
                  alignSelf: "center"
                }}
              />
            </View>

            {/* <Text style={{ textAlign: "center", color: "#D3D3D3" }}>
              ____Or Join with____
            </Text> */}

            <TouchableOpacity
              style={{
                alignItems: "center",
                alignSelf: "center",
                justifyContent: "center",
                backgroundColor: "#4867A7",
                width: widthPercentageToDP(10),
                height: widthPercentageToDP(10),
                borderRadius: widthPercentageToDP(7.5)
              }}
              //onPress={() => FBlogIn(g_id, this.props.user.checkOutStarted,notificationToken)}
            >
              <FontAwesome
                name="facebook"
                color={"#FFF"}
                size={widthPercentageToDP(5)}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps)(LoginHome);
