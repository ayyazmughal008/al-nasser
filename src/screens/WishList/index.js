import React, { Component } from "react";
import {
  Card,
  CardItem,
  Thumbnail,
  Icon,
  Grid,
  Col,
  Button,
  Text
} from "native-base";
import {
  FlatList,
  Image,
  TouchableHighlight,
  View,
  Alert,
  Modal,
  Dimensions,
  ScrollView,
  ActivityIndicator
} from "react-native";

import { connect } from "react-redux";
import BaseHeader from "../../components/BaseHeader/index.js";
import commonColor from "../../theme/variables/commonColor";
import { fetchWishListData } from "../../redux/actions";

var dwidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

const headers = new Headers();
headers.append(
  "Authorization",
  "Basic " + "YWRtaW5AYWxuYXNzZXIucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

class WishList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchMethods: [],
      isRefresh: true,
      showModal: false,
      modalMessage: null,
      isDelete: false,
      isModalVisible: false,
      noitems: false
    };
  }

  _showModal = () => this.setState({ isModalVisible: true });

  _hideModal = () => this.setState({ isModalVisible: false });

  componentWillMount() {
    const { userId, gId } = this.props.user;
    if (userId || gId) {
      fetchWishListData(userId, gId);
    }
  }

  deleteWishList = (userId, itemID) => {
    const { gId } = this.props.user;
    deletWishList = `https://www.alnasser.pk/api/Addtowishlist/${userId}/?item_id=${itemID}&&scope=wishlist_product_delete`;
    fetch(deletWishList, { headers: headers })
      .then(res => res.json())
      .then(json => {
        console.log("WishList Data inside WishList", json.success);
        if (json.success == true) {
          console.log("Success message");
          //alert("Item Deleted Successfully");
          this._showModal();
          fetchWishListData(userId, gId);
        }
      });
  };

  _keyExtractor = (item, index) => "MyKey" + index;

  clickHandler = (navigation, productName, productID, Image) => {
    navigation.navigate("ProductPage", {
      Name: productName,
      TargetId: productID,
      ProductImage: Image,
      senderPage: "home"
    });
  };

  render() {
    const navigation = this.props.navigation;
    const { searchMethods, showModal } = this.state;
    const { userId, gId, wishList } = this.props.user;

    if (wishList !== null) {
      if (wishList.length < 1) {
        return (
          <View style={{ flex: 1 }}>
            <BaseHeader
              PageTitle="Wish List"
              IconLeft="arrow-back"
              //route="confirmAddress"
              navigation={navigation}
            />
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <View>
                <Image
                  style={{
                    // width: dwidth * 2 / 2.1,
                    // height: deviceHeight * 2 / 4 - 120
                  }}
                  source={require("../../../assets/empty_wish.png")}
                />
              </View>

              <View style={{justifyContent:'center',alignItems:'center'}}>
              <Text
                style={{
                  fontSize: 20,
                  color: "#BDBDC8",
                  fontWeight: "500",
                  marginTop: 5,
                  textAlign:'center'
                }}
              >
                NO ITEMS
              </Text>
              <Text
                style={{
                  fontSize: 20,
                  color: "#BDBDC8",
                  fontWeight: "500",
                  marginTop: 5,
                  textAlign:'center'
                }}
              >
                IN YOUR WISHLIST
              </Text>
              </View>

              <View>
                <Button
                  transparent
                  block
                  onPress={() => navigation.navigate("HomePage")}
                  style={{
                    backgroundColor: "#111",
                    width: dwidth * 2 / 2.5,
                    height: deviceHeight * 1 / 4 - 120,
                    marginTop: 10,
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      fontSize: 15,
                      fontWeight: "300",
                      color: "#fff"
                    }}
                  >
                    Continue Shopping
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        );
      } else {
        return (
          <View style={{ flex: 1 }}>
            <BaseHeader
              PageTitle="Wish List"
              IconLeft="arrow-back"
              //route="confirmAddress"
              navigation={navigation}
            />

            <ScrollView>
              {userId
                ? <FlatList
                    //scrollEnabled = {false}
                    data={wishList}
                    numColumns={1}
                    keyExtractor={this._keyExtractor}
                    renderItem={({ item }) =>
                      <WishListItem
                        navigation={navigation}
                        userId={userId}
                        item={item}
                        clickHandler={this.clickHandler}
                        nav={navigation}
                        param1={item.product}
                        param2={item.product_id}
                        param3={item.image_path}
                        deleteWishList={this.deleteWishList}
                        param4={userId}
                        param5={item.item_id}
                      />}
                  />
                : <FlatList
                    //scrollEnabled = {false}
                    data={wishList}
                    numColumns={1}
                    keyExtractor={this._keyExtractor}
                    renderItem={({ item }) =>
                      <WishListItem
                        navigation={navigation}
                        gId={gId}
                        item={item}
                        clickHandler={this.clickHandler}
                        nav={navigation}
                        param1={item.product}
                        param2={item.product_id}
                        param3={item.image_path}
                        deleteWishList={this.deleteWishList}
                        param4={gId}
                        param5={item.item_id}
                      />}
                  />}
            </ScrollView>
            {this.state.isModalVisible
              ? <Modal
                  animationType={"slide"}
                  transparent={true}
                  visible={this.state.isModalVisible}
                  onRequestClose={() => {
                    console.log("Modal close");
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      justifyContent: "flex-end",
                      alignItems: "center",
                      alignContent: "center"
                    }}
                  >
                    <View
                      style={{
                        height: 100,
                        width: dwidth,
                        backgroundColor: "#fff",
                        alignItems: "center",
                        shadowRadius: 1,
                        borderRadius: 10
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 16,
                          fontWeight: "200",
                          color: "green",
                          marginTop: 20,
                          marginBottom: 10
                        }}
                      >
                        Item Deleted Successfully.!
                      </Text>

                      <TouchableHighlight
                        transparent
                        style={{
                          backgroundColor: commonColor.brandSuccess,
                          height: 40,
                          width: 40,
                          borderRadius: 100,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        onPress={() => {
                          this._hideModal();
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 15,
                            fontWeight: "700",
                            color: "#fff",
                            textAlign: "center",
                            textAlign: "center"
                          }}
                        >
                          OK
                        </Text>
                      </TouchableHighlight>
                    </View>
                  </View>
                </Modal>
              : <View />}
          </View>
        );
      }
    } else {
      return (
        <View style={{ flex: 1 }}>
          <BaseHeader
            PageTitle="Wish List"
            IconLeft="arrow-back"
            //route="confirmAddress"
            navigation={navigation}
          />
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <View>
              <Image
                style={{
                  // width: dwidth * 2 / 2.1,
                  // height: deviceHeight * 2 / 4 - 120
                }}
                source={require("../../../assets/empty_wish.png")}
              />
            </View>

            <View style={{justifyContent:'center',alignItems:'center'}}>
              <Text
                style={{
                  fontSize: 20,
                  color: "#BDBDC8",
                  fontWeight: "500",
                  marginTop: 5,
                  textAlign:'center'
                }}
              >
                NO ITEMS
              </Text>
              <Text
                style={{
                  fontSize: 20,
                  color: "#BDBDC8",
                  fontWeight: "500",
                  marginTop: 5,
                  textAlign:'center'
                }}
              >
                IN YOUR WISHLIST
              </Text>
              </View>

            <View>
              <Button
                transparent
                block
                onPress={() => navigation.navigate("HomePage")}
                style={{
                  backgroundColor: "#111",
                  width: dwidth * 2 / 2.5,
                  height: deviceHeight * 1 / 4 - 120,
                  marginTop: 10,
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: 15,
                    fontWeight: "300",
                    color: "#fff"
                  }}
                >
                  Continue Shopping
                </Text>
              </Button>
            </View>
          </View>
        </View>
      );
    }
  }
}

export const WishListItem = props => {
  return (
    <Card transparent style={{ flex: 1, marginRight: 3, marginLeft: 3 }}>
      <CardItem>
        <Grid>
          <TouchableHighlight
            onPress={() =>
              props.clickHandler(
                props.nav,
                props.param1,
                props.param2,
                props.param3
              )}
          >
            <Col size={1}>
              <Thumbnail
                style={{
                  resizeMode: "cover",
                  marginTop: 5,
                  height: 60,
                  width: 60
                }}
                square
                source={{ uri: props.item.image_path }}
              />
            </Col>
          </TouchableHighlight>
          <Col size={2} style={{ marginLeft: 10 }}>
            <Text
              style={{
                fontSize: 14,
                fontWeight: "300",
                color: "#333"
              }}
              numberOfLines={2}
              ellipsizeMode="tail"
            >
              {props.item.product}
            </Text>
            <Text
              style={{
                fontSize: 14,
                fontWeight: "300",
                color: "#333"
              }}
            >
              {"Price: "} {props.item.format_price}
            </Text>
          </Col>

          <Col size={1} style={{ marginLeft: 20, marginBottom: 20 }}>
            <Button
              transparent
              onPress={() => props.deleteWishList(props.param4, props.param5)}
            >
              <Icon name="ios-close" />
            </Button>
          </Col>
        </Grid>
      </CardItem>
    </Card>
  );
};

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps, { fetchWishListData })(WishList);
