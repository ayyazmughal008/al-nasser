import React, { Component } from "react";
import {
  Card,
  Container,
  CardItem,
  Thumbnail,
  Left,
  Right,
  Body,
  Input,
  Icon,
  Grid,
  Col,
  Button,
  Content,
  Header,
  Item,
  Footer,
  List,
  ListItem
} from "native-base";
//import {} from 'react-native'
import {
  View,
  Text,
  FlatList,
  TouchableHighlight,
  Image,
  Dimensions,
  ActivityIndicator
} from "react-native";
import { updateBrowsePosition } from "../../redux/actions";
// import { observer, inject } from "mobx-react/native";
import RoundImageButton from "../../components/RoundImageButton/index.js";
import NavigationService from "../../NavigationService";
import { connect } from "react-redux";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

const headers = new Headers();
headers.append(
  "Authorization",
  "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

class SearchProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchMethods: null,
      topSearchMethods: [],
      search: "",
      showComponment: false,
      isFetching: false,
      firstTtime: true,
      pageNo: 1,
      isLoading: false
    };
  }

  handleTopCategoryIconClick(categoryID) {
    updateBrowsePosition({
      categoryId: categoryID,
      currentPage: 0,
      topCategoryId: categoryID,
      isCategoryListingPage: true
    });
    NavigationService.navigate("Category");
  }

  componentWillMount() {
    console.log("SearchProducts Screen. componentWillMount()");
    this.fetchPopularSearchMethods();
  }

  fetchPopularSearchMethods() {
    fetchpopSearchUrl = "https://www.alnasser.pk/api/ExtendedBlocks/96";
    fetch(fetchpopSearchUrl, { headers: headers })
      .then(res => res.json())
      .then(json => {
        //console.log(json);
        this.setState({
          searchMethods: json.popular_searches
        });
      });
  }

  clickHandler(navigation, catID) {
    navigation.navigate("ProductList", {
      cid: catID
    });
  }

  // componentDidMount()
  // {
  //     this.fethchSearchMethod()
  // }

  fethchSearchMethod() {
    var { search, pageNo } = this.state;
    fetchSearchUrl = `https://www.alnasser.pk/api/GetSearchSuggestion?q=${search}&page=${pageNo}`;
    fetch(fetchSearchUrl, { headers: headers })
      .then(res => res.json())
      .then(json => {
        console.log(json);
        let obj = json.search_data;
        var result = Object.values(obj);

        console.log("inside Search Screen \n", result);

        this.setState({
          searchMethods: result,
          isFetching: false,
          firstTtime: false,
          isLoading: false
          //console.log("On search",search,topSearchMethods)
        });
      });
  }

  onRefresh() {
    this.setState(
      {
        isFetching: true
      },
      function() {
        this.fethchSearchMethod();
      }
    );
  }

  _keyExtractor = (item, index) => "MyKey" + index;

  addMore = () => {
    let { searchMethods, pageNo, search } = this.state;
    pageNo++;
    fetchSearchUrl = `https://www.alnasser.pk/api/GetSearchSuggestion?q=${search}&page=${pageNo}`;
    fetch(fetchSearchUrl, { headers: headers })
      .then(res => res.json())
      .then(json => {
        console.log(json);
        let obj = json.search_data;
        var result = Object.values(obj);
        let arrayLength = result.length;
        for (var i = 0; i < arrayLength; i++) {
          searchMethods.push(result[i]);
        }
        console.log("inside Search Screen \n", result);
        this.setState({
          searchMethods,
          isFetching: false,
          firstTtime: false,
          pageNo
          //console.log("On search",search,topSearchMethods)
        });
      });
  };

  clickHandlerSearch(navigation, catID, proName) {
    navigation.navigate("ProductPage", {
      Product: catID,
      Name: proName
    });
  }

  render() {
    const { goBack } = this.props.navigation;
    const navigation = this.props.navigation;
    const {
      searchMethods,
      topSearchMethods,
      showComponment,
      isFetching,
      firstTtime
    } = this.state;
    console.log("Total Search", searchMethods);
    var { categoryicons, subCategories } = this.props.homePageState;
    return firstTtime
      ? <View style={{ flex: 1 }}>
          {/* <Container> */}
          <Header
            noShadow
            style={{
              marginTop: 0,
              marginLeft: 0,
              marginRight: 0,
              borderRadius: 3,
              backgroundColor: "white",
              marginBottom: 2
            }}
          >
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon name="ios-arrow-back" />
              </Button>
            </Left>
            <Body>
              <Item>
                <Input
                  placeholder="Search"
                  onChangeText={search => this.setState({ search })}
                />
              </Item>
            </Body>
            <Right>
              <Button
                transparent
                onPress={() => (
                  this.onRefresh(),
                  this.setState({ isLoading: true })
                )}
              >
                <Icon name="ios-search" />
              </Button>
            </Right>
          </Header>
          {/* <Content> */}

          {/* Populate Round Images from Hame Page */}

          <View>
            <FlatList
              removeClippedSubviews={false}
              contentContainerStyle={{ flexDirection: "row" }}
              directionalLockEnabled={false}
              horizontal={true}
              keyExtractor={this._keyExtractor}
              showsHorizontalScrollIndicator={false}
              data={categoryicons}
              renderItem={({ item }) =>
                <RoundImageButton
                  navigation={navigation}
                  roundImageText={item.cname}
                  roundImageSource={item.image_path}
                  clickHandler={() =>
                    this.handleTopCategoryIconClick(item.target_id)}
                  categoryID={item.target_id}
                  subCat={subCategories.filter(
                    cat => cat.category_id === item.target_id
                  )}
                  style={{
                    resizeMode: "contain",
                    flex: 1
                  }}
                />}
            />
          </View>

          {/*Papoular Search will populate here first time when user navigate to this page  */}

          <View
            style={{
              backgroundColor: "grey",
              justifyContent: "center",
              alignItems: "center",
              height: 50,
              padding: 10,
              marginLeft: 10,
              marginRight: 10
            }}
          >
            <Text style={{ fontSize: 17, fontWeight: "300", color: "white" }}>
              PAPULAR SEARCHES
            </Text>
          </View>
          <FlatList
            data={searchMethods}
            scrollEnabled={true}
            keyExtractor={this._keyExtractor}
            renderItem={({ item }) =>
              <PopularSearchMethod
                item={item}
                clickHandler={this.clickHandler}
                nav={navigation}
                param={item.category_id}
              />}
          />

          {/* </Content>
                <MyFooter navigation={navigation} selected={"home"} />
            </Container> */}
          {this.state.isLoading &&
            <ActivityIndicator
              size="large"
              color="#0000ff"
              style={{
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                alignItems: "center",
                justifyContent: "center"
              }}
            />}
        </View>
      : <View style={{ flex: 1 }}>
          <Header
            noShadow
            style={{
              marginTop: 0,
              marginLeft: 0,
              marginRight: 0,
              borderRadius: 3,
              backgroundColor: "white",
              marginBottom: 0
            }}
          >
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon name="ios-arrow-back" />
              </Button>
            </Left>
            <Body>
              <Item>
                <Input
                  placeholder="Search"
                  blurOnSubmit={false}
                  onChangeText={search => this.setState({ search })}
                />
              </Item>
            </Body>
            <Right>
              <Button
                transparent
                onPress={() => (
                  this.onRefresh(),
                  this.setState({ isLoading: true })
                )}
              >
                <Icon name="ios-search" />
              </Button>
            </Right>
          </Header>
          {searchMethods.length === 0
            ? <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: 17,
                    fontWeight: "300",
                    textAlign: "center"
                  }}
                >
                  No item found. please search again
                </Text>
              </View>
            : <FlatList
                data={searchMethods}
                keyExtractor={this._keyExtractor}
                onEndReached={this.addMore}
                onEndReachedThreshold={0.5}
                renderItem={({ item }) =>
                  <SearchList
                    navigation={navigation}
                    item={item}
                    // clickHandler={this.clickHandlerSearch}
                    // navi={navigation}
                    // param1={item.product_id}
                    // param2={item.product}
                    clickHandler={() =>
                      navigation.navigate("ProductPage", {
                        Name: item.product,
                        TargetId: item.product_id,
                        ProductImage: item.main_pair.detailed.absolute_path,
                        // IsSearch: true,
                        senderPage: "home"
                      })}
                  />}
              />}
          {this.state.isLoading &&
            <ActivityIndicator
              size="large"
              color="#0000ff"
              style={{
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                alignItems: "center",
                justifyContent: "center"
              }}
            />}
        </View>;
  }
}
//export default SearchProducts

export const PopularSearchMethod = props => {
  return (
    <TouchableHighlight
      onPress={() => props.clickHandler(props.nav, props.param)}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: "white",
          padding: 10,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            alignContent: "center",
            alignItems: "center"
          }}
        >
          <Text style={{ color: "black", fontSize: 18, fontWeight: "300" }}>
            {props.item.category}
          </Text>
        </View>
        {/* <View style = {{backgroundColor:'black', height:2,flex:1, flexDirection: 'row'}}>
          </View> */}
      </View>
    </TouchableHighlight>
  );
};

export const SearchList = props => {
  return (
    // <TouchableHighlight
    //   onPress={() =>
    //     props.clickHandler(props.navi, props.param1, props.param2)}
    // >
    <Card style={{ flex: 1, marginRight: 10, marginLeft: 10 }}>
      <TouchableHighlight
        onPress={() =>
          props.clickHandler(props.navi, props.param1, props.param2)}
      >
        <CardItem>
          <Grid>
            <Col size={1}>
              <Thumbnail
                style={{
                  resizeMode: "cover",
                  marginTop: 5,
                  height: 60,
                  width: 60
                }}
                square
                source={{ uri: props.item.main_pair.detailed.absolute_path }}
              />
            </Col>
            <Col size={3} style={{ marginLeft: 10 }}>
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "300",
                  color: "#333"
                }}
                numberOfLines={2}
                ellipsizeMode="tail"
              >
                {props.item.product}
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "300",
                  color: "#333"
                }}
              >
                {props.item.product_code}
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "300",
                  color: "#333"
                }}
              >
                {props.item.list_price}
              </Text>
            </Col>
            <Col size={1} style={{ marginLeft: 5, marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "300",
                  color: "#333"
                }}
              >
                {props.item.category}
              </Text>
            </Col>
          </Grid>
        </CardItem>
      </TouchableHighlight>
    </Card>
  );
};

const mapStateToProps = state => ({
  homePageState: state.homepage
});

export default connect(mapStateToProps)(SearchProducts);
