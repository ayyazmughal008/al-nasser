import commonColor from "../../theme/variables/commonColor.js";

export default {
  cardTitleText: {
    fontSize: 12,
    color: "#555",
    fontWeight: "500",
    marginTop: 10,
    marginLeft:10
  },
  cardItem: {
    justifyContent: "space-between",
    paddingTop: 5,
    paddingBottom: 5
  },
  cardItemText: {
    fontSize: 13,
    color: "#555"
  },
  cardItemBtnText: {
    fontSize: 12,
    color: commonColor.brandPrimary,
    fontWeight: "bold"
  }
};
