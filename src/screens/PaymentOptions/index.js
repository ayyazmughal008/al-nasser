import React, { Component } from "react";
import { View, Text, FlatList, TouchableHighlight, Image } from "react-native";

import { Container, Content } from "native-base";

import BaseHeader from "../../components/BaseHeader";

import styles from "./styles";
//import {fetchPaymentMethods} from '../../redux/actions'
const headers = new Headers();
headers.append(
  "Authorization",
  "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

class PaymentOptions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recommendedMethods: null,
      otherMethods: null
    };
  }

  componentWillMount() {
    console.log("PaymentOptions Screen. componentWillMount()");
    this.fetchPaymentMethods();
  }

  fetchPaymentMethods() {
    fetchUrl = "https://www.alnasser.pk/api/payments?status=A&for_app=1";
    fetch(fetchUrl, { headers: headers }).then(res => res.json()).then(json => {
      console.log(json);
      this.setState({
        recommendedMethods: json.payments.filter(
          payment => payment.recommended === "0"
        ),
        // otherMethods: json.payments.filter(
        //   payment => payment.recommended === "1"
        // )
      });
    });
  }
  clickHandler(method, navigation) {
    const shippingFee = navigation.getParam("ShippingCharges", "123");
    id = method.payment_id;
    switch (method.payment) {
      case "Cash On Delivery":
        navigation.navigate("CashonDelivery", {
          id: id,
          shippingFee: shippingFee
        });
        break;
      case "Debit/Credit card by EasyPay":
        navigation.navigate("EasyPaisaWallet", {
          id: id,
          shippingFee: shippingFee
        });
        break;
      default:
        break;
    }
  }

  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const navigation = this.props.navigation;

    const { recommendedMethods, otherMethods } = this.state;
    return (
      <Container>
        <BaseHeader
          PageTitle="Payment Method(s)"
          IconLeft="arrow-back"
          route="confirmAddress"
          navigation={navigation}
        />
        <Content contentContainerStyle={{ paddingBottom: 10 }}>
          <View style={{ flex: 1 }}>
            <Text style={styles.cardTitleText}>
              {"Recomended Payment Methods".toUpperCase()}
            </Text>
            <FlatList
              data={recommendedMethods}
              renderItem={({ item }) =>
                <PaymentMethod
                  item={item}
                  clickHandler={this.clickHandler}
                  nav={navigation}
                />}
            />
            <Text style={styles.cardTitleText}>
              {"Other Payment Methods".toUpperCase()}
            </Text>
            <FlatList
              keyExtractor={this._keyExtractor}
              data={otherMethods}
              renderItem={({ item }) =>
                <PaymentMethod
                  item={item}
                  clickHandler={this.clickHandler}
                  nav={navigation}
                />}
            />
          </View>
          {/* <Card>
            <List
              removeClippedSubviews={false}
              dataArray={otherPaymentOptions}
              renderRow={item =>
                <CardItem style={styles.cardItem}>
                  <Text style={styles.cardItemText}>
                    {item.text}
                  </Text>
                  <Right>
                    <Button transparent small
                    onPress={() => navigation.navigate("EasyPaisaWallet")}
                    >
                      <Text style={styles.cardItemBtnText}>SELECT</Text>
                    </Button>
                  </Right>
                </CardItem>}
            />
          </Card> */}
          {/* </Content>
        <MyFooter navigation={navigation} selected={"bag"} /> */}
        </Content>
      </Container>
    );
  }
}
export default PaymentOptions;

export const PaymentMethod = props => {
  return (
    <TouchableHighlight
      onPress={() => props.clickHandler(props.item, props.nav)}
    >
      <View style={{ backgroundColor: "white", margin: 10 }}>
        <View
          style={{
            flexDirection: "row",
            alignContent: "center",
            alignItems: "center"
          }}
        >
          <Image
            style={{ width: 50, height: 50 }}
            source={{ uri: props.item.app_icon }}
            resizeMode="contain"
          />
          <Text>
            {props.item.payment}
          </Text>
        </View>
        {props.item.promo_text &&
          <View>
            <Text>
              {props.item.promo_text}
            </Text>
          </View>}
      </View>
    </TouchableHighlight>
  );
};
