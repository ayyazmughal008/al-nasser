"use strict";
import React from "react";
import {
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  Platform,
  StatusBar
} from "react-native";
import Style from "./styles";

import { SimpleLineIcons } from "@expo/vector-icons";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../components/MakeMeResponsive/MakeMeResponsive";
import styles from "./styles";
import { Button } from "native-base";
import { TextField } from "react-native-material-textfield";

const headers = new Headers();
headers.append(
  "Authorization",
  "Basic " + "YWRtaW5AYWxuYXNzZXIucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

export default class Register extends React.Component {
  state = {
    Etext: "",
    Loading: false,
    forgetPassword: false,
  };

  updatePassword(email) {
    const orderUrl = "https://www.alnasser.pk/api/extendedusers/";
    console.log("starting Password Recovery Data");
    fetch(orderUrl, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
      },
      body: JSON.stringify({
        email: email,
        forget_password: "true"
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log("Password Recovery results: \n", json);
        if (json.message) {
          alert(json.message);
        } else {
          alert(json.msg);
        }
      });
  }

  render() {
    const { navigate, goBack } = this.props.navigation;
    const { Loading, Etext } = this.state;
    return (
      <View style={Style.cont}>
        <StatusBar
          animated={true}
          backgroundColor={"#FFF"}
          barStyle={"light-content"}
        />
        {/* <View style={styles.statusBarBackground} /> */}
        <View style={styles.otherThenStatusBarView}>
          <View style={styles.NAVBar}>
            <TouchableOpacity style={styles.BACKBtn} onPress={() => goBack()}>
              <SimpleLineIcons
                name="arrow-left"
                color="#FFF"
                size={widthPercentageToDP(6)}
              />
            </TouchableOpacity>
            <Text style={styles.NavBarHeader}>
              {"RESET PASSWORD"}
            </Text>
          </View>
          <View style={styles.FormView}>
            <Text style={styles.FormHeader1}>
              {"Forgot Password?"}
            </Text>
            <Text style={styles.FormHeader2}>
              {
                "It's okay,it can happen to anyone. Enter your email and we'll send your reset password link."
              }
            </Text>
            {/* <Text style={styles.FormHeader2}>
              {"your reset password link."}
            </Text> */}
            <View style={{ height: "40%", width: "100%", padding: "5%" }}>
              <TextField
                value={Etext}
                labelPadding={1}
                keyboardType="email-address"
                labelHeight={heightPercentageToDP(1)}
                autoCorrect={false}
                label="EMAIL"
                maxLength={40}
                tintColor={"#111"}
                onChangeText={Etext =>
                  this.setState({
                    Etext
                  })}
              />
              <Button
                style={{
                  marginTop: "5%",
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                  alignSelf: "center"
                }}
                onPress={() => this.updatePassword(Etext)}
                medium
                dark
              >
                <Text
                  style={{ fontSize: widthPercentageToDP(4), color: "#FFF" }}
                >
                  {"SEND"}
                </Text>
              </Button>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
