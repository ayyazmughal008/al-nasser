import { StatusBar, StyleSheet } from "react-native";
import {
  widthPercentageToDP,
  heightPercentageToDP
} from "../../components/MakeMeResponsive/MakeMeResponsive";
export default StyleSheet.create({
  LOGO: {
    height: "100%",
    width: "100%"
  },
  LOGOView: {
    height: "25%",
    width: widthPercentageToDP(100),
    backgroundColor: "#FDD"
  },
  cont: {
    flex: 1,
    backgroundColor: "#FFF",
    alignItems: "center"
  },
  NavBar: {
    width: "100%",
    height: heightPercentageToDP(5),
    backgroundColor: "#111"
  },
  FormHeader1: {
    marginTop: "5%",
    fontSize: widthPercentageToDP(5),
    fontWeight: "bold"
  },
  FormHeader2: {marginRight:"5%",marginLeft:"5%", fontSize: widthPercentageToDP(3.5) },
  NavBarHeader: {
    fontSize: widthPercentageToDP(5),
    fontWeight: "bold",
    color: "#FFF"
  },
  TextFieldStyle: {
    height: widthPercentageToDP(15),
    width: "80%",
    marginBottom: "2%"
  },
  RegisterBtn: {
    width: "75%",
    height: widthPercentageToDP(15),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  FormView: {
    width: "100%",
    height: "92%",
    backgroundColor: "#FFF",
    alignItems: "center",
    paddingVertical: "2%"
  },
  BACKBtn: {
    width: "18%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    marginRight: "5%"
  },
  NAVBar: {
    width: "100%",
    height: "8%",
    backgroundColor: "#111",
    alignItems: "center",
    flexDirection: "row"
  },
  statusBarBackground: {
    height: StatusBar.currentHeight,
    backgroundColor: "#111",
    width: "100%"
  },
  otherThenStatusBarView: { flex: 1, backgroundColor: "#FFF", width: "100%" },
  RadioBtnInner: { flexDirection: "row", alignItems: "center" }
});
