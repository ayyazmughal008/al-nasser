import React, { Component } from "react";
import {
  Container,
  Content,
  List,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Title
} from "native-base";
import { View, Text, FlatList, ActivityIndicator } from "react-native";
import ThemeHeader from "../../components/Header";
import { connect } from "react-redux";
import BaseHeader from "../../components/BaseHeader/index.js";
import OrdersList from "../../components/OrdersList";
import { fetchUserOrderswithoutAnyDispatch } from "../../redux/actions";
const headers = new Headers();
headers.append(
  "Authorization",
  "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

class Orders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userOrders: null,
      pageNo: 1,
      isFetching: false,
      newOrders: [],
      totalPages:0
    };
  }

  componentWillMount() {
    const { userId } = this.props.user;

    if (userId) {
      this.fetchOrders(userId);
    }
    //   // else{
    //   //   this.fetchOrders();
    //   // }
  }

  fetchOrders = userId => {
    const { pageNo } = this.state;
    fetchUserOrderswithoutAnyDispatch(userId, pageNo).then(Res =>
      {
        let Pages = Math.ceil( Res.orders_qty/10)

        this.setState({
          newOrders: Res.orders,
          isFetching: false,
          totalPages:Pages
        })
      }

      

    );
  };

  addMore = () => {
    const { userId } = this.props.user;
    let { newOrders, pageNo,totalPages } = this.state;
    pageNo++;
    if(pageNo <= totalPages){
    fetchUserOrderswithoutAnyDispatch(userId, pageNo).then(Res => {
      let obj = Res.orders;
      let arrayLength = obj.length;
      for (let i = 0; i < arrayLength; i++) {
        newOrders.push(obj[i]);
      }
      if (obj.length)
        this.setState({
          newOrders,
          pageNo
        });
    });
  }
  };

  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const navigation = this.props.navigation;
    let { newOrders, isFetching } = this.state;
    let { orders_qty, userOrders, userId } = this.props.user;
    console.log("inside orders screen\n", orders_qty);

    return (
      <Container>
        <BaseHeader
          PageTitle="My Orders"
          IconLeft="arrow-back"
          //route="confirmAddress"
          navigation={navigation}
        />
        {userId !== null
          ? userOrders !== null
            ? orders_qty !== "0"
              ? <FlatList
                  data={newOrders}
                  extraData={this.state.newOrders}
                  contentContainerStyle={{ flexDirection: "column" }}
                  numColumns={1}
                  horizontal={false}
                  onEndReached={this.addMore}
                  onEndReachedThreshold={0.5}
                  keyExtractor={this._keyExtractor}
                  renderItem={({ item }) =>
                    <OrdersList
                      navigation={navigation}
                      orderid={item.wk_random_order_id}
                      orderplace={item.timestamp}
                      status={item.status}
                      clickHandler={() =>
                        navigation.navigate("OrderDetail", {
                          orderID: item.order_id,
                          randomID: item.wk_random_order_id
                        })}
                    />}
                />
              : <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "300",
                      textAlign: "center"
                    }}
                  >
                    You didn't placed any order yet.
                  </Text>
                </View>
            : <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: "300",
                    textAlign: "center"
                  }}
                >
                  You didn't placed any order yet.
                </Text>
              </View>
          : <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                margin: 10
              }}
            >
              <Text
                style={{ fontSize: 18, fontWeight: "300", textAlign: "center" }}
              >
                You didn't placed any order yet. Please Login and place your
                first order.
              </Text>
            </View>}
        {/* : <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: "300",
                      textAlign: "center"
                    }}
                  >
                    You didn't placed any order yet.
                  </Text>
                </View>
            : <View style={{ justifyContent: "center", alignItems: "center" }}>
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
          : <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                margin: 10
              }}
            >
              <Text
                style={{ fontSize: 18, fontWeight: "300", textAlign: "center" }}
              >
                You didn't placed any order yet. Please Login and place your
                first order.
              </Text>
            </View>} */}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps)(Orders);
