const data = [
  {
    id: 2,
    bannerImageSource: require("../../../assets/b5.png"),
    bannerImageText: "We Love Winters!",
    bannerSmallText: "Stay Warm & Cozy"
  },
  {
    id: 3,
    bannerImageSource: require("../../../assets/b2.png"),
    bannerImageText: "The Grunge Collection!",
    bannerSmallText: "Born For The Road"
  },
  {
    id: 4,
    bannerImageSource: require("../../../assets/b3.png"),
    bannerImageText: "On Point!",
    bannerSmallText: "Premium Bags"
  },
  {
    id: 5,
    bannerImageSource: require("../../../assets/b4.png"),
    bannerImageText: "Everyday Wear",
    bannerSmallText: "Printed Tees"
  }
];
module.exports = data;
