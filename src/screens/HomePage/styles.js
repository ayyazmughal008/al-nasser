export default {
  bannerHeading: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: "300",
    color: "#696d79",
    margin: 4,
    
  },
  bannerSliderImage: {
    height: 100,
    width: 170,
    resizeMode: "stretch"
  },
  sliderImageText: {
    fontSize: 14,
    fontWeight: "700",
    marginTop: 5
  },
  smallText: {
    fontSize: 11,
    color: "#a4a5a6",
    marginTop: 2.5,
    marginBottom: 10
  },
  cutOffPrice: {
    color: "#a4a5a6",
    marginLeft: 5,
    textDecorationLine: "line-through"
  }
};
