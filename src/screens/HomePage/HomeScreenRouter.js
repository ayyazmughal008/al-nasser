import React, { Component } from "react";
import HomePage from "../HomePage"
import SideBar from "../../components/Slider/SideBar"
import {StackNavigator,DrawerNavigator } from "react-navigation";

const myDrawer = DrawerNavigator(
    {
      Home: 
      { screen: HomePage }
    },
    {
      contentComponent: props => <SideBar {...props} />
    }
  );

// const MyApp = DrawerNavigator({
//   Home: {
//     screen: HomePage,
//   },
// });


  // const MyApp = DrawerNavigator({
  //   Home: {
  //     screen: HomePage,
  //   }
  // },
  // {
  //   contentComponent: props => <SideBar {...props} />
  // }
  // );

  // const nativeShop  = StackNavigator({

  //   Home: {screen:myDrawer},

  // });

  export default myDrawer;
