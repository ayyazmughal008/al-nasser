import React, { Component } from "react";
import { connect } from "react-redux";
import { updateBrowsePosition, fetchCartItems } from "../../redux/actions";
import { Notifications } from "expo";
import {
  Text,
  Dimensions,
  AsyncStorage,
  Linking,
  Modal,
  TouchableHighlight,
  Platform,
  FlatList,
  Image,
  ActivityIndicator
} from "react-native";
import { Constants } from "expo";
import {
  Container,
  Content,
  Button,
  List,
  Card,
  CardItem,
  Body,
  Grid,
  Col,
  Spinner,
  View
} from "native-base";
// import { observer, inject } from "mobx-react/native";
//import { Image } from "react-native-expo-image-cache";
import RoundImageButton from "../../components/RoundImageButton/index.js";
import BannerSlider from "../../components/BannerSlider/index.js";
import ThemeHeader from "../../components/Header/index.js";
import MyFooter from "../../components/Footer";
import styles from "./styles";
import NavigationService from "../../NavigationService";
import { fetchWishListData } from "../../redux/actions";
import registerForPushNotificationsAsync from "../../FCM_Notifications/index";

const headers = new Headers();
headers.append(
  "Authorization",
  "Basic " + "YWRtaW5AYWxuYXNzZXIucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchMethods: null,
      isRefresh: false,
      isModalVisible: true,
      notification: {},
      imageStatus: true
    };
  }

  componentWillMount() {
    const { userId, gId } = this.props.user;
    if (userId || gId) {
      fetchWishListData(userId, gId);
    }
    this.fetchCart();
  }

  fetchCart = () => {
    const { userId, gId } = this.props.user;
    if (userId) this.props.fetchCartItems(userId);
    else if (gId) this.props.fetchCartItems(gId);
    else this.props.fetchCartItems();
  };

  componentDidMount() {
    registerForPushNotificationsAsync();
    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    );
  }

  _handleNotification = notification => {
    //this.setState({ notification: notification });
    console.log('My Notification data is ',notification.data.type,"\n",notification.data.type_data)
    if (notification.data.type === "P") {
      this.props.navigation.navigate("ProductPage", {
        Name: "New Product",
        //TargetId: "262902",
        TargetId: notification.data.type_data,
      });
      //this.props.navigation.navigate("Notifications")
    }else if (notification.data.type === "C"){
      this.props.navigation.navigate("ProductList", {
        cid: notification.data.type_data,
        productName: notification.data.title
      });
    }else if (notification.data.type === "U"){
      this.props.navigation.navigate("ProductList", {
        targetUrl: notification.data.target_url,
        productName: notification.data.title
      });
    }else{
      this.props.navigation.navigate("Notifications")
    }
  };


  _showModal = () => this.setState({ isModalVisible: true });

  _hideModal = () => this.setState({ isModalVisible: false });

  handleTopCategoryIconClick(categoryID) {
    updateBrowsePosition({
      categoryId: categoryID,
      currentPage: 0,
      topCategoryId: categoryID,
      isCategoryListingPage: true
    });
    NavigationService.navigate("Category");
  }

  // handle click to route to listing page with a specific target URL
  handleOnPressTargetUrlListing(targetUrl, pName) {
    this.props.navigation.navigate("ProductList", {
      targetUrl: targetUrl,
      productName: pName
    });
  }
  handleOnPressTargetCID(id, pName) {
    console.log("handleOnPressTargetCID,", id);
    this.props.navigation.navigate("ProductList", {
      cid: id,
      productName: pName
    });
  }

  handleOnPressProductPage(targetId, imagePath) {
    console.log("handleOnPressProductPage,", targetId);
    this.props.navigation.navigate("ProductPage", {
      Name: "New Product",
      TargetId: targetId,
      ProductImage: imagePath,
      senderPage: "home"
    });
  }

  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const navigation = this.props.navigation;
    const { cartItems, wishListLength,notificationToken } = this.props.user;
    var { searchMethods, isRefresh } = this.state;
    const { id, sdkVersion, version } = Constants.manifest;
    let amount = cartItems.cart ? cartItems.cart.amount + "" : "0";
    //console.log(amount);

    var userfeadbackimages = [
      {
        id: 1,
        img:
          "https://static-01.daraz.pk/original/0341925b38eb791c720e12fe9e642900.jpg"
      },
      {
        id: 2,
        img:
          "https://static-01.daraz.pk/original/0341925b38eb791c720e12fe9e642900.jpg"
      },
      {
        id: 3,
        img:
          "https://static-01.daraz.pk/original/0341925b38eb791c720e12fe9e642900.jpg"
      },
      {
        id: 4,
        img:
          "https://static-01.daraz.pk/original/0341925b38eb791c720e12fe9e642900.jpg"
      }
    ];

    var {
      isLoaded,
      categoryicons,
      bannerslider,
      minibanner,
      sliderwidget,
      subCategories,
      appVersion
    } = this.props.homePageState;
    //console.log("Home Page sub category", subCategories);
    //console.log("My Notification token from redux is ", notificationToken);

    if (!isLoaded) {
      return (
        <View style={{ flex: 1 }}>
          <ThemeHeader
            navigation={this.props.navigation}
            drawerOpen="DrawerOpen"
            IconLeft="ios-menu"
            textInput="SearchProducts"
            PageTitle="AL NASSER"
            IconRight="ios-heart-empty"
            IconRight2="ios-cart"
          />

          <Spinner color="green" />
        </View>
      );
    } else {
      if (appVersion > 0) {
        // this._showModal();
        return (
          <View style={{ flex: 1 }}>
            <ThemeHeader
              navigation={this.props.navigation}
              drawerOpen="DrawerOpen"
              IconLeft="ios-menu"
              textInput="SearchProducts"
              PageTitle="AL NASSER"
              IconRight="ios-heart-empty"
              IconRight2="ios-cart"
            />
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                marginBottom: 36
              }}
            >
              <Modal
                animationType={"slide"}
                transparent={true}
                visible={this.state.isModalVisible}
                onRequestClose={() => {
                  console.log("Modal close");
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <View
                    style={{
                      height: 200,
                      width: 300,
                      backgroundColor: "#D3D3D3",
                      alignItems: "center",
                      justifyContent: "space-between",
                      shadowRadius: 1,
                      borderRadius: 10
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        fontWeight: "200",
                        color: "#696969",
                        marginTop: 10,
                        marginLeft: 5,
                        marginRight: 5
                      }}
                    >
                      New version of Clicky App is avilable on store. Let it
                      update and get the the latest features and discount
                    </Text>

                    <TouchableHighlight
                      transparent
                      onPress={() => {
                        Platform.OS === "ios"
                          ? Linking.openURL(
                              `https://itunes.apple.com/sa/app/clicky-online-shopping/id1448160644?mt=8`
                            )
                          : Linking.openURL(
                              `https://play.google.com/store/apps/details?id=com.clicky.pk`
                            );
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "700",
                          color: "#696969",
                          textAlign: "center",
                          marginBottom: 30
                        }}
                      >
                        Update Now
                      </Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </Modal>
            </View>
          </View>
        );
      } else {
        return (
          <Container>
            <ThemeHeader
              navigation={this.props.navigation}
              drawerOpen="DrawerOpen"
              IconLeft="ios-menu"
              textInput="SearchProducts"
              PageTitle="Search on AL NASSER"
              IconRight="ios-heart-empty"
              IconRight2="ios-cart"
              itemCount={amount === "undefined" ? "0" : amount}
              itemWish={wishListLength !== null ? wishListLength : "0"}
            />
            <Content
              contentContainerStyle={{ paddingBottom: 10 }}
              showsVerticalScrollIndicator={false}
              
            >
              <Card transparent>
                <FlatList
                  removeClippedSubviews={false}
                  contentContainerStyle={{ flexDirection: "row" }}
                  directionalLockEnabled={false}
                  horizontal={true}
                  keyExtractor={this._keyExtractor}
                  showsHorizontalScrollIndicator={false}
                  data={categoryicons}
                  renderItem={({ item }) =>
                    <RoundImageButton
                      navigation={navigation}
                      roundImageText={item.cname}
                      roundImageSource={item.image_path}
                      clickHandler={() =>
                        this.handleTopCategoryIconClick(item.target_id)}
                      categoryID={item.target_id}
                      subCat={subCategories.filter(
                        cat => cat.category_id === item.target_id
                      )}
                      style={{
                        resizeMode: "contain",
                        flex: 1
                      }}
                    />}
                />
              </Card>

              <Card transparent>
                <Text style={styles.bannerHeading}>Trending Now</Text>
                <FlatList
                  removeClippedSubviews={false}
                  bounces={false}
                  keyExtractor={this._keyExtractor}
                  directionalLockEnabled={false}
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  data={bannerslider}
                  renderItem={({ item }) =>
                    <BannerSlider
                      //onPress={() => navigation.navigate("ProductList")}
                      onPress={
                        item.destination_type === "C"
                          ? () =>
                              this.handleOnPressTargetCID(
                                item.target_id,
                                item.name
                              )
                          : () =>
                              this.handleOnPressTargetUrlListing(
                                item.target_url,
                                item.name
                              )
                      }
                      //bannerImageText={}
                      bannerImageSource={item.image_path}
                      destinationType={item.destination_type}
                      targetUrl={item.target_url}
                      targetId={item.target_id}
                      //bannerSmallText={}
                    />}
                />
              </Card>

              <Card
                transparent
                style={{ justifyContent: "center", alignItems: "center" }}
              >
                <TouchableHighlight
                  onPress={
                    minibanner[0].image_paths[0].destination_type === "C" ||
                    minibanner[0].image_paths[0].destination_type === "P"
                      ? minibanner[0].image_paths[0].destination_type === "C"
                        ? () =>
                            this.handleOnPressTargetCID(
                              minibanner[0].image_paths[0].target_id,
                              minibanner[0].name
                            )
                        : () =>
                            this.handleOnPressProductPage(
                              minibanner[0].image_paths[0].target_id,
                              minibanner[0].image_paths[0].absolute_path
                            )
                      : () =>
                          this.handleOnPressTargetUrlListing(
                            minibanner[0].image_paths[0].target_url,
                            minibanner[0].name
                          )
                  }
                >
                  <View style={{ alignItems: "center" }}>
                    <Image
                      source={{ uri: minibanner[0].image_paths[0].image_path }}
                      onLoadStart={() => this.setState({ imageStatus: true })}
                      onLoadEnd={() => this.setState({ imageStatus: false })}
                      // {...{
                      //   preview:
                      //     "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAFjCAYAAAATuV17AAAX9klEQVR42u3diY7rOLJF0f7/j615Hh/OfYhulYrUZNmWzGVgA1WZN51pyuYmI4Kh/3z11Vd/AwCwl/8YBAAAgQAACAQAQCAAAAIBAIBAAAAEAgAgEAAAgQAACAQAAAIBABAIAIBAAAAEAgAgEAAACAQAQCAAAAIBABAIAIBAAAAgEAAAgQAACAQAQCAAAAIBAIBAAAAEAgAgEAAAgQAACAQAAAIBABAIAIBAAAAEAgAgEAAACAQAQCAAAAIBABAIAAAEAgAgEAAAgQAACAQAQCAAABAIAIBAAAAEAgAgEAAAgQAAQCAAAAIBABAIAIBAAAAEAgAAgQAACAQAQCAAAAIBABAIAAAEAgAgEAAAgQAACAQAQCAAABAIAIBAAAAEAgAgEAAAgQAAQCAAAAIBABAIAIBAAAAEYhAAAAQCACAQAACBAAAIBLgVX3/99Re++eabL3z77bd/f/fdd//l+++//y8//PDDJqY/E+q58tyhfld+r2sAAgFuJoqpLDLp//TTT3///PPPf//6669f+P333//+448//v7zzz+/sPfx119/ffm5PEf47bffvjzvL7/88uX3/Pjjj19+71wqJRZyAYEAF5HGVBSZxDOh1+Q+FUUm/imPPubPV2KZyiWyilwilvx92bVEKkQCAgFeTCbf+a4iwshEXZK44iN/Vwklf+90txKpRISuLwgEOHGXUeGoTLKZbDPpliw+4VEhsLyuiLHyKXYoIBDgIBFHpFHCODP0dNUdSoXAsqvKDisyIREQCLAjPFWhqTN2GtOEdyW7pwnvTNTZ2UyZV17Nv5+fqRBanmcaRqucyyOiq7+58ieVOxHmAoEAjZ1GJuZMxpmAjzzmVVEli5JEleBOS24fCRNNw2tVyluyqWT+WWLJz8zDXHYmIBAMndvIRDgPUe0N99RKPRN2iaJKZ0sQc55VEdaiXmflbyKVqUz2vu78XGSS55u+Tu8rEAiGkEcm+flEumen0Uo6Xz3xPN2xTIsCIr4S6JEQV34+z+W9BQLBx+c3asLcI4zpWYpMuiWNT5DpvMKszrDs2ZWUUOVJQCD4uFBVJraIY8ukOA1PRRwjTYwllKM7tPz7qt4S2gKB4NbJ8ZoIt0yCJY6pNEaN8c9P2JdMtoikigkibMl2EAhuN/nVjmNrbL8mPKeyl3cl2V1EJltFktBW7UiMJQgEl57oMlFlwsrEtXWlnAmxKoqM474x3rIryfczxlWdZhxBILhkZdXWHUfkIcTyeIgrYx6RbM0tRSTGHASCy4WrthwAzL+Z5jeM4XnhrVyDtVP7lWPKDsb4g0Dw1rLchJ7WVsDVhbZCVfIbz2Ea2loTSe0AIx7XAwSCl696s5NYqq6anpquiUro5DXXJiJZCyeW2Evqrg0IBC/JdWxZ5WYCq+S4yen1JdTVJmbrbkSlFggETw1Z1cp2S0lu8hzE8X6RROJ1FmdLpZaQFggEp5eOrp0kr5PjmbCI43ryz25krey3TrKTCAgEp1VZLYWsKteRCUq46tq7kbqWawuBLBZcSxAIHpJHdhNrydhpa3Hjdp+DiEtl13VXRF1+QSA4HDtfm2S0Er/34mBtN5Lvy2WBQLBLHmvVO/leVrEmlvvnRtZ6a2UHmuS6aw0CweqEsnSiuU4yq9b5vAT70oJBch0EgtWTzEuVVtNST6vRz9t1rpVokwgIBIfKdB02G+uQaO99QCIgEPxr57F00EyJ7nhl20t5kZKIqjsQCHlsmiysOMcMZ25ZVBgvAgF5NOXhVLmCiiWJWFwQiIEYeHJYkodkOapCa22RQSIEgoEqbpZWltXPyqSALWW+dqoEAocE/yEPkwH2vm/sWAkEHz4JRA5LK0mTAB6RiLYnBIIPLc9c6m0llo1HE+vVpcBZIQLBB9b2904Z62uFvRLpVe9VtwLlvQSCDzpl3jtdXPX8dh44qwQ8X/OeIhAMEHKwWsQzdrV1ANWulkBw4w95PsS9pGd2JeLVeOT9laKLXl5NUp1AcPMPd2+FWGWXxgrPWqTUDtcihUBww9BVdhi9pLmzHjj7YOpSPsR7jUDwASfN84HWvwjPqsxyUp1A8AHnPZZCCuSBZ1VmLYVMhbIIBBcnScte6Cpfl9TEszsdtJLqWbxkV6zij0Bw4d1Hr3OqvAfenQ/Je1DhBoHgZiWVDnbh1aGs3i5YVRaB4KJJzFb8OfJw3gOvJouZXh4uOxQ7YQLBxRLnrbCBJol414KmF06tA4bGiUBwkcR57zRwVnvkgStVZWmhQyC4WNJyqZWEccI7T6n38nKKOggEF0ict1Z5qq5w5QWONicEggt8OFsttd3YB3c52KpjL4HgTR/MXu7DrWmPj2mkHBKfr/9ufc3Y7lvo9BLqqRCUCyEQvKHKpXdDH+1KtsmiKDlEulktZ1WcsEsmvSn5Wr6Xf5N/mx1efm76XMTST6i3FjvuhkkguFCdfZXtGqO+ODKZZfwycUW2Cfdl3DKee8jP5Gezio5gMu7ZFZJ3fxcyf8/WOSW7EALBBdpFOHHeH7NM7lNpZDJrjeGRR54nQslkmGsTmURUVtbrh10VfBAIXlx51ct9KNv9d04jE3nEmsnrLGGsPTIpRlS5VhXmck2+auZCdEsgELxwUmyVRWbC0u3033H3ui/Kq8TRui6ZHHUD+N+h14xHSyJCrwSCN53utfv45xjVjqN3P/hXP3J9akcy8m6kDhe2hK5rAoHgTYlIlVf/K23uVaddRSSj3xFyqfxcLoRA8Ibdx+gfvLqZ0SvzHI8k27MQGDU3srQQ0qmXQPDi0t3RG9PVhJSJ+cryaOVGRgw7Lh2CzQJAHo9A8IQSyFbpbrWDGHlcWqvZuzxGvc1wteFR0ksgeNHuY75iq55XoybPe6GQO0pktBLWSqb3DhYq6SUQvKB+ftSDg9Ow1d0fo5Zg9259m/FQUUggOHG11tvuZ2cyasL87J1HJbirPUnGPBN7yH/na+EZ5cHVE2qkxUDe172FkTMhBIITyx5b1Vcj9hCq9uBn7TxaLUgy3tUocUq+lu9F2pn4IpXqoXVWie9o8X+FIQSCN8T58/8jhq8yubbCHkflEWlUEvvIxJ1JrnfXvaP5kJGuaYWx5ruQLJjckoBAcEKVUWvCHPED1ku8vrtPVTVqPCMnM9rCYOlMSL5OIASCB1dovS3+aLuP3tmBvWGiOsR31cOMoyWRe9c1CycCIRA8GO/vtW0f6cOVybnXQ2lPruPZieq6Zo9IZLTquqVdtpb4BIIHt/fziWjEu7j1YuV7E9SvmJSXWu5vfYx0vqd3fxsteggEJ1df1UGr0erkH9l9vKNE9tF8zUg5gF5lnSahBIIHaJ11qIllpBLH3jmYPZVW75iElu4cubUia4TrXLcZXgpjmQ8IBAdWsK0JcbRDVnUTojtOwr0OylvDbqNU2i31xiIQAsHB1WtLICOFr0qkR/IJV8gV9U5b72lxMkIIp8ap9X53HoRAYEt/eCyOhoGukivqdRLYmkwfJYy1FLKVByEQ7KziaSXQR2vxkInjSPiqch9XGKteNd3WXcgIC4a6R0jvPS+MRSDYeeah1b5ktIZ7EcCR8NXVckWZHI9UZNXrGCGEE0m0dptV0iyMRSB4oL3DSEnVRyfeq63ce4flHCrcdu5JHoRA8GDYZsT8R+8k/tqke7Wxql1lde/dSv79SPcKiSha13u0g7MEgocmm1bSNV8bKXx1tH3JFXNFVRiRCTJS3EN2YaMIJK+1db0jUQIhEGycaFpx/9FO5R5NPldH2ytOunlNRxipZY37gxAITu6jVFVFI63CMmEcFchoxQafdM1bu+8IxG1uCQQbD871KrAI5FqNE/GcE+m9G0wZIwLBwQqs0TqTRiBHDhGOWK32aR0YWgLJAsoYEQgOfoBGmxQTD2+tRrcIxLmBzzoDVTcCM0YEggNb+JT1jnZznaN3ICSQe7//W63dq5zZGBEINiQR5wIZsZ3D0ZsyCWF9XhFJdiRZRBkjAsGG1h0tgYxWxnhUIO4hcW+B9Hpi5WvGiECwIpBWHfyIHUmPCiQrVRVY9859tQSS94LxIRAcFMhoIRkCcRZkHpo0PgSCldVX6zFiL6BHQlgEcu9EeqsXXHYhxodAQCAEAgIhEBAIgeA5pewEQiAgEAIBgRAI3imQK91dj0Dw7FLelkDyMD4EAgIhEBAIgYBACAQEQiAgEAIBgYBAJNEJhEA+TSCS6AQCAiEQqMIiEBAIgcBBQgIBgRAICAQEcn+BtO7Ap5kigYxCmim2BJImo8aHQLDy4dHOnUBG/wzoxksgOPjhad1QKrfzdEMpAhkBN5QiEDy4+nJHQvcDGbWEtyUQt7QlEOyI/84Fkq8lPzJSHoRAxkyg9+6JnkWUMSIQrHyAEq6aCyQrsqzMCGRbCGu03donvf9TcTjPA+Z9kDygMSIQbBBI6wOUdiYEsv7Iz4wm2096/0cU8/d/FgURizEiEKzEgHsrsNHOgkQCBDLmKfTWDnzEfnAEgt0CyQdlPnHmAzXaWZDkfFr5IAIZ7wxIvpZraowIBBuqUFor76zMRkoOZzJp5YO2CCThLwK556Kh997P94wRgeDgQarRqosyDtl1HRFIdnEqse4ZtmwdpM1CwvUkEGyMA7e28aNVFx0VSCag5ItMOPcsnNDKh0DwhFLe0WL7VZFzRCBXbf2Sa3eEkQpI5tc7/z9iM1ECwam18KOtrGsc9gok//6KJ/fzerIAyCp7D4n9j3DNeztOOS0CwQmVWBHISD2xahyOCCQhwCuNU15LJsH8XbmOW6kDdCNc88h1XnVX11ICnUCwY7KpEtZWQ7lRPkxVkdZKqm4JY11pnHrnG7bIcIRFQy0W5te6ytflswgEByacq0+MrwhrHDlMmIknK/5PeB0jxP8r/9F6/aN1YCAQnPKBysrr6hPjK0TaKmm+28o9O6m9u49aMIxwgC7v91yvUV8/geBlFSkjbemPhn6mHYzveiCyEsgj5D96J9Dz+uU/CAQHV62tliYjncotkR4J/1TV2jvDH73eZnt2UZ++WKgCg9ZNpEbrvkAgOLWtQ2v1PVJZY+8GQ3e5N0hW1q1c1p4T9Z9+nXsdeB0IJRA8+MFqlbGOdrCqV1Bw9VPplcc6svso+Y0QvlrqvKApJoHgCb2BRtvaHzlQ+M6VfIVljoTeRuu+3Gug6M6SBIITQiCt8M1oq7MKYx2VSH72VWG/adjtkb93hOqj2mW3zn8k/2P3QSB48APWukPhiG1NjvTFmh/CfPZOZHri/OjfWs0DR7i2S3k+5z8IBE9qazLiFv/oqfSWRJ4xbnnOM+RRTTNHrTS8Ugk2COQjwlitJPJIE810N/bI5Dzt1ntWk8JIPtcoYnokbDXiznKp+678B4HgxFO6Pmj9nNCR3UjdZ/6RSqezdh0j7ip7C6NIdKRuCwSCp9NLNI621V8K6R1d8UdImcgik2qfnsktE/mUfC3fy64vf0OknvE/628ZKe6/VKU2UsNQAsFbk42ZADPpjFZY8Mj5irVdScY5csjvmFPSOPt3X/kmWK/cVTs8SCB44sq7FSYZsdyx+kudPZGXSNY4+/eN1J5mKRQ5Wl6PQPDSXUhrwhz1xO4jTQqv9BgtDNm742aNhdJdAsELJ8y6U+GIH7xn7kReJY/RVtyRZata7QrNL0EgHx3G6tXNj9z2+o4SqQKI0aqNlkKxV7yPPQjk47b/vdLHkZOPmXiOtn1/hzwq5zHaartXDDJag1ACwdtWcL348Sj9k9b6Kp11JuMZjwiuDjGOeI1a792qfpM8JxC8cBU36v2zt4T5Mj6ZlK4ikkyaEduz2qjceffs5DmB4AItIPQQet4J8Ud3HTVBjir3pQOgbltLIHjDLqQ1OY50H4ktk1bGKSJJkv2V+ZFKkkcc2RGNvrqucx+9CkK7DwLBG1Z0vVyIFd2/J7BqP5LxqfDWWbuTPM+0LUok7m5665VX9V41TgSCi5zmrZvxKIn890SWlW6rl1Um/2LtFPr03857adVuI7/LpPj/LN3T3m6ZQPDmiqxWaCYTm6qW/riVTOYNEqvnVYTQor5fsqjGi9V80WTYfo/2dh+67hII3rwL6VW2OJi1P/E+lUqLaZdeY7ZOBNHbJau8IhBc5EPayoUo68W7hZwQYe+e7xY3BIKL7EJ69wsftdEirnGos7Wwcb9zAsEFE5WtXEiFsoQK8I7Eea/nlfcjgeBiycreTZbqplNWfHjV7qPXZl/inEBww1YReg3hlVVXcnIEgg+relGVhWfLo3fmo07mO+BKILj4LqS3Aqz7blsB4pkl5a3QVe2AvfcIBBen1yfLBxnPXri03nPVGcF7jkBwo1BCr3mgXlk4m16nXXcaJBB8WDKzPtQkgjPeZ728m5ucEQhuHlboHTCssIJ6fDwijyxCevJQPk4g+IADXUv5EP2I8EiurXfeo+7zIXRFIPiAEEMvPq2tBM7e3U5DpN5XBIIPz4fU6WAfduypuFoq0pD3IBB8mESyYuw9EuZS3ost8liquMoiRasSAsGHHvTqxaydFMaWRciSPCocKqdGIBjwtHC1OyER7C3X1eWAQDBQ6WWvMmsqEZMBthRi5D0Tedh5EAgGOqneu1uccBa27jzqPJFyXQLBgBPD0k5EYl3CPDmNpbAVeRCIgbC67Nbzk4hqq97OQ74MBGKiWJRIvlbnRMS4xym0WDo3JE8GAsG/yjOX4tz5nrYn45R6r8nDQUEQCJo5kd4j4YxU24h5f25hRa/Ee5owF7YCgWB3dZbY9+eylgtTbQUCwWkr0cqLGLP758CW+lpNDwmSBwgEm6hW3b1YeDXNS+7ExHLfA6URw9I11vIfBIKnJFSnq9NMRiaZ++w6tuwytfoHgeChVWqFONYkkryJyeYe8ojwI4eePKYt/i0KQCA45azIUnJ9WqWlxPO6VXbZdSwtBipZ7vAoCASnUmGPpQmoTq9nskoIzCT0fnHUwcClRLkybRAIXjIZrYVA8vXqk+S08nvDj2vluboNgEBwyZBWTUyVZCeS14arIvC1Xcf0cKDrAwLBS8+LbJ2kEvpKkt1E9fzqqgh7y64jCwAhKxAI3jppVR+tpQlrOmlVqIRIXp/nmPezcg1AILjFobRpfqTKfsXczxn3CHxt7OeHP8kDBILL7Ua2VGpV1U9CYBXaIpN9oarsOPaOtV0HCAS3CKdsXRVXaKtEYnXclkbGpXIcawcBp6fJIxkVViAQfGRYaxramu5K5En+KeRINmOUsdoijwpXGUcQCG5fGbRW9jtfOVeV0Ggx+4xZ5JvXHZmWOLaOnYo3EAg+tux3a2hrGr+vSTHPUfmST5kca5eR11W5jb3SKOHmZ92rBQSCj5TINLRVcfy1cExNkhXmqsOJ0+R7PXdx5ddfTJPhU7HuGZMaD5VVIBAM1WajwjR7VtvzfEkmz/x85U0yIV9xIq3Xm2R2CSN/d15D2PP6p1VVeT4VbCAQDJ1sn6/C9z6mYZyEu2qHEjLJVugrcslkO921nLGrqCqp/I4KReX35vdPhXH09ZU05nkh7yEQCNA5GLcllLP2qDBP7VTy/JnUW3IpSjJT6ns9SeR5I6/aWZz1t08PXpIGCARYCW/VRL3nsNzWPEqFwKZUKGkr85+fPvej4qgS3PkhS/kNEAhwQu7grN3JFR7TA395fXmd7qcCAgGesDuZtvHISn26IzhjB/BMUSwVADi3AQIBXlQOW7mJquaqHMS8HPZdQpmHzCrBP91hnJXMBwgEeCDUVTKZJ7gjlqzyK8Hd2rUcFcQ0h1I7igpFVbVUqwqMMEAgwA3EUk0I52W1Lan0EuXTr+ffhvzsVBZV2TWVBVGAQAAAIBAAAIEAAAgEAEAgAAACAQCAQAAABAIAIBAAAIEAAAgEAAACAQAQCACAQAAABAIAIBAAAAgEAEAgAAACAQAQCACAQAAAIBAAAIEAAAgEAEAgAAACMQgAAAIBABAIAIBAAAAEAgAAgQAACAQAQCAAAAIBABAIAAAEAgAgEAAAgQAACAQAQCAAABAIAIBAAAAEAgAgEAAAgQAAQCAAAAIBABAIAIBAAAAEAgAAgQAACAQAQCAAAAIBABAIAAAEAgAgEAAAgQAACAQAQCAAABAIAIBAAAAEAgAgEAAACAQAQCAAAAIBABAIAIBAAAAgEAAAgQAACAQAQCAAAAIBAIBAAAAEAgAgEADArfg/4FyyPqOfJaQAAAAASUVORK5CYII=",
                      //   uri: minibanner[0].image_paths[0].image_path
                      // }}
                      resizeMode="contain"
                      style={{
                        width: deviceWidth * 2 / 2.1,
                        height: deviceHeight * 2 / 4 - 120,
                        marginLeft: 10,
                        marginRight: 10
                      }}
                    />

                    {this.state.imageStatus &&
                      <View
                        style={{
                          justifyContent: "center",
                          alignItems: "center",
                          position: "absolute",

                          opacity: 0.7,
                          backgroundColor: "transparent"
                        }}
                      >
                        <Image
                          style={{
                            width: deviceWidth * 2 / 2.1,
                            height: deviceHeight * 2 / 4 - 120,
                            marginLeft: 10,
                            marginRight: 10
                          }}
                          source={require("../../../assets/placeholder.png")}
                        />
                        <ActivityIndicator size="large" color="#fff" />
                      </View>}
                  </View>
                </TouchableHighlight>
              </Card>

              {/* <Card transparent>
                <To
              </Card> */}

              {sliderwidget.map((widget, index) =>
                //console.log('output widget ....\n',widget)
                <Card transparent key={"card" + index}>
                  <Text style={styles.bannerHeading}>
                    {widget.name}
                  </Text>
                  <FlatList
                    //key = {widget.name}
                    removeClippedSubviews={false}
                    directionalLockEnabled={false}
                    horizontal={true}
                    keyExtractor={this._keyExtractor}
                    showsHorizontalScrollIndicator={false}
                    bounces={false}
                    data={widget.image_paths}
                    renderItem={({ item }) =>
                      <BannerSlider
                        imageStyle={styles.bannerSliderImage}
                        //onPress={() => navigation.navigate("ProductList")}
                        onPress={
                          // () => this.handleOnPressTargetUrlListing(item.target_url)
                          item.destination_type === "C" ||
                          item.destination_type === "P"
                            ? item.destination_type === "C"
                              ? () =>
                                  this.handleOnPressTargetCID(
                                    item.target_id,
                                    item.name
                                  )
                              : () =>
                                  this.handleOnPressProductPage(
                                    item.target_id,
                                    item.absolute_path
                                  )
                            : () =>
                                this.handleOnPressTargetUrlListing(
                                  item.target_url,
                                  item.name
                                )
                        }
                        bannerImageSource={item.image_path}
                        //bannerSmallText={"dummy"}
                      />}
                  />
                </Card>
              )}
            </Content>
            <MyFooter navigation={navigation} selected={"home"} />
          </Container>
        );
      }
    }
  }
}
const mapStateToProps = state => ({
  homePageState: state.homepage,
  user: state.user
});

export default connect(mapStateToProps, { fetchWishListData, fetchCartItems })(
  HomePage
);
