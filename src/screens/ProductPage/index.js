import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchCartItems, updateUserGid } from "../../redux/actions";
import {
  ScrollView,
  Dimensions,
  View,
  TouchableOpacity,
  Text,
  Alert,
  Modal,
  TouchableHighlight,
  ActivityIndicator,
  TouchableWithoutFeedback
} from "react-native";
import {
  Container,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Tabs,
  Tab,
  Footer,
  Right,
  Header,
  Body,
  Left,
  Title
} from "native-base";
import { Image } from "react-native-expo-image-cache";
// import { observer, inject } from "mobx-react/native";
import Swiper from "react-native-swiper";
import IconSLI from "react-native-vector-icons/SimpleLineIcons";
import IconMCI from "react-native-vector-icons/MaterialCommunityIcons";
import ThemeHeader from "../../components/Header";
import ImageModel from "../../components/ImageModel";
import ToastModal from "../../components/ToastModal";
import commonColor from "../../theme/variables/commonColor";
import styles from "./styles";

var deviceHeight = Dimensions.get("window").height;
var dwidth = Dimensions.get("window").width;
const productUrlPrefix = "https://www.alnasser.pk/api/Extendedproducts/";
const headers = new Headers();
headers.append(
  "Authorization",
  "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
//headers.append("method",'GET')

// @inject("view.app", "domain.user", "app", "routerActions")
// @observer
class ProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      saveStatus: "SAVE",
      bagStatus: "ADD TO BAG",
      wishStatus: "ADD TO WISHLIST",
      button1: false,
      button2: false,
      button3: false,
      button4: false,
      modalVisible: false,
      // Product Details from API
      productId: null,
      productDetails: null,
      productVariants: null,
      isProuctDetailLoading: false,
      isProductDetailLoaded: false,
      error: null,
      isModalVisible: false,
      isModalVisible2: false,
      isModalVisible3: false,
      isFetch: false,
      sizeChart: [],
      isBag: false,
      isWish: false,
      imageStatus: true
    };
  }

  componentWillMount() {
    this.makeRemoteRequest();
  }

  _showModal = () => this.setState({ isModalVisible: true });

  _hideModal = () => this.setState({ isModalVisible: false });

  _showModal2 = () => this.setState({ isModalVisible2: true });

  _hideModal2 = () => this.setState({ isModalVisible2: false });

  _showModal3 = () => this.setState({ isModalVisible3: true });

  _hideModal3 = () => this.setState({ isModalVisible3: false });

  makeRemoteRequest = () => {
    this.setState({
      productId: this.props.navigation.getParam("TargetId")
    });
    const productId = this.props.navigation.getParam("TargetId");
    const url = `${productUrlPrefix}${productId}`;
    console.log("Going to fecth URL = " + url);
    this.setState({ isProuctDetailLoading: true });
    headers.append("method", "GET");
    fetch(url, {
      headers: headers
    })
      .then(res => res.json())
      .then(res => {
        if (res.product_options) {
          this.setState({
            productDetails: res,
            sizeChart: res.size_chart,
            productVariants: res.product_options[0].variants.map(v => ({
              name: v.variant_name,
              variantId: v.variant_id,
              optionId: v.option_id,
              state: false
            })),
            error: res.error || null,
            isProductDetailloading: false,
            isProductDetailLoaded: true,
            isFetch: true
          });
        } else {
          this.setState({
            productDetails: res,
            sizeChart: res.size_chart,
            error: res.error || null,
            isProductDetailloading: false,
            isProductDetailLoaded: true,
            isFetch: true
          });
        }
      })
      .catch(error => {
        this.setState({ error, loading: false, refereshing: false });
      });
  };

  addToBag() {
    const { userId, gId } = this.props.user;
    var gid = null;

    if (userId) {
      addToCartUrl = `https://www.alnasser.pk/api/Addtocart/${userId}`;
    } else if (gId) {
      addToCartUrl = `https://www.alnasser.pk/api/Addtocart/${gId}`;
    } else {
      addToCartUrl = `https://www.alnasser.pk/api/Addtocart/`;
    }
    // !userId && gId !== null ? addToCartUrl = `http://clicky.pk/api/Addtocart/${gId}` :
    //                           addToCartUrl = `http://clicky.pk/api/Addtocart/`
    var methodType = "";
    userId !== null || gId !== null
      ? (methodType = "PUT")
      : (methodType = "POST");
    console.log("add to cart url: ", addToCartUrl);
    var cartData = {};
    var variants = this.state.productVariants;
    //console.log("Product Variant", variants);
    // if (variants === null || variants.length < 1 ) {
    if (this.state.productDetails.amount === "0") {
      this._showModal3();
      return;
    } else {
      if (variants === null) {
        cartData = {
          amount: 1,
          product_options: {},
          extra: [],
          product_id: this.state.productId
        };
      } else {
        var selectedOption = variants.filter(v => v.state === true);
        if (selectedOption.length > 0) {
          // User has selected atleast one option
          cartData = {
            amount: 1,
            product_options: {},
            extra: [],
            product_id: this.state.productId
          };
          cartData.product_options[selectedOption[0].optionId] =
            selectedOption[0].variantId;
        } else {
          //Alert.alert("Please select option first");
          this._showModal2();
          return;
        }
      }
    }
    //console.log('AddToCart Object,',cartData)

    fetch(addToCartUrl, {
      method: methodType,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
      },
      body: JSON.stringify({
        product_data: cartData
      })
    })
      .then(res => res.json())
      .then(res => {
        console.log("Add to cart response:", res);
        console.log("Add to cart response:", gId);

        if (gId === undefined || gId === null ) {
          res.g_id !== null ? (gid = res.g_id) : (gid = null);
          if (gid !== null) updateUserGid(gid);
        }
      })
      .then(() => {
        var { gId, userId } = this.props.user;
        if (userId) this.props.fetchCartItems(userId);
        else if (gId) this.props.fetchCartItems(gId);
        else this.props.fetchCartItems(null);
      })
      .then(() => this._showModal());
    this.setState({
      bagStatus: "GO TO BAG"
    });
  }

  addToWish() {
    const { userId, gId } = this.props.user;
    var gid = null;

    if (userId) {
      addToWishUrl = `https://www.alnasser.pk/api/Addtowishlist/${userId}`;
    } else if (gId) {
      addToWishUrl = `https://www.alnasser.pk/api/Addtowishlist/${gId}`;
    } else {
      addToWishUrl = `https://www.alnasser.pk/api/Addtowishlist/`;
    }
    var methodType = "";
    userId !== null || gId !== null
    ? (methodType = "PUT")
    : (methodType = "POST");
    console.log("add to wish url: ", addToWishUrl);
    var cartData = {};
    var variants = this.state.productVariants;
    if (this.state.productDetails.amount === "0") {
      this._showModal3();
      return;
    } else {
      if (variants === null) {
        cartData = {
          amount: 1,
          product_options: {},
          extra: [],
          product_id: this.state.productId
        };
      } else {
        var selectedOption = variants.filter(v => v.state === true);
        if (selectedOption.length > 0) {
          // User has selected atleast one option
          cartData = {
            amount: 1,
            product_options: {},
            extra: [],
            product_id: this.state.productDetails.product_id
          };
          cartData.product_options[selectedOption[0].optionId] =
            selectedOption[0].variantId;
        } else {
          this._showModal2();
          return;
        }
      }
    }
    //console.log('AddToCart Object,',cartData)

    fetch(addToWishUrl, {
      method: methodType,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization:
        "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
      },
      body: JSON.stringify({
        product_data: cartData
      })
    })
      .then(res => res.json())
      .then(res => {
        console.log("Add to Wish response:", res);
        if (gId === null) {
          res.g_id !== null ? (gid = res.g_id) : (gid = null);
          if (gid !== null) updateUserGid(gid);
        }
      })
      .then(() => {
        var { gId, userId } = this.props.user;
        if (userId) this.props.fetchCartItems(userId);
        else if (gId) this.props.fetchCartItems(gId);
        else this.props.fetchCartItems(null);
      })
      .then(() => this._showModal());
    this.setState({
      wishStatus: "GO TO WISHLIST"
    });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  toggleSizeSelect(id) {
    this.setState({
      button2: !this.state.button2,
      productVariants: this.state.productVariants.map(v => {
        if (v.variantId === id)
          return {
            variantId: v.variantId,
            optionId: v.optionId,
            state: !v.state,
            name: v.name
          };
        if (v.state === true)
          return {
            variantId: v.variantId,
            optionId: v.optionId,
            state: false,
            name: v.name
          };
        return v;
      })
    });
  }

  render() {
    const navigation = this.props.navigation;
    const {
      isProductDetailLoaded,
      productVariants,
      bagStatus,
      wishStatus
    } = this.state;
    const { userId, gId, cartItems } = this.props.user;
    let amount = cartItems.cart ? cartItems.cart.amount + "" : "0";
    console.log("userId:", userId);
    // console.log("gID", gId);
    // const product = navigation.getParam("Product");
    // const title = navigation.getParam("Name", "Nice Product");
    // const preview = require("../../../assets/arrow_black.png");
    return !this.state.isFetch
      ? <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            opacity: 0.7,
            backgroundColor: "transparent"
          }}
        >
          <ActivityIndicator size="large" color="green" />
        </View>
      : <Container>
          <Header
            noShadow
            style={{
              marginTop: 0,
              marginLeft: 0,
              marginRight: 0,
              borderRadius: 3,
              backgroundColor: "white",
              marginBottom: 2
            }}
          >
            <Left>
              {
                <Button
                  transparent
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Icon name="ios-arrow-back" />
                  {/* <Image
                  source={require("../../../assets/arrow_black.png")}
                  
                  resizeMode="contain"
                  style={{ height: 23, width: 40, marginRight: 10 }}
                /> */}
                </Button>
              }
            </Left>
            {this.state.isFetch &&
              <Body>
                <Title
                  style={{
                    color: "black",
                    fontSize: 16,
                    fontWeight: "400",
                    alignSelf: "flex-start"
                  }}
                >
                  {this.state.productDetails.product}
                </Title>
              </Body>}
          </Header>
          <Content
            style={{ backgroundColor: "#fff", marginBottom: 0 }}
            contentContainerStyle={{ paddingBottom: 10 }}
          >
            {this.state.isFetch
              ? <View style={{ position: "relative" }}>
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={true}
                    style={{ flexDirection: "row" }}
                  >
                    {this.state.productDetails.image.map((item, index) =>
                      <View key={"sort" + index}>
                        <Image
                          style={{
                            width: dwidth - 2,
                            height: 450,
                            flex: 1
                          }}
                          resizeMode="contain"
                          //source={{ uri: item.main }}
                          {...{
                            preview:
                              "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg==",
                            uri: item.main
                          }}
                          onLoadStart={() =>
                            this.setState({ imageStatus: true })}
                          onLoadEnd={() =>
                            this.setState({ imageStatus: false })}

                          //key={imgUri}
                        />
                        {this.state.imageStatus &&
                          <View
                            style={{
                              justifyContent: "center",
                              alignItems: "center",
                              position: "absolute",
                              left: 0,
                              right: 0,
                              top: 0,
                              bottom: 0,
                              opacity: 0.7,
                              backgroundColor: "transparent"
                            }}
                          >
                            <ActivityIndicator size="large" color="#fff" />
                          </View>}
                      </View>
                    )}
                  </ScrollView>
                </View>
              : <View style={{ position: "relative" }}>
                  <Image
                    style={{
                      width: dwidth - 2,
                      height: 450,
                      resizeMode: "cover",
                      flex: 1
                    }}
                    source={require("../../../assets/placeholder.png")}
                  />
                </View>}
            {this.state.isFetch &&
              <List>
                <View style={{ marginTop: 15 }}>
                  <Text style={styles.productName}>
                    {" "}{this.state.productDetails.product}
                  </Text>
                </View>
                <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ fontSize: 18, color: "black" }}>
                      {this.state.productDetails.formatprice}
                    </Text>
                    {this.state.productDetails.discount !== 0 &&
                      <Text style={styles.cutOffPrice}>
                        {this.state.productDetails.list_price}
                      </Text>}
                    {/* <Text style={styles.discount}>
                    {" "}{this.state.productDetails.discount}% off
                  </Text> */}
                  </View>
                </ListItem>
                {this.state.productDetails.discount !== 0 &&
                  <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View
                        style={{
                          width: 60,
                          height: 21,
                          borderRadius: 3,
                          borderWidth: 1,
                          borderColor: "red"
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 13,
                            fontWeight: "400",
                            color: "red",
                            textAlign: "center"
                          }}
                        >
                          {this.state.productDetails.discount}% Off
                        </Text>
                      </View>
                    </View>
                  </ListItem>}
              </List>}
            <View style={{ paddingTop: 10 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={styles.sizeView1}>
                  <IconMCI
                    name="ruler"
                    style={{ fontSize: 18, color: "#999" }}
                  />
                </View>
                <View style={{ flex: 2, alignItems: "flex-start" }}>
                  <Text style={styles.sizeText}> Size </Text>
                </View>
                <Right style={{ flex: 8 }}>
                  {this.state.sizeChart.length == 0
                    ? // this.state.productDetails.size_chart.length == 0?
                      <Text
                        style={{
                          fontSize: 15,
                          color: commonColor.brandPrimary,
                          marginRight: 5
                        }}
                      >
                        No Size Chart Available
                      </Text>
                    : <ImageModel
                        Size={this.state.sizeChart.icon.absolute_path}
                      />}
                </Right>
              </View>
              {this.state.isFetch &&
                <View style={{ position: "relative" }}>
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    style={{ flexDirection: "row" }}
                  >
                    <View style={styles.sizeButtons}>
                      {this.state.productDetails.product_options &&
                        productVariants.map(variant =>
                          <TouchableOpacity
                            style={
                              !variant.state ? styles.sizes : styles.sizesAlt
                            }
                            onPress={() =>
                              this.toggleSizeSelect(variant.variantId)}
                            key={variant.variantId}
                          >
                            <Text>
                              {variant.name}
                            </Text>
                          </TouchableOpacity>
                        )}
                    </View>
                  </ScrollView>
                </View>}
            </View>

            <View>
              <Text style={styles.capitalText}>MORE INFO</Text>
              {this.state.isFetch &&
                <View
                  style={{
                    padding: 10,
                    borderTopWidth: 1,
                    borderTopColor: "#ddd"
                  }}
                >
                  {/* <Text style={{ color: "#777" }}>
                  Company Name: {this.state.productDetails.company}
                </Text> */}
                  {this.state.productDetails.brand &&
                    <Text style={{ color: "#777" }}>
                      Brand: {this.state.productDetails.brand.value}
                    </Text>}
                </View>}
            </View>
          </Content>

          <Footer>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <TouchableOpacity
                style={styles.saveButton}
                onPress={() =>
                  //this.setState({ saveStatus: "SAVED" })
                  {
                    {
                      wishStatus === "ADD TO WISHLIST"
                        ? this.addToWish()
                        : navigation.navigate("WishList");
                    }
                    this.setState({ isWish: true, isBag: false });
                  }}
              >
                <IconSLI name="heart" style={styles.footerBtnIcon} />
                {/* <Text style={styles.footerBtnText}>
                  {" "}{this.state.wishStatus}
                </Text> */}
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.bagButton}
                onPress={() => {
                  {
                    bagStatus === "ADD TO BAG"
                      ? this.addToBag()
                      : navigation.navigate("BAG");
                  }
                  this.setState({ isBag: true, isWish: false });
                }}
              >
                <IconSLI name="handbag" style={styles.footerBtnIcon} />
                <Text style={styles.footerBtnText}>
                  {" "}{this.state.bagStatus}
                </Text>
              </TouchableOpacity>
            </View>
          </Footer>
          {this.state.isModalVisible
            ? <Modal
                animationType={"slide"}
                transparent={true}
                visible={this.state.isModalVisible}
                onRequestClose={() => null}
              >
                <TouchableWithoutFeedback onPress={() => this._hideModal()}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      justifyContent: "flex-end",
                      alignItems: "center",
                      alignContent: "center"
                    }}
                  >
                    <View
                      style={{
                        height: 200,
                        width: dwidth,
                        backgroundColor: "#fff",
                        alignItems: "center",
                        shadowRadius: 1,
                        borderRadius: 10,
                        justifyContent: "space-between"
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 18,
                          fontWeight: "700",
                          color: "green",
                          textAlign: "center",
                          marginTop: 10
                        }}
                      >
                        {this.state.isBag
                          ? "Product Added to Bag Successfully.!"
                          : "Product Added to WishList Successfully.!"}
                      </Text>

                      <View style={{ flexDirection: "row" }}>
                        <TouchableHighlight
                          onPress={() => (
                            navigation.navigate("HomePage"),
                            this._hideModal()
                          )}
                        >
                          <Text
                            style={{
                              fontSize: 17,
                              fontWeight: "400",
                              color: "green"
                            }}
                          >
                            Go to Home
                          </Text>
                        </TouchableHighlight>
                      </View>

                      <View
                        style={{
                          flexDirection: "row",
                          height: 50,
                          width: dwidth,
                          backgroundColor: commonColor.brandSuccess,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <TouchableHighlight
                          transparent
                          onPress={() => {
                            {
                              this.state.isBag
                                ? (
                                    this._hideModal(),
                                    navigation.navigate("BAG")
                                  )
                                : (
                                    this._hideModal(),
                                    navigation.navigate("WishList")
                                  );
                            }
                          }}
                        >
                          <Text
                            style={{
                              fontSize: 18,
                              fontWeight: "700",
                              color: "white",
                              textAlign: "center"
                            }}
                          >
                            {" "}{this.state.isBag
                              ? this.state.bagStatus +
                                " (" +
                                (amount === "undefined" ? "0" : amount) +
                                ")"
                              : this.state.wishStatus}
                          </Text>
                        </TouchableHighlight>
                      </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </Modal>
            : <View />}
          {this.state.isModalVisible2
            ? <Modal
                animationType={"slide"}
                transparent={true}
                visible={this.state.isModalVisible2}
                onRequestClose={() => null}
              >
                <TouchableWithoutFeedback onPress={() => this._hideModal2()}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      justifyContent: "flex-end",
                      alignItems: "center",
                      alignContent: "center"
                    }}
                  >
                    <View
                      style={{
                        height: 200,
                        width: dwidth,
                        backgroundColor: "#fff",
                        alignItems: "center",
                        shadowRadius: 1,
                        borderRadius: 10,
                        justifyContent: "space-between"
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 18,
                          fontWeight: "700",
                          color: "#050404",
                          textAlign: "center",
                          marginTop: 10
                        }}
                      >
                        Please select size
                      </Text>

                      {this.state.isFetch &&
                        <View style={{ flexDirection: "row" }}>
                          {this.state.productDetails.product_options &&
                            productVariants.map(variant =>
                              <TouchableOpacity
                                style={
                                  !variant.state
                                    ? styles.sizes
                                    : styles.sizesAlt
                                }
                                onPress={() =>
                                  this.toggleSizeSelect(variant.variantId)}
                                key={variant.variantId}
                              >
                                <Text>
                                  {variant.name}
                                </Text>
                              </TouchableOpacity>
                            )}
                        </View>}
                      <View
                        style={{
                          flexDirection: "row",
                          height: 50,
                          width: dwidth,
                          backgroundColor: commonColor.brandSuccess,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <TouchableHighlight
                          transparent
                          onPress={() => {
                            this.state.isBag
                              ? (
                                  bagStatus === "ADD TO BAG"
                                    ? this.addToBag()
                                    : navigation.navigate("BAG"),
                                  this._hideModal2()
                                )
                              : (
                                  wishStatus === "ADD TO WISHLIST"
                                    ? this.addToWish()
                                    : navigation.navigate("WishList"),
                                  this._hideModal2()
                                );
                          }}
                        >
                          <Text
                            style={{
                              fontSize: 18,
                              fontWeight: "700",
                              color: "white",
                              textAlign: "center"
                            }}
                          >
                            {" "}{this.state.isBag
                              ? this.state.bagStatus
                              : this.state.wishStatus}
                          </Text>
                        </TouchableHighlight>
                      </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </Modal>
            : <View />}
          {this.state.isModalVisible3
            ? <Modal
                animationType={"slide"}
                transparent={true}
                visible={this.state.isModalVisible3}
                onRequestClose={() => {
                  console.log("Modal close");
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    justifyContent: "flex-end",
                    alignItems: "center",
                    alignContent: "center"
                  }}
                >
                  <View
                    style={{
                      height: 100,
                      width: dwidth,
                      backgroundColor: "#fff",
                      alignItems: "center",
                      shadowRadius: 1,
                      borderRadius: 10
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        fontWeight: "200",
                        color: "green",
                        marginTop: 20,
                        marginBottom: 10
                      }}
                    >
                      Sorry, This Product is Out of Stock.!
                    </Text>

                    <TouchableHighlight
                      transparent
                      style={{
                        backgroundColor: commonColor.brandSuccess,
                        height: 40,
                        width: 40,
                        borderRadius: 100,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      onPress={() => {
                        this._hideModal3();
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "700",
                          color: "#fff",
                          textAlign: "center",
                          textAlign: "center"
                        }}
                      >
                        OK
                      </Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </Modal>
            : <View />}
        </Container>;
  }
}

//export default ProductPage;
const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps, { fetchCartItems, updateUserGid })(
  ProductPage
);
