const commonColor = require("../../theme/variables/commonColor");

export default {
  defaultDot: {
    backgroundColor: "#ddd",
    opacity: 1,
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3
  },
  activeDot: {
    backgroundColor: "black",
    opacity: 1,
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3
  },
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#9DD6EB"
  },
  pic: {
    flex: 1,
    resizeMode: "contain",
    alignSelf: "center",
    marginLeft: 23
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#97CAE5"
  },
  slide3: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#92BBD9"
  },
  alertBtn: {
    position: "absolute",
    bottom: 30,
    left: 10,
    padding: 10,
    backgroundColor: "rgba(255,255,255,0.3)"
  },
  productName: {
    fontSize: 16,
    color: "#090909",
    marginLeft: 10
  },
  cutOffPrice: {
    fontSize: 16,
    color: "#898C94",
    marginLeft: 10,
    textDecorationLine: "line-through"
  },
  discount: {
    fontSize: 16,
    color: "#7468C5",
    marginLeft: 10
  },
  tagText: {
    justifyContent:'flex-start',
    color: "#999",
    alignSelf: "center",
    marginTop: -4
  },
  sizeView1: {
    flex: 1,
    alignItems: "flex-start",
    marginLeft: 10,
    flexDirection:'row'
  },
  sizeText: {
    color: "#999",
    marginLeft: -15,
    marginTop: 0
  },
  sizeButtons: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 0,
    paddingVertical: 10
  },
  sizes: {
    alignItems: "center",
    justifyContent: "center",
    width: 45,
    height: 45,
    borderColor: "#B2BAB5",
    borderWidth: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginLeft: 10
  },
  sizesAlt: {
    alignItems: "center",
    backgroundColor: 'grey',
    justifyContent: "center",
    width: 45,
    height: 45,
    borderColor: "#7468C5",
    borderWidth: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginLeft: 10
  },
  capitalText: {
    padding: 10,
    fontSize: 14,
    color: "#777"
  },
  saveButton: {
    flex: 0.5,
    flexDirection: "row",
    backgroundColor: "#535766",
    alignItems: "center",
    justifyContent: "center"
  },
  footerBtnIcon: {
    fontSize: 20,
    color: "white"
  },
  footerBtnText: {
    fontSize: 16,
    color: "white"
  },
  bagButton: {
    flex: 1.5,
    flexDirection: "row",
    backgroundColor: commonColor.brandSuccess,
    alignItems: "center",
    justifyContent: "center"
  }
};
