import React, { Component } from "react";
import { connect } from "react-redux";
import { View,Image } from "react-native";
import { login } from "../../redux/actions";
import {
  Container,
  Content,
  Button,
  Input,
  Text,
  Form,
  Item,
  Header,
  Left,
  Right,
  Body,
  Title,
  Icon
} from "native-base";
import ThemeHeader from "../../components/Header/index.js";
import ToastModal from "../../components/ToastModal";

const headers = new Headers();
headers.append(
  "Authorization",
  "Basic " + "YWRtaW5AYWxuYXNzZXIucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      forgetPassword: false,
      recoveryEmail: ""
    };
  }

  // updatePassword(email){
  //   passwordRecover(email)
  // }

  updatePassword(email) {
    const orderUrl = "https://www.alnasser.pk/api/extendedusers/";
    console.log("starting Password Recovery Data");
    fetch(orderUrl, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
      },
      body: JSON.stringify({
        email: email,
        forget_password: "true"
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log("Password Recovery results: \n", json);
        if (json.message) {
          alert(json.message);
          // <ToastModal
          //   Toast = {json.message}
          //   />
        } else {
          // <ToastModal
          // Toast = {json.message}
          // />
          alert(json.message);
        }
      });
  }

  onLogin() {
    var { email, password } = this.state;
    var { gId, checkOutStarted } = this.props.user;
    //checkOutStarted = true
    gId === null || gId === undefined
      ? this.props.login(email, password, null, checkOutStarted)
      : this.props.login(email, password, gId, checkOutStarted);
  }

  render() {
    const navigation = this.props.navigation;
    var { forgetPassword, recoveryEmail } = this.state;
    var { loginError, gId } = this.props.user;
    return (
      <Container>
        {/* <ThemeHeader
          PageTitle="LOGIN"
          IconLeft="arrow-back"
          route="loginHome"
          navigation={navigation}
        /> */}

        <Header
          style={{
            marginTop: 5,
            marginLeft: 5,
            marginRight: 5,
            borderRadius: 5,
            backgroundColor: "white",
            marginBottom: 6
          }}
        >
          <Left>
            {
              <Button
                transparent
                //onPress={() => navigation.navigate("HomePage")}
                onPress={() => this.props.navigation.goBack()}
              >
                <Image
                      source={require("../../../assets/arrow_black.png")}
                      resizeMode = "contain"
                      style={{height:23,width:40,marginRight:10}}
                    />
              </Button>
            }
          </Left>

          <Body>
            <Title
              style={{
                color: "black",
                fontSize: 16,
                fontWeight: "400",
                alignSelf: "flex-start"
              }}
            >
              LOGIN
            </Title>
          </Body>
        </Header>

        <Content
          padder
          style={{ backgroundColor: "#fff", marginBottom: null }}
          bounces={false}
        >
          {!forgetPassword
            ? <Form>
                <Item style={{ marginLeft: 0 }}>
                  <Input
                    placeholder="Email address"
                    keyboardType= "email-address"
                    autoCapitalize="none"
                    onChangeText={email => this.setState({ email })}
                  />
                </Item>
                <Item style={{ marginLeft: 0 }}>
                  <Input
                    placeholder="Password"
                    secureTextEntry
                    onChangeText={password => this.setState({ password })}
                  />
                </Item>
                {loginError &&
                  <Text style={{ color: "red" }}>
                    {loginError}
                  </Text>}
                <Button
                  primary
                  block
                  //onPress={() => navigation.navigate("Profile")}
                  onPress={() => this.onLogin()}
                  style={{ marginTop: 30 }}
                >
                  <Text> LOGIN </Text>
                </Button>
                <Button
                  block
                  transparent
                  onPress={() =>
                    this.setState({
                      forgetPassword: true
                    })}
                >
                  <Text style={{ fontWeight: "700", fontSize: 12 }}>
                    FORGOT PASSWORD?
                  </Text>
                </Button>
              </Form>
            : <Form
                style={{
                  justifyContent: "center",
                  alignContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    justifyContent: "center",
                    alignContent: "center",
                    alignItems: "center",
                    fontSize: 20,
                    fontWeight: "500",
                    textAlign: "center"
                  }}
                >
                  Forgot Passowrd?
                </Text>

                <Text
                  style={{
                    justifyContent: "center",
                    alignContent: "center",
                    alignItems: "center",
                    fontSize: 15,
                    fontWeight: "300",
                    textAlign: "center"
                  }}
                >
                  It's okay, it can happen to anyone. Enter your email and we'll
                  send you reset password link.
                </Text>

                <Item style={{ marginLeft: 0 }}>
                  <Input
                    placeholder="Email ID"
                    autoCapitalize="none"
                    onChangeText={recoveryEmail =>
                      this.setState({ recoveryEmail })}
                  />
                </Item>
                <Button
                  block
                  onPress={() => this.updatePassword(recoveryEmail)}
                >
                  <Text style={{ fontWeight: "700", fontSize: 12 }}>
                    SEND RESET LINK
                  </Text>
                </Button>
              </Form>}
        </Content>
      </Container>
    );
  }
}
//export default Login;
const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps, { login })(Login);
