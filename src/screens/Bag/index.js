import React, { Component } from "react";
import {
  View,
  AsyncStorage,
  Modal,
  TouchableHighlight,
  Dimensions,
  FlatList,
  Image
} from "react-native";
import { connect } from "react-redux";
import { fetchCartItems, setCheckOutStarted } from "../../redux/actions";
import {
  Container,
  Text,
  Content,
  Button,
  Icon,
  Card,
  List,
  ListItem,
  Left,
  Right,
  Body
} from "native-base";

import styles from "./styles.js";
import ThemeHeader from "../../components/Header/index.js";
import BaseHeader from "../../components/BaseHeader/index.js";
import BagItems from "../../components/BagItems";
import MyFooter from "../../components/Footer";
import ToastModal from "../../components/ToastModal";
import commonColor from "../../theme/variables/commonColor";

var deviceHeight = Dimensions.get("window").height;
var dwidth = Dimensions.get("window").width;

const headers = new Headers();
headers.append(
  "Authorization",
  "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

class Bag extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDelete: false,
      isAddressHas: false,
      isModalVisible: false
    };
  }

  componentDidMount() {
    console.log("Bag component mounted");
    // const { userId, gId } = this.props.user;
    // if (userId) this.props.fetchCartItems(userId);
    // else if (gId) this.props.fetchCartItems(gId);
    // else this.props.fetchCartItems();
    this.fetchCart();
  }

  _showModal = () => this.setState({ isModalVisible: true });

  _hideModal = () => this.setState({ isModalVisible: false });

  fetchCart = () => {
    const { userId, gId } = this.props.user;
    if (userId) this.props.fetchCartItems(userId);
    else if (gId) this.props.fetchCartItems(gId);
    else this.props.fetchCartItems();
  };

  deleteCartItem(gId, userId, itemID) {
    let deletCart;
    if (userId) {
      deletCart = `https://www.alnasser.pk/api/viewcart/${userId}?view=cart&width=&gc=&scope=clear_cart_item&item_id=${itemID}`;
    } else if (gId) {
      deletCart = `https://www.alnasser.pk/api/viewcart/${gId}?view=cart&width=&gc=&scope=clear_cart_item&item_id=${itemID}`;
    }
    fetch(deletCart, { headers: headers })
      .then(res => res.json())
      .then(json => {
        console.log("Delete Cart Item logs \n", json.success);
        if (json.success == true) {
          console.log("Success message");
          //alert('Item Deleted Successfully.')
          this._showModal();
          this.fetchCart();
        }
      });
  }

  isCartEmpty(cartData) {
    for (var key in cartData) {
      if (cartData.hasOwnProperty(key)) return false;
    }
    return true;
  }
  initiateCheckOut() {
    const { userId } = this.props.user;
    const { userHomeAddress, userOfficeAddress, userProfile } = this.props.user;
    console.log("My Address", userProfile);
    if (!userId) {
      //console.log("User not logged In, go to login page");
      setCheckOutStarted(true);
      this.props.navigation.navigate("Profile");
    } else if (userProfile.s_firstname === null) {
      this.props.navigation.navigate("UpdateAddress", {
        isProfile: false
      });
    } else {
      this.props.navigation.navigate("SaveAddress");
      setCheckOutStarted(true);
    }
  }

  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const navigation = this.props.navigation;
    var cartData = this.props.user.cartItems;
    const { userId, gId } = this.props.user;
    const { userProfile } = this.props.user;
    //console.log("Cart Data message", cartData.message);
    console.log("Cart Data message", userProfile);

    if (
      this.isCartEmpty(cartData) ||
      cartData.message === "cart is empty" ||
      cartData.error
    ) {
      return (
        <Container>
          <BaseHeader
            PageTitle="MY BAG"
            IconLeft="arrow-back"
            //route="confirmAddress"
            navigation={navigation}
          />
          <Content
            padder
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: 10 }}
          >
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <View>
                <Image
                  style={{
                    // width: dwidth * 2 / 2.1,
                    // height: deviceHeight * 2 / 4 - 120
                  }}
                  source={require("../../../assets/empty_bag.png")}
                />
              </View>
              <View>
                <Text
                  style={{
                    fontSize: 20,
                    color: "#BDBDC8",
                    fontWeight: "500",
                    marginTop: 5
                  }}
                >
                  YOUR BAG {"\n"} IS EMPTY
                </Text>
              </View>
              <View>
                <Button
                  transparent
                  block
                  onPress={() => navigation.navigate("HomePage")}
                  style={{
                    backgroundColor: "#111",
                    width: dwidth * 2 / 2.5,
                    height: deviceHeight * 1 / 4 - 120,
                    marginTop: 10,
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      fontSize: 15,
                      fontWeight: "300",
                      color: "#fff"
                    }}
                  >
                    Continue Shopping
                  </Text>
                </Button>
              </View>
            </View>
          </Content>
        </Container>
      );
    } else {
      return (
        <Container>
          <BaseHeader
            PageTitle="MY BAG"
            IconLeft="arrow-back"
            //route="confirmAddress"
            navigation={navigation}
          />
          <Content
            padder
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: 10 }}
          >
            <View style={styles.bagTopContent}>
              <View>
                <Text style={styles.textMuted}>
                  ITEMS ({cartData.cart.amount})
                </Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.textMutedLight}>TOTAL:</Text>
                <Text style={styles.textMuted}>
                  {cartData.cart.format_display_subtotal}
                </Text>
              </View>
            </View>
            <FlatList
              removeClippedSubviews={false}
              //dataArray={dataNotification}
              keyExtractor={this._keyExtractor}
              data={cartData.cart.products}
              renderItem={({ item }) =>
                <BagItems
                  //navigation = {navigation}
                  product={item.product}
                  description={item.description}
                  soldby={item.company} //
                  //id={item.product_id} //
                  imageSource={item.image_path} //
                  imageSourceNotify={item.imageSaleNotify}
                  quantity={item.amount}
                  price={item.format_subtotal}
                  clickHandler={() =>
                    this.deleteCartItem(gId, userId, item.item_id)}
                />}
            />
            <View>
              <Text style={styles.textMuted}>PRICE DETAILS</Text>
              <Card transparent>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "space-between",
                    flexDirection: "row",
                    margin: 10
                  }}
                >
                  <Text style={styles.textMutedLight}>Bag Total</Text>
                  <Text style={styles.textMutedLight}>
                    {cartData.cart.format_display_subtotal}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "space-between",
                    flexDirection: "row",
                    margin: 10
                  }}
                >
                  <Text style={styles.textMutedLight}>Bag Discount</Text>

                  <Text style={styles.discountedText}>
                    {cartData.cart.format_discount}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "space-between",
                    flexDirection: "row",
                    margin: 10
                  }}
                >
                  <Text style={styles.textMutedLight}>Sub Total</Text>

                  <Text style={styles.textMutedLight}>
                    {cartData.cart.format_total}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "space-between",
                    flexDirection: "row",
                    margin: 10
                  }}
                >
                  <Text style={styles.textMutedLight}>Coupon Discount</Text>

                  <Text style={styles.textMutedLight}>0</Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "space-between",
                    flexDirection: "row",
                    margin: 10
                  }}
                >
                  <Text style={styles.textMutedLight}>Total Payable</Text>

                  <Text style={styles.price}>
                    {cartData.cart.format_total}
                  </Text>
                </View>
              </Card>
            </View>
            <Button
              primary
              block
              //onPress={() => this.props.navigation.navigate("SaveAddress")}
              onPress={() => this.initiateCheckOut()}
              style={{ marginTop: 10, marginBottom: 15 }}
            >
              <Text> Continue </Text>
            </Button>
            {this.state.isModalVisible
            ? <Modal
                animationType={"slide"}
                transparent={true}
                visible={this.state.isModalVisible}
                onRequestClose={() => {
                  console.log("Modal close");
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    justifyContent: "flex-end",
                    alignItems: "center",
                    alignContent: "center"
                  }}
                >
                  <View
                    style={{
                      height: 100,
                      width: dwidth,
                      backgroundColor: "#fff",
                      alignItems: "center",
                      shadowRadius: 1,
                      borderRadius: 10
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        fontWeight: "200",
                        color: "green",
                        marginTop: 20,
                        marginBottom: 10
                      }}
                    >
                      Item Deleted Successfully.!
                    </Text>

                    <TouchableHighlight
                      transparent
                      style={{
                        backgroundColor: commonColor.brandSuccess,
                        height: 40,
                        width: 40,
                        borderRadius: 100,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      onPress={() => {
                        this._hideModal();
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 15,
                          fontWeight: "700",
                          color: "#fff",
                          textAlign: "center",
                          textAlign: "center"
                        }}
                      >
                        OK
                      </Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </Modal>
            : <View />}
          </Content>
          <MyFooter navigation={navigation} selected={"bag"} />
        </Container>
      );
    }
  }
}
//export default Bag;
const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps, { fetchCartItems, setCheckOutStarted })(
  Bag
);
