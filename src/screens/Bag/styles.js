import commonColor from "../../theme/variables/commonColor";

export default {
  bagTopContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 5
  },
  textMuted: {
    fontSize: 12,
    color: "#555",
    fontWeight: "500"
  },
  textMutedLight: {
    fontSize: 12,
    color: "#555",
    fontWeight: "300"
  },
  discountedText: {
    fontSize: 13,
    fontWeight: "300",
    color: commonColor.brandPrimary,
    marginLeft: 2
  },
  price: {
    fontSize: 14,
    fontWeight: "500",
    color: "#555"
  }
};
