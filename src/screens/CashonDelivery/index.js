import React, { Component } from "react";
import { View, ActivityIndicator } from "react-native";
import {
  Card,
  Container,
  CardItem,
  Thumbnail,
  Grid,
  Col,
  Button,
  Text,
  Content,
  Footer
} from "native-base";
import IconMI from "react-native-vector-icons/MaterialIcons";
import { connect } from "react-redux";
import { placeCodOrder } from "../../redux/actions";
import styles from "./styles";
//import Container from "../../theme/components/Container";
import BaseHeader from "../../components/BaseHeader";

const headers = new Headers();
headers.append(
  "Authorization",
  "Basic YWRtaW5Ab21uaXNlbGwucGs6MTI2WThiMnU3ODE2Y3MwOGE4RzNVek8zNXM1cGxPMzg="
);
headers.append("method", "GET");

class CashonDelivery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      address: "",
      city: "",
      phoneNo: ""
    };
  }

  placeOrder(paymentId, user, firstName, lastName, address, city, phoneNo) {
    placeCodOrder(paymentId, user, firstName, lastName, address, city, phoneNo);
  }


  render() {
    const navigation = this.props.navigation;
    const { cartItems,userProfile } = this.props.user;
    const theUser = this.props.user;
    const paymentId = navigation.getParam("id", -1);
    const shippingFee = navigation.getParam("shippingFee", "12345");
    console.log(
      "Cart Items in CoD Screen \n",
      shippingFee,
      cartItems.cart.format_total
    );
    let totalAmount;
    let temShip = parseInt(shippingFee.substring(3));
    let temTotal = parseInt(cartItems.cart.total);
    totalAmount = temShip + temTotal;
    console.log("Total amount is \n", totalAmount);

    return (
      <Container>
        <BaseHeader
          PageTitle="Cash on Delivery"
          IconLeft="arrow-back"
          //route="bag"
          navigation={navigation}
        />
        <Content
          style={{ marginBottom: 50 }}
          contentContainerStyle={{ paddingBottom: 10 }}
        >
          <Card style={{ marginRight: 10, marginLeft: 10 }}>
            <CardItem>
              <Grid style={{ flexDirection: "row" }}>
                {/* <Col> */}
                <Thumbnail
                  style={{
                    resizeMode: "cover",
                    marginTop: 5,
                    height: 30,
                    width: 40
                  }}
                  square
                  source={require("../../../assets/cash.png")}
                />
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: "300",
                    color: "#333",
                    marginLeft: 10
                  }}
                  numberOfLines={2}
                  ellipsizeMode="tail"
                >
                  You can pay in cash to our courier when {"\n"}you receive the
                  goods at your doorstep.
                </Text>
                {/* </Col> */}
              </Grid>
            </CardItem>
          </Card>
        </Content>

        <Footer style={{ position: "absolute", bottom: 0, height: 150 }}>
          <Card>
            <View
              style={{
                flexDirection: "row",
                paddingLeft: 10,
                paddingRight: 10,
                marginTop: 10,
                alignItems: "flex-end",
                justifyContent: "space-between"
              }}
            >
              <Text
                style={{
                  flex: 1,
                  flexDirection: "row",
                  fontSize: 15,
                  fontWeight: "300",
                  color: "#555"
                }}
              >
                Subtotal{" "}
              </Text>

              <Text
                style={{
                  flex: 1,
                  flexDirection: "row",
                  fontSize: 15,
                  fontWeight: "300",
                  color: "#555"
                }}
              >
                {cartItems.cart.format_display_subtotal}{" "}
              </Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                paddingLeft: 10,
                paddingRight: 10,
                justifyContent: "space-between"
              }}
            >
              <Text
                style={{
                  flex: 1,
                  flexDirection: "row",
                  fontSize: 15,
                  fontWeight: "300",
                  color: "#555"
                }}
              >
                Discount Price{" "}
              </Text>

              <Text
                style={{
                  flex: 1,
                  flexDirection: "row",
                  fontSize: 15,
                  fontWeight: "300",
                  color: "#555"
                }}
              >
                {cartItems.cart.format_discount}
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                paddingLeft: 10,
                paddingRight: 10,
                justifyContent: "space-between"
              }}
            >
              <Text
                style={{
                  flex: 1,
                  flexDirection: "row",
                  fontSize: 15,
                  fontWeight: "300",
                  color: "#555"
                }}
              >
                Shipping Charges{" "}
              </Text>

              <Text
                style={{
                  flex: 1,
                  flexDirection: "row",
                  fontSize: 15,
                  fontWeight: "300",
                  color: "#555"
                }}
              >
                {shippingFee}
              </Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                paddingLeft: 10,
                paddingRight: 10,
                justifyContent: "space-between"
              }}
            >
              <Text
                style={{
                  flex: 1,
                  flexDirection: "row",
                  fontSize: 15,
                  fontWeight: "300",
                  color: "#555"
                }}
              >
                Total Amount{" "}
              </Text>

              <Text
                style={{
                  flex: 1,
                  flexDirection: "row",
                  fontSize: 15,
                  fontWeight: "300",
                  color: "#555"
                }}
              >
                {"Rs "}
                {totalAmount}
              </Text>
            </View>

            <View style={{ flex: 1, padding: 5 }}>
              <Button
                primary
                block
                style={{ marginLeft: 10, marginRight: 10 }}
                onPress={() =>
                  this.placeOrder(
                    paymentId,
                    theUser,
                    userProfile.s_firstname,
                    userProfile.s_lastname,
                    userProfile.s_address,
                    userProfile.s_city,
                    userProfile.phone,
                  )}
                style={{ marginTop: 5 }}
              >
                <Text> Place Order </Text>
                {this.props.loader.showLoader &&
                  <ActivityIndicator
                    style={{ position: "absolute", zIndex: 2, right: "2%" }}
                    size="large"
                    color="#fff"
                  />}
              </Button>
            </View>
          </Card>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  loader: state.loader
});

export default connect(mapStateToProps, { placeCodOrder })(CashonDelivery);
