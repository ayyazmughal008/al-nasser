import React from "react";
import {
  Container,
  Content,
  List,
  Body,
  Header,
  Left,
  Title,
  Text,
  Button,
  Icon,
  Right
} from "native-base";
import { View, FlatList, ActivityIndicator, ScrollView } from "react-native";
import { connect } from "react-redux";
import ReviewCartList from "../../components/ReviewCartList";
import { fetchOrdersDetail } from "../../redux/actions";

class OrderDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      myOrderDetails: null,
      productDetails: null,
      orderID: null
    };
  }

  componentWillMount() {
    let orderID = this.props.navigation.getParam("orderID", "123123");
    fetchOrdersDetail(orderID);
  }

  _keyExtractor = (item, index) => "MyKey" + index;

  render() {
    const navigation = this.props.navigation;
    const orderID = navigation.getParam("orderID", "123123");
    const randomID = navigation.getParam("randomID", "123123");
    const { orderDetail } = this.props.user;
    console.log("inside Order Details\n", orderID);

    return (
      <Container>
        <Header
          style={{
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
            borderRadius: 0,
            backgroundColor: "white",
            marginBottom: 0
          }}
        >
          <Left>
            {
              <Button
                transparent
                //onPress={() => navigation.navigate('DrawerOpen')}
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon
                  style={{ padding: 5, marginLeft: -5, color: "black" }}
                  name="ios-arrow-back"
                />
              </Button>
            }
          </Left>
          <Body>
            <Title
              style={{
                color: "black",
                fontSize: 16,
                fontWeight: "400",
                alignSelf: "flex-start"
              }}
            >
              {/* {category_name} */}
              Order # {randomID}
            </Title>
          </Body>
        </Header>
        {orderDetail !== null
          ? <ScrollView>
              <View style={{}}>
                <View
                  style={{
                    flex: 1,
                    backgroundColor: "gainsboro",
                    height: 50,
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={{
                      color: "black",
                      fontSize: 16,
                      fontWeight: "400",
                      textAlign: "left",
                      marginLeft: 20
                    }}
                  >
                    Order Placed on {orderDetail.timestamp}
                  </Text>
                </View>

                {
                  <FlatList
                    scrollEnabled={false}
                    data={orderDetail.products}
                    keyExtractor={this._keyExtractor}
                    numColumns={1}
                    renderItem={({ item }) =>
                      <ReviewCartList
                        imageSource={item.image_path}
                        product={item.product}
                        //soldby={item.company}
                        price={item.formatprice}
                        quantity={item.amount}
                        subTotal={item.subtotal}
                        //tax = {item.}
                      />}
                  />
                }

                <View style={{ flex: 1, justifyContent: "flex-start" }}>
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: "#fff",
                      height: 50,
                      justifyContent: "center"
                    }}
                  >
                    <Text
                      style={{
                        color: "black",
                        fontSize: 16,
                        fontWeight: "400",
                        textAlign: "left",
                        marginLeft: 20
                      }}
                    >
                      SHIPPING DETAILS
                    </Text>
                  </View>
                  <Text
                    style={{
                      color: "black",
                      fontSize: 16,
                      fontWeight: "300",
                      textAlign: "left",
                      marginLeft: 20
                    }}
                  >
                    {orderDetail.shipping_address.name}
                  </Text>
                  <Text
                    style={{
                      color: "black",
                      fontSize: 16,
                      fontWeight: "300",
                      textAlign: "left",
                      marginLeft: 20
                    }}
                  >
                    {orderDetail.shipping_address.address}
                  </Text>
                  <Text
                    style={{
                      color: "black",
                      fontSize: 16,
                      fontWeight: "300",
                      textAlign: "left",
                      marginLeft: 20
                    }}
                  >
                    {orderDetail.shipping_address.city}
                  </Text>
                  <Text
                    style={{
                      color: "black",
                      fontSize: 16,
                      fontWeight: "300",
                      textAlign: "left",
                      marginLeft: 20
                    }}
                  >
                    {orderDetail.shipping_address.country}
                  </Text>
                  <Text
                    style={{
                      color: "black",
                      fontSize: 16,
                      fontWeight: "300",
                      textAlign: "left",
                      marginLeft: 20
                    }}
                  >
                    {orderDetail.shipping_address.phone}
                  </Text>
                </View>

                <View
                  style={{
                    flex: 1,
                    backgroundColor: "#fff",
                    height: 50,
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={{
                      color: "black",
                      fontSize: 16,
                      fontWeight: "400",
                      textAlign: "left",
                      marginLeft: 20
                    }}
                  >
                    PAYMENT SUMMARY
                  </Text>
                </View>
                <View style={{ flex: 1, marginTop: 10, marginBottom: 10 }}>
                  <View
                    style={{
                      justifyContent: "space-between",
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        color: "black",
                        fontSize: 16,
                        fontWeight: "300",
                        textAlign: "left",
                        marginLeft: 10
                      }}
                    >
                      Payment Mode
                    </Text>
                    <Text
                      style={{
                        color: "black",
                        fontSize: 16,
                        fontWeight: "300",
                        textAlign: "right",
                        marginRight: 5
                      }}
                    >
                      {orderDetail.payment_method}
                    </Text>
                  </View>

                  <View
                    style={{
                      justifyContent: "space-between",
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        color: "black",
                        fontSize: 16,
                        fontWeight: "300",
                        textAlign: "left",
                        marginLeft: 10
                      }}
                    >
                      Sub Total{" "}
                    </Text>
                    <Text
                      style={{
                        color: "black",
                        fontSize: 16,
                        fontWeight: "300",
                        textAlign: "right",
                        marginRight: 5
                      }}
                    >
                      {orderDetail.summary.formatsubtotal}
                    </Text>
                  </View>

                  <View
                    style={{
                      justifyContent: "space-between",
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        color: "black",
                        fontSize: 16,
                        fontWeight: "300",
                        textAlign: "left",
                        marginLeft: 10
                      }}
                    >
                      Payment Status{" "}
                    </Text>
                    {orderDetail.payment_status
                      ? <Text
                          style={{
                            color: "black",
                            fontSize: 16,
                            fontWeight: "300",
                            textAlign: "right",
                            marginRight: 5
                          }}
                        >
                          {orderDetail.payment_status}
                        </Text>
                      : <Text
                          style={{
                            color: "black",
                            fontSize: 16,
                            fontWeight: "300",
                            textAlign: "right",
                            marginRight: 5
                          }}
                        >
                          Pending
                        </Text>}
                  </View>

                  <View
                    style={{
                      justifyContent: "space-between",
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        color: "black",
                        fontSize: 16,
                        fontWeight: "300",
                        textAlign: "left",
                        marginLeft: 10
                      }}
                    >
                      Shipping Charges{" "}
                    </Text>
                    <Text
                      style={{
                        color: "black",
                        fontSize: 16,
                        fontWeight: "300",
                        textAlign: "right",
                        marginRight: 5
                      }}
                    >
                      {orderDetail.summary.shipping_cost}
                    </Text>
                  </View>

                  <View
                    style={{
                      justifyContent: "space-between",
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        color: "black",
                        fontSize: 16,
                        fontWeight: "300",
                        textAlign: "left",
                        marginLeft: 10
                      }}
                    >
                      Payable Amount{" "}
                    </Text>
                    <Text
                      style={{
                        color: "black",
                        fontSize: 16,
                        fontWeight: "300",
                        textAlign: "right",
                        marginRight: 5
                      }}
                    >
                      {orderDetail.summary.formattotal}
                    </Text>
                  </View>
                </View>
              </View>
            </ScrollView>
          : <View style={{ flex: 1, justifyContent: "center" }}>
              {/* <Text style={{textAlign:'center',fontSize:20,fontWeight:'400'}}>Please Wait</Text> */}
              <ActivityIndicator size="large" color="black" />
            </View>}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps, { fetchOrdersDetail })(OrderDetail);
