import React, { Component } from "react";
import {
  Container,
  Button,
  List,
  ListItem,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Body,
  Text,
  Left,
  Right,
  Grid,
  Col
} from "native-base";

import ThemeHeader from "../../components/Header/index.js";
import MyFooter from "../../components/Footer";
import styles from "./styles";

class ConfirmAddress extends Component {
  render() {
    const navigation = this.props.navigation;
    var blockTwo = [
      {
        thumbnail: require("../../../assets/6.png"),
        date: "20 Nov 2017"
      },
      {
        thumbnail: require("../../../assets/7.png"),
        date: "12 Dec 2017"
      }
    ];
    var blockThree = [
      {
        leftText: "Order Total",
        rightText: "$ 1,598"
      },
      {
        leftText: "Delivery",
        rightText: "FREE"
      },
      {
        leftText: "Total Payable",
        rightText: "$ 1,598"
      }
    ];
    return (
      <Container>
        <ThemeHeader
          PageTitle="CONFIRM"
          IconLeft="arrow-back"
          route="saveAddress"
          navigation={navigation}
        />
        <Content
          style={{ marginBottom: 50 }}
          padder
          contentContainerStyle={{ paddingBottom: 15 }}
        >
          <Text style={styles.blockTitleText}>CONFIRM ADDRESS</Text>
          <Card style={{ paddingHorizontal: 10, marginBottom: 15 }}>
            <CardItem style={{ marginLeft: 0, paddingLeft: 0 }}>
              <Body>
                <Text style={{ fontSize: 13, color: "#777" }}>
                  #95,{"\n"}
                  Prestige Apartment,{"\n"}
                  11th Main Road, 4th cross,{"\n"}
                  BTM Layout,{"\n"}
                  Bangalore - 590007,{"\n"}
                  Karnataka
                </Text>
              </Body>
            </CardItem>
            <CardItem style={{ paddingHorizontal: 0 }}>
              <Grid>
                <Col>
                  <Button light block style={{ marginRight: 10 }}>
                    <Text style={{ fontSize: 12 }}>Edit Address</Text>
                  </Button>
                </Col>
                <Col>
                  <Button light block>
                    <Text style={{ fontSize: 12 }}>Add New Address</Text>
                  </Button>
                </Col>
              </Grid>
            </CardItem>
          </Card>
          <Text style={styles.blockTitleText}>ESTIMATED DELIVERY TIME </Text>
          <Card style={{ marginBottom: 15 }}>
            <List
              removeClippedSubviews={false}
              dataArray={blockTwo}
              renderRow={item =>
                <ListItem style={{ marginLeft: 0 }}>
                  <Thumbnail
                    square
                    size={40}
                    source={item.thumbnail}
                    style={{ marginRight: 10, resizeMode: "contain" }}
                  />
                  <Text style={styles.blockContentText}>
                    Estimated Delivery Date:{" "}
                  </Text>
                  <Text style={styles.dateText}>
                    {item.date}
                  </Text>
                </ListItem>}
            />
          </Card>
          <Text style={styles.blockTitleText}>ORDER SUMMARY</Text>
          <Card>
            <List>
              <ListItem itemHeader style={{ paddingTop: 12 }}>
                <Text style={{ fontSize: 12, color: "#555" }}>2 ITEMS</Text>
              </ListItem>
            </List>
            <List
              removeClippedSubviews={false}
              dataArray={blockThree}
              renderRow={item =>
                <ListItem style={{ marginLeft: 0 }}>
                  <Left style={{ borderBottomWidth: 0 }}>
                    <Text style={{ fontSize: 13, color: "#555" }}>
                      {item.leftText}
                    </Text>
                  </Left>
                  <Right style={{ borderBottomWidth: 0 }}>
                    <Text style={{ fontSize: 13, color: "#555" }}>
                      {item.rightText}
                    </Text>
                  </Right>
                </ListItem>}
            />
          </Card>
          <Button
            primary
            block
            onPress={() => navigation.navigate("PaymentOptions")}
            style={{ marginTop: 10, marginBottom: 15 }}
          >
            <Text>
              {" "}{"continue to payment".toUpperCase()}
            </Text>
          </Button>
        </Content>
        <MyFooter navigation={navigation} selected={"bag"} />
      </Container>
    );
  }
}
export default ConfirmAddress;
