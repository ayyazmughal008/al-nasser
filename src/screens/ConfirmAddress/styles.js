export default {
  blockTitleText: {
    fontSize: 12,
    color: "#555",
    fontWeight: "500"
  },
  blockContentText: {
    fontSize: 13,
    color: "#555",
    flex: 1
  },
  dateText: {
    fontSize: 13,
    color: "#555",
    fontWeight: "bold",
    textAlign: "right",
    marginTop: 10
  }
};
