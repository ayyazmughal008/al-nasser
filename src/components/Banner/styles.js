const React = require("react-native");
const { Dimensions } = React;

const deviceWidth = Dimensions.get("window").width;

export default {
  bannerImageContainer: {
    alignItems: "center"
  },
  bannerImage: {
    width: null,
    flex: 1,
    height: deviceWidth
  }
};
