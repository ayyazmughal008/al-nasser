import React, { Component } from "react";
import { Image, TouchableOpacity } from "react-native";
// import { observer, inject } from "mobx-react/native";
import styles from "./styles";

// @inject("view.app", "domain.user", "app", "routerActions")
// @observer
class Banner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryDrop: null
    };
  }
  categoryDropdown(id) {
    if (this.state.categoryDrop === id) {
      this.setState({ categoryDrop: null });
      return;
    }
    this.setState({ categoryDrop: id });
  }
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={styles.bannerImageContainer}
      >
        <Image style={styles.bannerImage} source={this.props.bannerSource} />
      </TouchableOpacity>
    );
  }
}

export default Banner;
