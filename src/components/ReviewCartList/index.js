import React, { Component } from "react";
import { View } from "react-native";
import {
  Card,
  CardItem,
  Thumbnail,
  Grid,
  Col,
  Button,
  Text
} from "native-base";
// import { inject } from "mobx-react/native";
import IconMI from "react-native-vector-icons/MaterialIcons";
import styles from "./styles";

// @inject("view.app", "domain.user", "app", "routerActions")
class ReviewCartList extends Component {
  render() {
    //console.log('ImageSource,',this.props.imageSource)
    const navigate = this.props.navigattion;
    return (
      <Card transparent style={{ flex: 1}}>
        <CardItem>
          <Grid>
            <Col size={1}>
              <Thumbnail
                style={{
                  resizeMode: "cover",
                  marginTop: 5,
                  height: 60,
                  width: 60
                }}
                square
                source={{ uri: this.props.imageSource }}
              />
            </Col>
            <Col size={4} style={{ marginLeft: 10 }}>
              <Text
                style={styles.itemDesc}
                numberOfLines={2}
                ellipsizeMode="tail"
              >
                {this.props.product}
              </Text>
              <Text style={styles.soldBy}>
                Sold by: {this.props.soldby}
              </Text>
              <Text style={styles.price}>
                {" "}{this.props.price}
              </Text>
              <Text style={styles.inStock}>
                Quantity {this.props.quantity}
              </Text>
              <View style={styles.totalView}>
                <Text style={styles.discountedText}>
                  Subtotal {this.props.subTotal}
                </Text>
              </View>
            </Col>
          </Grid>
        </CardItem>
      </Card>
    );
  }
}
export default ReviewCartList;
