// import React, { Component } from "react";
// import { View } from "react-native";
// import {
//   Card,
//   CardItem,
//   Thumbnail,
//   Grid,
//   Col,
//   Button,
//   Text
// } from "native-base";
// import { inject } from "mobx-react/native";
// import IconMI from "react-native-vector-icons/MaterialIcons";

// @inject("view.app", "domain.user", "app", "routerActions")
// class SearchList extends Component {
//   render() {
//     //console.log('ImageSource,',this.props.imageSource)
//     const navigate = this.props.navigattion;
//     return (
//       <Card style={{ flex: 1, marginRight: 10, marginLeft: 10 }}>
//         <CardItem>
//           <Grid>
//             <Col size={1}>
//               <Thumbnail
//                 style={{
//                   resizeMode: "cover",
//                   marginTop: 5,
//                   height: 60,
//                   width: 60
//                 }}
//                 square
//                 source={{ uri: this.props.imageSource }}
//               />
//             </Col>
//             <Col size={3} style={{ marginLeft: 10 }}>
//               <Text
//                 style={{
//                   fontSize: 14,
//                   fontWeight: "300",
//                   color: "#333"
//                 }}
//                 numberOfLines={2}
//                 ellipsizeMode="tail"
//               >
//                 {this.props.product}
//               </Text>
//               <Text
//                 style={{
//                   fontSize: 14,
//                   fontWeight: "300",
//                   color: "#333"
//                 }}
//               >
//                 {this.props.detail}
//               </Text>
//               <Text
//                 style={{
//                   fontSize: 14,
//                   fontWeight: "300",
//                   color: "#333"
//                 }}
//               >
//                 {this.props.price}
//               </Text>
//             </Col>
//             <Col size={1} style = {{marginLeft:5,marginTop:10}}>
//               <Text
//                 style={{
//                   fontSize: 13,
//                   fontWeight: "300",
//                   color: "#333"
//                 }}
//               >
//                 {this.props.category}
//               </Text>
//             </Col>
//           </Grid>
//         </CardItem>
//       </Card>
//     );
//   }
// }
// export default SearchList;
