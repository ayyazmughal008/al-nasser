import React, { Component } from "react";
import { View, Image, TouchableOpacity, Dimensions } from "react-native";
import {
  Container,
  Text,
  Button,
  List,
  ListItem,
  Content,
  Card,
  CardItem,
  Left,
  Right,
  Body,
  Grid,
  Col
} from "native-base";
// import { inject } from "mobx-react/native";
import IconMI from "react-native-vector-icons/MaterialIcons";
import styles from "./styles";

// @inject("view.app", "domain.user", "app", "routerActions")
class AddressList extends Component {
  render() {
    //console.log('ImageSource,',this.props.imageSource)
    const navigation = this.props.navigation;
    return (
      <Card transparent>
        <CardItem>
          <Grid>
            <Col size={6}>
              <Text style={styles.addres}>
                {this.props.userAddressType}
              </Text>

              <Text style={styles.normal}>
                {this.props.firstName} {this.props.lastName}
              </Text>

              <Text style={styles.normal}>
                {this.props.userAddress}
              </Text>

              <Text style={styles.normal}>
                {this.props.userCity}
              </Text>

              <Text style={styles.normal}>
                {this.props.userCountery} {this.props.userState}
              </Text>

              <Text style={styles.phonenum}>
                {this.props.userPhone}
              </Text>
            </Col>
          </Grid>
        </CardItem>
        <View
          style={{
            justifyContent: "center",
            borderTopWidth: 0.5,
            borderTopColor: "#ccc",
            marginBottom: 0,
            padding: 0,
            borderBottomWidth: 0
          }}
        >
          <Button
            transparent
            style={styles.view4}
            onPress={() =>
              navigation.navigate("UpdateAddress", {
                isProfile: this.props.isProfile
              })}
          >
            <Text
              style={{
                right: "0%",
                fontSize: 13,
                fontWeight: "500",
                color: "#777"
              }}
            >
              Change Address
            </Text>
          </Button>
        </View>
      </Card>
    );
  }
}
export default AddressList;
