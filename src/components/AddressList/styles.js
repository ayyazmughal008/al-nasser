const React = require("react-native");
const { Dimensions } = React;

const deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;
var thumbWidth = deviceWidth / 2;
var thumbHeight = deviceHeight / 2 - 5;

export default {
  contentView: {
    height: 210
  },
  contentView1: {
    marginTop: 5,
    justifyContent: "center",
    width: deviceWidth
  },
  view1: {
    flexDirection: "row",
    paddingLeft: 10,
    paddingRight: 10,
    height: 200
  },
  view3: {
    flexDirection: "row",
    paddingLeft: 10,
    paddingRight: 10
  },
  view4: {
    justifyContent: "center",
    paddingLeft: deviceWidth / 1.5
  },
  normal: {
    flexDirection: "row",
    fontSize: 15,
    fontWeight: "300",
    color: "#555"
  },
  phonenum: {
    flexDirection: "row",
    fontSize: 17,
    fontWeight: "300",
    color: "black"
  },
  addres: {
    flexDirection: "row",
    fontSize: 25,
    fontWeight: "400",
    color: "grey"
  }
};
