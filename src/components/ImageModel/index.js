import React, { Component } from "react";
import {
  View,
  Dimensions,
  Modal,
  StyleSheet,
  TouchableHighlight,
  ActivityIndicator
} from "react-native";
import { Image } from "react-native-expo-image-cache";
import { Text, Icon, Button, Thumbnail } from "native-base";

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

class ImageModel extends Component {
  state = {
    modalVisible: false,
    imageStatus: false
  };
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    return (
      <View style={styles.containerView}>
        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => null}
        >
          <View style={styles.modalConatinerView}>
            <View style={styles.modalContentView}>
              {/* close button */}
              <View style={styles.modalHeaderView}>
                <TouchableHighlight
                  transparent
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}
                >
                  <Text style={styles.modalTitleText}>Close</Text>
                </TouchableHighlight>
              </View>
              {/* render items  */}

              <Image
                style={{
                  width: deviceWidth - 2,
                  height: 450,
                  flex: 1
                }}
                resizeMode="contain"
                //source={{ uri: this.props.Size }}
                {...{
                  preview:
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg==",
                  uri: this.props.Size
                }}
                onLoadStart={() => this.setState({ imageStatus: true })}
                onLoadEnd={() => this.setState({ imageStatus: false })}
              />
              {this.state.imageStatus &&
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    position: "absolute",
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    opacity: 0.7,
                    backgroundColor: "transparent"
                  }}
                >
                  <ActivityIndicator size="large" color="#fff" />
                </View>}
            </View>
          </View>
        </Modal>

        <View style={styles.modalInitView}>
          <Button
            transparent
            onPress={() => {
              this.setModalVisible(true);
            }}
          >
            <Text style={styles.modalInitBtnText}>SIZE CHART</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10
  },
  modalConatinerView: {
    height: deviceHeight,
    width: deviceWidth,
    alignItems: "center",
    justifyContent: "flex-end"
  },
  modalContentView: {
    height: deviceHeight / 2 + 20,
    backgroundColor: "#fff",
    alignItems: "center",
    borderTopColor: "#333",
    borderTopWidth: 0.5,
    shadowColor: "#777",
    shadowOpacity: 0.2,
    shadowRadius: 1
  },
  modalHeaderView: {
    width: deviceWidth,
    borderBottomColor: "#ddd",
    borderBottomWidth: 1,
    padding: 10
  },
  modalTitleText: {
    fontSize: 15,
    fontWeight: "700",
    color: "red",
    textAlign: "center"
  },
  modalInitView: {
    height: deviceHeight / 13,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  modalInitBtnText: {
    color: "#555",
    fontWeight: "300",
    marginLeft: 8,
    fontSize: 12
  }
});
export default ImageModel;
