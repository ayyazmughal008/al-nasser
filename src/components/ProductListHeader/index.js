import React, { Component } from "react";
import {
  Header,
  Title,
  Button,
  Left,
  Right,
  Body,
  Icon,
  View,
  Thumbnail
} from "native-base";
// import { inject } from "mobx-react/native";
import IconBadge from "react-native-icon-badge";
import {
  Dimensions,
  TouchableHighlight,
  Image,
  Platform,
  SafeAreaView,
  StatusBar,
  Text
} from "react-native";

const { height, width } = Dimensions.get("window");

// @inject("routerActions")
class ProductListHeader extends Component {
  componentWillMount() {
    this.startHeaderHeight = 80;
    if (Platform.OS == "android") {
      this.startHeaderHeight = 50 + StatusBar.currentHeight;
    }
  }
  render() {
    const navigation = this.props.navigation;
    return (
      <Header
      noShadow
      style={{
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
        borderRadius: 3,
        backgroundColor: "white",
        marginBottom: 2
        }}
      >
        <Left>
          {
            <Button
              transparent
              //onPress={() => navigation.navigate('DrawerOpen')}
              onPress={() => this.props.navigation.goBack()}
            >
              <Image
                      source={require("../../../assets/arrow_black.png")}
                      resizeMode = "contain"
                      style={{height:23,width:40,marginRight:10}}
                    />
            </Button>
          }
        </Left>
        <Body>
          <Title
            style={{
              color: "grey",
              fontSize: 16,
              fontWeight: "400",
              alignSelf: "flex-start",
              marginTop: 7,
              marginRight: 35
            }}
          >
            {this.props.PageTitle}
          </Title>
        </Body>
        <Right>
          {
            <TouchableHighlight
              transparent
              //onPress={() => navigation.navigate('DrawerOpen')}
              onPress={() => this.props.navigation.navigate("SearchProducts")}
            >
              <Icon name="ios-search" transparent style={{margin:10 }} />
            </TouchableHighlight>
          }
          {this.props.IconRight &&
            <View>
              <IconBadge
                MainElement={
                  <TouchableHighlight
                    transparent
                    onPress={() => navigation.navigate("WishList")}
                  >
                    <Image
                      source={require("../../../assets/navwishlist.png")}
                      style={{ height: 33, width: 33,margin:9 }}
                    />
                  </TouchableHighlight>
                }
                BadgeElement={
                  <Text
                    style={{ color: "white", fontSize: 10, fontWeight: "300" }}
                  >
                    {this.props.itemWish}
                  </Text>
                }
                IconBadgeStyle={{
                  width: 20,
                  height: 20,
                  backgroundColor: "orange"
                }}
              />
            </View>
          }
          {this.props.IconRight2 &&
            <View>
              <IconBadge
                MainElement={
                  <TouchableHighlight
                    transparent
                    onPress={() => navigation.navigate("BAG")}
                  >
                    <Image
                      source={require("../../../assets/bag.png")}
                      style={{ height: 30, width: 30,margin:10 }}
                    />
                  </TouchableHighlight>
                }
                BadgeElement={
                  <Text
                    style={{ color: "white", fontSize: 10,fontWeight: "300" }}
                  >
                    {this.props.itemCount}
                  </Text>
                }
                IconBadgeStyle={{
                  width: 20,
                  height: 20,
                  backgroundColor: "orange"
                }}
              />
            </View>
          }
        </Right>
      </Header>
    );
  }
}

export default ProductListHeader;
