import React, { Component } from "react";
import {
  View,
  Dimensions,
  Modal,
  StyleSheet,
  TouchableHighlight
} from "react-native";
import { Text, Icon, Button, Thumbnail } from "native-base";

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

class ToastModal extends Component {
  state = {
    modalVisible: true
  };

  //   componentDidMount(){
  //     this.setModalVisible(true)
  //   }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    return (
      <View style={styles.containerView}>
        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => null}
        >
          <View style={styles.modalConatinerView}>
            <View style={styles.modalContentView}>
              <View style={styles.modalHeaderView}>
                <TouchableHighlight
                  transparent
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}
                >
                  <Text style={styles.modalTitleText}>Close</Text>
                </TouchableHighlight>
              </View>

              <Text
                style={{
                  fontSize: 16,
                  fontWeight: "200",
                  color: "green"
                }}
              >
                {this.props.Toast}
              </Text>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {
    flex: 1.5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10
  },
  modalConatinerView: {
    height: deviceHeight,
    width: deviceWidth,
    alignItems: "center",
    justifyContent: "flex-end"
  },
  modalContentView: {
    height: deviceHeight / 5 ,
    backgroundColor: "#fff",
    alignItems: "center",
    borderTopColor: "#333",
    borderTopWidth: 0.5,
    shadowColor: "#777",
    shadowOpacity: 0.2,
    shadowRadius: 1
  },
  modalHeaderView: {
    width: deviceWidth,
    borderBottomColor: "#ddd",
    borderBottomWidth: 1,
    padding: 10
  },
  modalTitleText: {
    fontSize: 15,
    fontWeight: "700",
    color: "red",
    textAlign: "center"
  },
  modalInitView: {
    height: deviceHeight / 13,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  modalInitBtnText: {
    color: "#555",
    fontWeight: "300",
    marginLeft: 8,
    fontSize: 12
  }
});
export default ToastModal;
