import React, { Component } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { Text, Card, CardItem, Button, Col, Icon, Grid } from "native-base";
// import { inject } from "mobx-react/native";
import IconMI from "react-native-vector-icons/MaterialIcons";
import styles from "./styles";

// @inject("view.app", "domain.user", "app", "routerActions")
class OrdersList extends Component {
  render() {
    //console.log('ImageSource,',this.props.imageSource)
    const navigate = this.props.navigattion;
    return (
   
        <Card transparent  >
          <TouchableOpacity
            // style={styles.contentView1}
            onPress={this.props.clickHandler}
          >
            <CardItem>
              <Grid>
                <Col size={4}>
                  <Text style={styles.orderID}>
                    Order id {this.props.orderid}
                  </Text>

                  <Text style={styles.orderID}>
                    Placed on {this.props.orderplace}
                  </Text>

                  <Text style={styles.order}>Order Status</Text>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                  <View
                    style={{
                      width: 80,
                      height: 25,
                      backgroundColor:'#fff',
                      borderRadius: 3,
                      borderWidth: 1,
                      borderColor: "grey", 
                    }}
                  >
                  <Text style={styles.status}>
                    {this.props.status}
                  </Text>
                  </View>
                  </View>
                </Col>
                <Col size={1} style={{position:'absolute',alignSelf:'center',right:'-2%'}} >
                  <Icon  name="ios-arrow-dropright"  />
                </Col>
              </Grid>
            </CardItem>
          </TouchableOpacity>
        </Card>
    );
  }
}
export default OrdersList;
