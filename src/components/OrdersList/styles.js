const React = require("react-native");
const { Dimensions } = React;

const deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;
var thumbWidth = deviceWidth / 2;
var thumbHeight = deviceHeight / 5 - 10;

export default {
  contentView: { 
    justifyContent:'center',
  },
  contentView1: {
      height: thumbHeight,
      width: deviceWidth
  },
  view3: {
    flexDirection: "row",
    paddingLeft: 5,
    paddingRight:5,
  },
  orderID: { 
     
      fontSize: 16,
      fontWeight: "300",
      color: "#555"
  },
  order: { 
   
    fontSize: 14,
    fontWeight: "300",
    color: "#555"
},
  status: {
    textAlign:'center',
    fontSize:16,
    fontWeight:"500",
    color: "#111",
  }
};
