import React, { Component } from "react";
import {
  Header,
  Title,
  Button,
  Left,
  Right,
  Body,
  Icon,
  View,
  Thumbnail
} from "native-base";
// import { inject } from "mobx-react/native";
import {
  Dimensions,
  TouchableHighlight,
  Image,
  Platform,
  SafeAreaView,
  StatusBar,
  Text
} from "react-native";

const { height, width } = Dimensions.get("window");

// @inject("routerActions")
class BaseHeader extends Component {
  componentWillMount() {
    this.startHeaderHeight = 80;
    if (Platform.OS == "android") {
      this.startHeaderHeight = 50 + StatusBar.currentHeight;
    }
  }
  render() {
    const navigation = this.props.navigation;
    return (
      <Header
      noShadow
      style={{
        marginTop: 0,
        marginLeft: 0,
        marginRight: 0,
        borderRadius: 0,
        backgroundColor: "black",
        marginBottom: 0
        }}
      >
        <Left>
          {
            <Button
              transparent
              //onPress={() => navigation.navigate('DrawerOpen')}
              onPress={() => this.props.navigation.goBack()}
            >
              {/* <Image
                      source={require("../../../assets/arrow_white.png")}
                      resizeMode = "contain"
                      style={{height:23,width:40,marginRight:10}}
                    /> */}
                    <Icon name = "ios-arrow-back" style={{color:'white'}}/>
            </Button>
          }
        </Left>
        <Body>
          <Title
            style={{
              color: "white",
              fontSize: 16,
              fontWeight: "400",
              alignSelf: "flex-start"
            }}
          >
            {this.props.PageTitle}
          </Title>
        </Body>
      </Header>
    );
  }
}

export default BaseHeader;
