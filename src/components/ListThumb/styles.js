const React = require("react-native");
const { Dimensions } = React;

const deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;
var thumbWidth = deviceWidth / 2;
var thumbHeight = deviceHeight / 2 - 5;

export default {
  contentView: {
    height: thumbHeight,
    width: thumbWidth
  },
  view1: {
    flex: 0.9,
    flexDirection: "row",
    paddingLeft: 10,
    
  },view4: {
    flex: 0.5,
    flexDirection: "row",
    paddingLeft: 10,
    
  },
  view2: {
    flex: 0.6,
    flexDirection: "row",
    justifyContent: "space-between",
    width: thumbWidth,
    marginTop: 5
  },
  brandText: {
    fontSize: 13,
    fontWeight: "300",
    color: "#555"
  },
  icon: {
    color: "#696D79",
    fontSize: 24,
    marginTop: -20,
    marginRight: 4
  },
  beforeDiscountText: {
    fontSize: 14,
    fontWeight: "500",
    color: "#555"
  },
  lineThroughText: {
    fontSize: 12,
    fontWeight: "300",
    color: "#888",
    marginTop:2,
    marginLeft: 2,
    textDecorationLine: "line-through"
  },
  afterDiscountText: {
    fontSize: 11,
    fontWeight: "300",
    color: "#7468C5",
    marginTop:2,
    marginLeft: 2
  },
  descriptionText: {
    fontSize: 11,
    fontWeight: "300",
    color: "#888",
    marginBottom: 4
  }
};
