import React, { Component } from "react";
import { Image, Dimensions, StyleSheet } from "react-native";
import { Card, CardItem } from "native-base";
// import { inject } from "mobx-react/native";

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

// @inject("routerActions")
class SaleThumb extends Component {
  render() {
    const navigation = this.props.navigation;
    return (
      <Card style={styles.card}>
        <CardItem onPress={() => navigation.navigate("ProductList")}>
          <Image
            style={styles.carditemImage}
            source={this.props.imageFlexSource}
          />
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    width: deviceWidth,
    borderColor: "transparent",
    shadowOpacity: 0
  },
  carditemImage: {
    width: null,
    flex: 1,
    resizeMode: "stretch",
    height: deviceHeight / 4 - 10
  }
});

export default SaleThumb;
