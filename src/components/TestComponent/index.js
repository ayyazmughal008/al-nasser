import React, { Component } from "react";
import { StyleSheet, Dimensions,Image } from "react-native";
import { Text, Button, Thumbnail } from "native-base";

var deviceHeight = Dimensions.get("window").height;

class TestComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryDrop: null
    };
  }
  categoryDropdown(id) {
    if (this.state.categoryDrop === id) {
      this.setState({ categoryDrop: null });
      return;
    }
    this.setState({ categoryDrop: id });
  }
  render() {
    const navigation = this.props.navigation;
    return (
      <Button
        transparent
        onPress={this.props.clickHandler}
        style={styles.roundButtonsWrap}
      >
        <Image  source={this.props.roundImageSource} 
          style = {styles.round}
         />
        <Text style={styles.text}>
          {this.props.roundImageText}
        </Text>
      </Button>
    );
  }
}

const styles = StyleSheet.create({
  roundButtonsWrap: {
    flexDirection: "column",
    height: deviceHeight / 5,
    paddingLeft: 5,
    paddingRight: 5
  },
  text: {
    fontSize: 11,
    fontWeight: "500"
  },
  round:{
    borderWidth:1,
       borderColor:'#fff',
       alignItems:'center',
       justifyContent:'center',
       width:100,
       height:100,
       backgroundColor:'#fff',
       borderRadius:60
  }
});
export default TestComponent;
