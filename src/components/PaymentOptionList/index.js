import React, { Component } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { Text, Card, CardItem, Button, Grid, Col, Thumbnail } from "native-base";
// import { inject } from "mobx-react/native";
import styles from "./styles";

// @inject("view.app", "domain.user", "app", "routerActions")
class PaymentOptionList extends Component {
    render(){
      //console.log('ImageSource,',this.props.imageSource)
      const navigate = this.props.navigattion;
      return(
          <Card style={{flex:1, marginRight:10,marginLeft:10}}>
            <CardItem>
            <Grid>
                <Col size={1}>
                <Thumbnail
                    style={{ resizeMode: "cover", marginTop: 5 , height:40, width: 40}}
                    square
                    source={{uri: this.props.imageSource}}
                />
                </Col>
                <Col size={2}>
                <Text
                    style={styles.paymentHeading}
                    numberOfLines={2}
                    ellipsizeMode="tail">
                      this.props.
                    {/* {this.props.payment} */}
                    Debit/Credit card by EasyPay
                </Text>
                <Text style={styles.discountHeading}>
                    {/* {this.props.addtionalDiscount} */}
                    Additional Discount
                </Text>
                </Col>
            </Grid>
            </CardItem>
          </Card>
      )
    }
  }
  export default PaymentOptionList;
