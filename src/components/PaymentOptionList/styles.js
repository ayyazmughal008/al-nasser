const React = require("react-native");
const { Dimensions } = React;

const deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;
var thumbWidth = deviceWidth / 2;
var thumbHeight = deviceHeight / 2 - 5;

export default {
  paymentHeading: {
    fontSize: 16,
    fontWeight: "300",
    color: "#333"
  },
  discountHeading: {
    fontSize: 13,
    fontWeight: "300",
    color: "#888"
  },
};
