const React = require("react-native");
const { Dimensions } = React;

const deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

export default {
  sliderImage: {
    height: deviceHeight * 2 / 4 - 120,
    width: deviceWidth * 2 / 2.2,
    resizeMode: "contain"
  },
  sliderImageText: {
    fontSize: 14,
    fontWeight: "700",
    marginTop: 5
  },
  smallText: {
    fontSize: 11,
    color: "#a4a5a6",
    marginTop: 2,
    marginBottom: 0
  }
};
