import React, { Component } from "react";
import { View, Image, TouchableHighlight } from "react-native";
import {
  Card,
  CardItem,
  Grid,
  Col,
  Row,
  Text,
  Thumbnail,
  Icon,
  Right
} from "native-base";
// import { inject } from "mobx-react/native";
import IconFA from "react-native-vector-icons/FontAwesome";
import styles from "./styles";

// @inject("view.app", "domain.user", "app", "routerActions")
class NotifyContent extends Component {
  render() {
    return (
      <TouchableHighlight onPress={this.props.clickHandler}>
        <Card>
          <CardItem>
            <Grid>
              <Row>
                <Col size={1}>
                  <View style={{ marginTop: 5, marginLeft: 0 }}>
                    <Thumbnail
                      style={{ width: 50, height: 50, borderRadius: 5 }}
                      source={require("../../../assets/ic_launcher.png")}
                    />
                  </View>
                </Col>
                <Col size={5} style={{ marginLeft: 10 }}>
                  <View>
                    <View style={{ flexDirection: "row" }}>
                      <Text style={styles.tagText} numberOfLines={2}>
                        {this.props.tag}
                      </Text>
                    </View>
                    <Text
                      style={styles.descriptionText}
                      numberOfLines={2}
                      ellipsizeMode="tail"
                    >
                      {this.props.description}
                    </Text>
                    <Text
                      style={styles.timeText}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {this.props.time}
                    </Text>
                  </View>
                </Col>
                <Col size={1} style={{marginLeft:5}}>
                <Icon name="ios-cart" />
                </Col>
              </Row>
              <Row>
                <Col>
                  {this.props.imageSaleThumb &&
                    <Image
                      style={styles.notifyImage}
                      source={this.props.imageSaleThumb}
                    />}
                </Col>
              </Row>
            </Grid>
          </CardItem>
        </Card>
      </TouchableHighlight>
    );
  }
}

export default NotifyContent;
