const React = require("react-native");
const { Dimensions } = React;

const deviceWidth = Dimensions.get("window").width;
import commonColor from "../../theme/variables/commonColor";

export default {
  bellIcon: {
    fontSize: 28,
    color: commonColor.brandPrimary
  },
  tagText: {
    fontSize: 15,
    fontWeight: "700",
    color: "#333",
    marginRight: 10,
    
  },
  descriptionText: {
    fontSize: 14,
    fontWeight: "400",
    color: "#333",
    lineHeight: 16,
    marginRight: 10
  },
  timeText: {
    fontSize: 13,
    fontWeight: "300",
    color: "#888",
    marginBottom: 4,
    marginRight: 10
  },
  notifyImage: {
    width: null,
    height: deviceWidth / 2,
    margin: 6,
    resizeMode: "contain",
    borderColor: "#ccc",
    justifyContent: "center"
  }
};
