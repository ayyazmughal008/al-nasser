import React, {Component} from "react";
import {View, Image, Dimensions, StyleSheet} from "react-native";
import {Text, Card, CardItem} from "native-base";
var deviceWidth = Dimensions.get("window").width;
var thumbWidth = deviceWidth / 3 - 20;

class SaleBrandThumb extends Component {
  render() {
    return (
      <View style={{height: 95, width: deviceWidth / 3 - 10}}>
        <Card style={{borderColor: "transparent"}}>
          <CardItem style={{flex: 1, flexDirection: "column"}}>
            <Image style={styles.image} source={this.props.imageSource} />
            <Text style={styles.text}>
              {this.props.saleData}
            </Text>
          </CardItem>
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: thumbWidth,
    height: 60,
    borderWidth: 1,
    flex: 1,
    resizeMode: "contain",
    alignSelf: "center",
    borderColor: "#111"
  },
  text: {
    textAlign: "center",
    fontSize: 14,
    fontWeight: "500",
    backgroundColor: "#111",
    color: "#fff",
    paddingTop: 2,
    paddingBottom: 2,
    width: thumbWidth + 10
  }
});

export default SaleBrandThumb;
