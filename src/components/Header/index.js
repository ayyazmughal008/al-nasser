import React, { Component } from "react";
import {
  Header,
  Title,
  Button,
  Left,
  Right,
  Body,
  Icon,
  View,
  Thumbnail
} from "native-base";
// import { inject } from "mobx-react/native";
import IconBadge from "react-native-icon-badge";
import {
  Dimensions,
  TouchableHighlight,
  Image,
  Platform,
  SafeAreaView,
  StatusBar,
  Text,
  TouchableOpacity
} from "react-native";

const { height, width } = Dimensions.get("window");

// @inject("routerActions")
class ThemeHeader extends Component {
  componentWillMount() {
    this.startHeaderHeight = 80;
    if (Platform.OS == "android") {
      this.startHeaderHeight = 50 + StatusBar.currentHeight;
    }
  }
  render() {
    const navigation = this.props.navigation;
    return (
      <Header
        noShadow
        style={{
          marginTop: 0,
          marginLeft: 0,
          marginRight: 0,
          borderRadius: 3,
          backgroundColor: "white",
          marginBottom: 2
        }}
      >
        <Left>
          {
            <TouchableOpacity
              transparent
              onPress={() => navigation.navigate(this.props.drawerOpen)}
            >
              <Icon
                //onPress={() => navigation.goBack()}
                //onPress= {() => navigation.openDrawer()}
                style={{ padding: 5, marginLeft: -5 }}
                name={this.props.IconLeft}
              />
            </TouchableOpacity>
          }
        </Left>
        <Body>
          <TouchableHighlight
            transparent
            style={{ alignContent: "center", alignItems: "center" }}
            onPress={() => navigation.navigate(this.props.textInput)}
          >
            <Title
              style={{
                color: "grey",
                fontSize: 16,
                fontWeight: "400",
                alignSelf: "flex-start",
                marginTop: 7,
                marginRight: 35
              }}
            >
              {this.props.PageTitle}
            </Title>
          </TouchableHighlight>
        </Body>
        <Right>
          {this.props.IconRight &&
            <View>
              <IconBadge
                MainElement={
                  <TouchableHighlight
                    transparent
                    onPress={() => navigation.navigate("WishList")}
                  >
                    <Image
                      source={require("../../../assets/navwishlist.png")}
                      style={{ height: 33, width: 33, margin: 9 }}
                    />
                    {/* <Icon name="ios-heart-empty"/> */}
                  </TouchableHighlight>
                }
                BadgeElement={
                  <Text
                    style={{ color: "white", fontSize: 10, fontWeight: "300" }}
                  >
                    {this.props.itemWish}
                  </Text>
                }
                IconBadgeStyle={{
                  width: 20,
                  height: 20,
                  backgroundColor: "orange"
                }}
              />
            </View>}
          {this.props.IconRight2 &&
            <View>
              <IconBadge
                MainElement={
                  <TouchableHighlight
                    transparent
                    onPress={() => navigation.navigate("BAG")}
                  >
                    <Image
                      source={require("../../../assets/bag.png")}
                      style={{ height: 30, width: 30, margin: 10 }}
                    />
                  </TouchableHighlight>
                }
                BadgeElement={
                  <Text
                    style={{ color: "white", fontSize: 10, fontWeight: "300" }}
                  >
                    {this.props.itemCount}
                  </Text>
                }
                IconBadgeStyle={{
                  width: 20,
                  height: 20,
                  backgroundColor: "orange"
                }}
              />
            </View>}
        </Right>
      </Header>
    );
  }
}

export default ThemeHeader;
