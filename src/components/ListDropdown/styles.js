export default {
  listHeading: {
    fontSize: 16,
    color: "#444",
    fontWeight: "600"
  },
  listIconHeading: {
    fontWeight: "100",
    paddingTop: 2,
    fontSize: 18,
    color: "#444"
  },
  listDropItems: {
    marginLeft: 10
  },
  listDropText: {
    fontSize: 14,
    fontWeight: "300",
    paddingLeft: 10
  },
  listIcon: {
    fontWeight: "100",
    paddingTop: 2,
    fontSize: 14,
    color: "#777"
  }
};
