import React, { Component } from "react";
import {connect} from 'react-redux'
import NavigationService from '../../NavigationService'
import {updateBrowsePosition} from "../../redux/actions"
import { Text, View, FlatList } from "react-native";
import { List, ListItem, Icon, Body, Right } from "native-base";
import styles from "./styles";
class ListDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryDrop: null,
      categories: null
    };
  }
  componentWillMount(){
    this.setState({categories: this.props.data})
  }
  categoryDropdown(id) {
    if (this.state.categoryDrop === id) {
      this.setState({ categoryDrop: null });
      return;
    }
    this.setState({ categoryDrop: id , categories: this.props.data});
  }

  handleClick = (id,topId, category) => {
    updateBrowsePosition({
        categoryId: id,
        category:category,
        topCategoryId: topId,
        isCategoryListingPage: false,
        currentPage:0
    })
    NavigationService.navigate('ProductList')
}
renderItemSub = (item) => {
  const data = item.subcategories
  //console.log('clicked cate:',data)
  if(data === undefined){
    this.handleClick(item.category_id,this.props.browsePosition.topCategoryId,item)
  }
  return(
    <View>
      <FlatList
            data={data}
            renderItem={this.renderItemSub2}
            keyExtractor={item => item.category_id}
        >
        </FlatList>
    </View>
  )
}
renderItemSub2 = ({item}) => {
  return(
    <ListItem
            icon
            style={styles.listDropItems}
            onPress={() => this.handleClick(item.category_id,this.props.browsePosition.topCategoryId,item)}
            >
            <Body>
              <Text style={styles.listDropText}>
                {item.category}
              </Text>
            </Body>
            <Right>
                <Icon style={styles.listIcon} name="ios-arrow-forward" />
            </Right>
    </ListItem>
  )
}

renderItem = ({ item}) => {
      return (
        <View>
        <ListItem
          onPress={()=> this.categoryDropdown(item.category_id)}
          style={{ marginLeft: 0, paddingLeft: 10 }}
        >
          <Body>
            <Text style={styles.listHeading}>
              {item.category}
            </Text>
          </Body>
          <Right>
            <Icon
              style={styles.listIconHeading}
              name={
                item.category_id === this.state.categoryDrop
                  ? "ios-arrow-up"
                  : "ios-arrow-down"
              }
            />
          </Right>
        </ListItem>
        {this.state.categoryDrop === item.category_id && this.renderItemSub(item)}
        </View>
      );
  }

  render() {

    return (
      <FlatList
        data={this.state.categories}
        renderItem={this.renderItem}
        keyExtractor={item => item.category_id}
        extraData={this.state.categoryDrop}
      />
    );
  }
}

const mapStateToProps = state => ({
  homepage: state.homepage,
  browsePosition: state.browsePosition
})
export default connect(mapStateToProps)(ListDropdown)




// import React, { Component } from "react";
// import { Text, View } from "react-native";
// import { List, ListItem, Icon, Body, Right } from "native-base";
// import { observer, inject } from "mobx-react/native";
// import styles from "./styles";

// @inject("view.app", "domain.user", "app", "routerActions")
// @observer
// class ListDropdown extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       categoryDrop: null
//     };
//   }
//   categoryDropdown(id) {
//     if (this.state.categoryDrop === id) {
//       this.setState({ categoryDrop: null });
//       return;
//     }
//     this.setState({ categoryDrop: id });
//   }
//   render() {

//     return (
//       <List
//         removeClippedSubviews={false}
//         bounces={false}
//         dataArray={list}
//         renderRow={item =>
//           <View>
//             <ListItem
//               onPress={() => this.categoryDropdown(item.id)}
//               style={{ marginLeft: 0, paddingLeft: 10 }}
//             >
//               <Body>
//                 <Text style={styles.listHeading}>
//                   {item.value}
//                 </Text>
//               </Body>
//               <Right>
//                 <Icon
//                   style={styles.listIconHeading}
//                   name={
//                     item.id === this.state.categoryDrop
//                       ? "ios-arrow-up"
//                       : "ios-arrow-down"
//                   }
//                 />
//               </Right>
//             </ListItem>
//             {this.state.categoryDrop === item.id &&
//               <List
//                 removeClippedSubviews={false}
//                 bounces={false}
//                 dataArray={item.sublist}
//                 renderRow={sublistItem =>
//                   <ListItem
//                     icon
//                     style={styles.listDropItems}
//                     onPress={() =>
//                       this.props.navigation.navigate("ProductList")}
//                   >
//                     <Body>
//                       <Text style={styles.listDropText}>
//                         {sublistItem}
//                       </Text>
//                     </Body>
//                     <Right>
//                       <Icon style={styles.listIcon} name="ios-arrow-forward" />
//                     </Right>
//                   </ListItem>}
//               />}
//           </View>}
//       />
//     );
//   }
// }

// export default ListDropdown;
