const React = require("react-native");
const { Dimensions } = React;

const deviceWidth = Dimensions.get("window").width;
import commonColor from "../../theme/variables/commonColor";

export default {
  footer: {
    marginHorizontal: -10,
    position: "absolute",
    bottom: 0
  },
  footerTab: {
    width: deviceWidth,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  footerBtn: {
    paddingLeft: 0,
    paddingRight: 0,
    margin: 0
  },
  footerIconActive: {
    fontSize: 17,
    color: commonColor.brandPrimary,
    lineHeight: 20,
    fontWeight: "500"
  },
  footerIconInactive: {
    fontSize: 17,
    color: "#777",
    lineHeight: 20,
    fontWeight: "500"
  },
  footerIconTabTextActive: {
    fontSize: 11,
    color: commonColor.brandPrimary,
    lineHeight: 15,
    fontWeight: "100",
    textAlign: "center"
  },
  footerIconTextInactive: {
    fontSize: 11,
    color: "#777",
    lineHeight: 15,
    fontWeight: "100",
    textAlign: "center"
  }
};
