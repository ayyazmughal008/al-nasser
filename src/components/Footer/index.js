import React, { Component } from "react";
import { Text, Footer, FooterTab, Button } from "native-base";
// import { inject } from "mobx-react/native";
import styles from "./style.js";
import IconFA from "react-native-vector-icons/FontAwesome";
import IconSLI from "react-native-vector-icons/SimpleLineIcons";

// @inject("routerActions", "view.app")
class ThemeFooter extends Component {
  render() {
    const tabs = this.props;
    const navigation = this.props.navigation;
    return (
      <Footer style={styles.footer}>
        <FooterTab style={styles.footerTab}>
          <Button
            active={tabs.selected === "home" ? true : false}
            style={styles.footerBtn}
            onPress={() => navigation.navigate("HomePage")}
          >
            <IconSLI
              style={
                tabs.selected === "home"
                  ? styles.footerIconActive
                  : styles.footerIconInactive
              }
              name="home"
            />
            <Text
              style={
                tabs.selected === "home"
                  ? styles.footerIconTabTextActive
                  : styles.footerIconTextInactive
              }
            >
              Home
            </Text>
          </Button>
          <Button
            active={tabs.selected === "grid" ? true : false}
            style={styles.footerBtn}
            onPress={() => navigation.navigate("CategoryPage")}
          >
            <IconSLI
              style={
                tabs.selected === "grid"
                  ? styles.footerIconActive
                  : styles.footerIconInactive
              }
              name="grid"
            />
            <Text
              style={
                tabs.selected === "grid"
                  ? styles.footerIconTabTextActive
                  : styles.footerIconTextInactive
              }
            >
              Categories
            </Text>
          </Button>
          <Button
            active={tabs.selected === "loginHome" ? true : false}
            style={styles.footerBtn}
            onPress={() => navigation.navigate("Profile")}
          >
            <IconFA
              style={
                tabs.selected === "loginHome"
                  ? styles.footerIconActive
                  : styles.footerIconInactive
              }
              name="user-o"
            />
            <Text
              style={
                tabs.selected === "loginHome"
                  ? styles.footerIconTabTextActive
                  : styles.footerIconTextInactive
              }
            >
              Profile
            </Text>
          </Button>
          <Button
            active={tabs.selected === "notifications" ? true : false}
            style={styles.footerBtn}
            onPress={() => navigation.navigate("NOTIFICATIONS")}
          >
            <IconFA
              style={
                tabs.selected === "notifications"
                  ? styles.footerIconActive
                  : styles.footerIconInactive
              }
              name="bell-o"
            />
            <Text
              style={
                tabs.selected === "notifications"
                  ? styles.footerIconTabTextActive
                  : styles.footerIconTextInactive
              }
            >
              Notifications
            </Text>
          </Button>
          <Button
            active={tabs.selected === "bag" ? true : false}
            style={styles.footerBtn}
            onPress={() => navigation.navigate("BAG")}
          >
            <IconSLI
              style={
                tabs.selected === "bag"
                  ? styles.footerIconActive
                  : styles.footerIconInactive
              }
              name="handbag"
            />
            <Text
              style={
                tabs.selected === "bag"
                  ? styles.footerIconTabTextActive
                  : styles.footerIconTextInactive
              }
            >
              Bag
            </Text>
          </Button>
        </FooterTab>
      </Footer>
    );
  }
}
export default ThemeFooter;
