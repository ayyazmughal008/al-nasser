import React, { Component } from "react";
import { StyleSheet } from "react-native";
import { View, Text } from "native-base";

class SaleTitle extends Component {
  render() {
    return (
      <View style={styles.contentView}>
        <View style={{ flexDirection: "row", alignSelf: "center" }}>
          <View style={styles.sideView} />
          <Text style={styles.titleText}>
            {this.props.saleTitle}
          </Text>
          <View style={styles.sideView} />
        </View>
        <Text style={styles.subtitleText}>
          {this.props.saleSubTitle}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentView: {
    flex: 1,
    margin: 18,
    marginBottom: 0
  },
  sideView: {
    height: 2,
    width: 30,
    backgroundColor: "#111",
    marginTop: 11
  },
  titleText: {
    textAlign: "center",
    fontSize: 20,
    fontWeight: "700",
    fontStyle: "italic",
    marginLeft: 10,
    marginRight: 10
  },
  subtitleText: {
    textAlign: "center",
    fontSize: 15,
    fontWeight: "500",
    marginBottom: 8
  }
});
export default SaleTitle;
