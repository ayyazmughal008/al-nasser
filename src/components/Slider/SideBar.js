import React, {Component} from "react";
import { AppRegistry, Image,ImageBackground, StatusBar } from "react-native";
import { Container, Content, Text, List, ListItem } from "native-base";
const routes = ["Home", "Chat", "Profile"];
export default class SideBar extends Component {
  render() {
    return (
      <Container>
        <Content>
          <ImageBackground
            source={{
              uri: "https://upload.wikimedia.org/wikipedia/commons/2/22/North_Star_-_invitation_background.png"
            }}
            style={{
              height: 120,
              alignSelf: "stretch",
              justifyContent: "center",
              alignItems: "center"
            }}>
            <Image
              square
              style={{ height: 80, width: 70 }}
              source={{
                uri: "https://upload.wikimedia.org/wikipedia/commons/2/22/North_Star_-_invitation_background.png"
              }}
            />
          </ImageBackground>
          <List
            dataArray={routes}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data)}>
                  <Text>{data}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}