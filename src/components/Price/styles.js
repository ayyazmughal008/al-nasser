const React = require("react-native");
const { Dimensions } = React;

const deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;
var thumbWidth = deviceWidth / 2;
var thumbHeight = deviceHeight / 2 - 5;

export default {
  contentView: { 
    flex:1,
    justifyContent:'center',
    height: 150, 
  },
  contentView1: {
      flex:1,
      justifyContent:'center',
      height: thumbHeight,
      width: deviceWidth
  },
  view1: {
    flex: 1,
    flexDirection: "row",
    paddingLeft: 10,
    paddingRight:10,
    width: deviceWidth
  },
  view3: {
    flexDirection: "row",
    paddingLeft: 10,
    paddingRight:10,
    justifyContent: "space-between"
  },
  normal: {
      flex: 1,
      flexDirection: "row",
      fontSize: 15,
      fontWeight: "300",
      color: "#555"
  },
  phonenum: {
    flex: 1,
    flexDirection: "row",
    fontSize: 17,
    fontWeight: "300",
    color: "black"
  },
  addres: {
    flex:1,
    flexDirection:'row',
    fontSize:25,
    fontWeight:"400",
    color: "grey",
  }


};