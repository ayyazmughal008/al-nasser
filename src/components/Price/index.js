import React, { Component } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { Text, Card, CardItem, Button } from "native-base";
// import { inject } from "mobx-react/native";
import styles from "./styles";

// @inject("view.app", "domain.user", "app", "routerActions")
class Price extends Component {
    render(){
      //console.log('ImageSource,',this.props.imageSource)
      const navigate = this.props.navigattion;
      return(
        <View style={styles.contentView}>
          <Card transparent style={{shadowOpacity:0,margin:5}}> 
              <View style={styles.view3}>
                  <Text style={styles.addres}>
                      PAYMENT SUMMARY
                  </Text>
              </View>
  
              <View style={styles.view3}>
                  <Text style={styles.normal}> Subtotal</Text>
                  <Text style={styles.normal}> {this.props.subTotal}</Text>
              </View>
  
              <View style={styles.view3}>
                  <Text style={styles.normal}>Tax</Text>
                  <Text style={styles.normal}>{this.props.tax}</Text>
              </View>
  
              <View style={styles.view3}>
                  <Text style={styles.normal}>Shipping Cost</Text>
                  <Text style={styles.normal}>{this.props.shipping} </Text>
              </View>
  
              <View style={styles.view3}>
                  <Text style={styles.normal}>Total </Text>
                  <Text style={styles.normal}>{this.props.total}</Text>
              </View>

              {/* <View style={styles.view3}>
                  <Text style={styles.phonenum}>
                  {this.props.userPhone}
                  </Text>
              </View> */}
          </Card>
  
        </View>
      )
    }
  }
  export default Price;
