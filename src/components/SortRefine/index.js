import React, { Component } from "react";
import { View, Dimensions } from "react-native";
import Sort from "./Sort";
import Refine from "./Refine";

var deviceWidth = Dimensions.get("window").width;

class SortRefine extends Component {
  state = {
    modalVisible: false
  };
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {
    return (
      <View
        style={{
          alignItems: "center",
          height: 40,
          width: deviceWidth,
          backgroundColor: "#fff",
          flexDirection: "row",
          borderBottomColor: "#ddd",
          borderBottomWidth: 1
        }}
      >
        <Sort
          sortingVeriable={this.props.sortingVeriable}
          sortingFunction={this.props.sortingFunction}
        />
        <View
          style={{
            height: 30,
            width: 1,
            backgroundColor: "#ddd"
          }}
        />
        <Refine
          filterVariable={this.props.filterVariable}
          sortingFunction={this.props.sortingFunction}
          hashesVariable={this.props.hashesVariable}
          DATAToBeSave={this.props.DATAToBeSave}
          clearSavedData={this.props.clearSavedData}
          RefineType={this.props.RefineType}
          RefineData={this.props.RefineData}
          // variantVariable = {this.props.variantVariable}
        />
      </View>
    );
  }
}
export default SortRefine;
