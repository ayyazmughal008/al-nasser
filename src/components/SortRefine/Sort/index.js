import React, { Component } from "react";
import {
  View,
  Dimensions,
  Modal,
  StyleSheet,
  TouchableHighlight
} from "react-native";
import { Text, Icon, Button } from "native-base";

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

class Sort extends Component {
  state = {
    modalVisible: false
  };
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    return (
      <View style={styles.containerView}>
        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => null}
        >
          <View style={styles.modalConatinerView}>
            <View style={styles.modalContentView}>
              {/* close button */}
              <View style={styles.modalHeaderView}>
                <TouchableHighlight
                  transparent
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}
                >
                  <Text style={styles.modalTitleText}>Close</Text>
                </TouchableHighlight>
              </View>
              {/* render items  */}
              {this.props.sortingVeriable.map((item, index) =>
                <View key={"sort" + index}>
                  <Button
                    transparent
                    block
                    onPress={() => {
                      this.setModalVisible(!this.state.modalVisible);
                      this.props.sortingFunction(item.sort_by, item.sort_order);
                    }}
                  >
                    <Text>
                      {item.name}
                    </Text>
                  </Button>
                </View>
              )}
            </View>
          </View>
        </Modal>

        <View style={styles.modalInitView}>
          <Button
            transparent
            onPress={() => {
              this.setModalVisible(true);
            }}
          >
            <Icon style={{ color: "#555" }} name="ios-arrow-down" />
            <Text style={styles.modalInitBtnText}>SORT</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {
    height: "100%",
    flexDirection: "row",
    width: deviceWidth / 2,
    justifyContent: "center",
    alignItems: "center"
  },
  modalConatinerView: {
    height: deviceHeight,
    width: deviceWidth,
    alignItems: "center",
    justifyContent: "flex-end",
    marginTop: 40
  },
  modalContentView: {
    height: deviceHeight / 2 + 20,
    backgroundColor: "#fff",
    alignItems: "center",
    borderTopColor: "#333",
    borderTopWidth: 0.5,
    shadowColor: "#777",
    shadowOpacity: 0.2,
    shadowRadius: 1
  },
  modalHeaderView: {
    width: deviceWidth,
    borderBottomColor: "#ddd",
    borderBottomWidth: 1,
    padding: 10
  },
  modalTitleText: {
    fontSize: 15,
    fontWeight: "700",
    color: "red",
    textAlign: "center"
  },
  modalInitView: {
    height:'100%',
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  modalInitBtnText: {
    color: "#555",
    fontWeight: "300",
    marginLeft: 8,
    fontSize: 12
  }
});
export default Sort;
