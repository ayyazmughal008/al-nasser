import React, { Component } from "react";
import { View, Dimensions, Platform, StyleSheet, Switch } from "react-native";
import {
  Text,
  Icon,
  Item,
  Input,
  Container,
  Content,
  List,
  ListItem,
  Left,
  Right,
  CheckBox
} from "native-base";
import { Col, Grid } from "react-native-easy-grid";

var deviceWidth = Dimensions.get("window").width;
const myId = 10;

class Discount extends Component {
  constructor() {
    super();
    this.state = {
      switch1Array: [],
      ready: false
    };
  }

  componentWillMount() {
    let myTempArray = [];
    const { filterVariable, RefineType, RefineData } = this.props;
    // const ThisOne = RefineType === "Brand" ? true : false;
   // alert(RefineData + " " + RefineType);
    const RefineDataArray = RefineData.split("-");
    let i = 1,
      Leng = RefineDataArray.length;
    let RefineDataArraywithoutFirstMem = [];
    if (Leng > 1)
      for (i; i < Leng; i++) {
        RefineDataArraywithoutFirstMem.push(RefineDataArray[i]);
      }
    filterVariable.map((item, index) => {
      if (item.filter_id == myId)
        item.variants.map((myItem, myIndex) => {
          if (
            RefineDataArraywithoutFirstMem.indexOf("" + myItem.variant_id) !=
              -1 &&
            RefineType == "Discount"
          ) {
            myTempArray.push({ itemId: myItem.variant_id, switchState: true });
          } else {
            myTempArray.push({ itemId: myItem.variant_id, switchState: false });
          }
        });
    });
    this.setState({ switch1Array: myTempArray, ready: true });
  }

  toggleSwitch1 = value => {
    this.setState({ switch1Value: value });
    console.log("Switch 1 is: " + value);
  };

  myApplyFunction = () => {
    let stringTobeSend = "";
    this.state.switch1Array.map((item, index) => {
    if (item.switchState) stringTobeSend = stringTobeSend + "-" + item.itemId;
    });
    if (stringTobeSend) {
    stringTobeSend = myId+ stringTobeSend;
    }
    this.props.DATAToBeSave(stringTobeSend, "Discount");
    return stringTobeSend;
    };

  render() {
    var dataSaleThumb = [
      {
        id: 1,
        categories: "Arrow"
      },
      {
        id: 2,
        categories: "Arrow New York"
      },
      {
        id: 3,
        categories: "Blackberrys"
      },
      {
        id: 4,
        categories: "CODE by Lifestyle"
      },
      {
        id: 5,
        categories: "Cotton Country P..."
      },
      {
        id: 6,
        categories: "Four One Oh"
      },
      {
        id: 7,
        categories: "Hancock"
      },
      {
        id: 8,
        categories: "INVICTUS"
      },
      {
        id: 9,
        categories: "John Miller"
      },
      {
        id: 10,
        categories: "John Players"
      },
      {
        id: 11,
        categories: "Louis Philips"
      },
      {
        id: 12,
        categories: "Marks & Spencer"
      },
      {
        id: 13,
        categories: "Park Avenue"
      },
      {
        id: 14,
        categories: "Peter England"
      },
      {
        id: 15,
        categories: "Peter England Eli..."
      },
      {
        id: 16,
        categories: "Raymond"
      },
      {
        id: 17,
        categories: "Urban Nomad"
      },
      {
        id: 18,
        categories: "Van Heusen"
      },
      {
        id: 19,
        categories: "Wills Lifestyle"
      }
    ];

    //const { check } = this.state;
    var hash = ["13"];
    return (
      <Container>
        {/* <Item borderType="regular" style={styles.searchItem}>
          <Icon name="search" style={styles.searchItemIcon} />
          <Input
            placeholderTextColor="#777"
            placeholder="Search by brand name"
            style={{height: Platform.OS === "ios" ? 30 : undefined}}
          />
        </Item> */}
        <Content
          padder
          bounces={true}
          scrollEnabled={true}
          showsVerticalScrollIndicator={false}
          style={{ padding: 0, marginBottom: 0,backgroundColor:"#C0C0C0" }}
        >
          <Grid>
            <Col >
              <View>
                {/* <List
                  contentContainerStyle={{}}
                  dataArray={dataSaleThumb}
                  renderRow={item =>
                    <ListItem button style={{paddingTop: 5, paddingBottom: 5}}>
                      <Left>
                        <Icon
                          active
                          name="checkmark"
                          style={styles.listitemLeftIcon}
                        />
                        <Text style={styles.listitemText}>
                          {item.categories}
                        </Text>
                      </Left>
                      <Right>
                        <Text style={styles.listitemText}>20</Text>
                      </Right>
                    </ListItem>}
                /> */}
                {this.state.ready &&
                  this.props.filterVariable.map((item, index) => {
                    if (item.filter_id == myId)
                      return item.variants.map((myItem, myIndex) =>
                        <View
                          style={{
                            paddingTop: 5,
                            paddingBottom: 5,
                            flexDirection: "row"
                          }}
                          key={"sort" + myIndex}
                        >
                          {/* <Button
                        transparent
                        block
                        onPress={() => {
                          this.setState({component: item.description});
                          // this.props.sortingFunction(
                          //   item.sort_by,
                          //   item.sort_order
                          // );
                        }}
                      > */}
                          {/* <CheckBox
                          value={check}
                          onValueChange={() =>
                            this.setState({ check: false })}
                          checked={hash.push(myItem.variant_id)}
                        /> */}
                          <Switch
                            onValueChange={value => {
                              let { switch1Array } = this.state;
                              switch1Array[myIndex].switchState = value;
                              this.setState({ switch1Array });
                            }}
                            value={this.state.switch1Array[myIndex].switchState}
                          />
                          <Text style={styles.listitemText}>
                            {myItem.variant}
                          </Text>
                          {/* </Button> */}
                        </View>
                      );
                  })}
              </View>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  searchItem: {
    borderColor: "transparent",
    borderRadius: 5,
    backgroundColor: "#eee",
    width: deviceWidth * (3 / 4) - 50,
    height: 30,
    margin: 10,
    marginLeft: 8
  },
  searchItemIcon: {
    fontSize: 20,
    color: "#777",
    marginLeft: 10,
    marginTop: 2
  },
  listitemLeftIcon: {
    color: "#777",
    fontSize: Platform.OS === "android" ? 16 : 24,
    fontWeight: "300"
  },
  listitemText: {
    fontSize: 15,
    fontWeight: "300",
    color: "#fff",
    marginLeft: 15,
    marginTop:5
  }
});

export default Discount;
