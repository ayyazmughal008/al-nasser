import React, {Component} from "react";
import {View, StyleSheet, Platform} from "react-native";
import {
  Text,
  Icon,
  Container,
  Content,
  List,
  ListItem,
  Left,
  Right
} from "native-base";
import {Col, Grid} from "react-native-easy-grid";

class Sizes extends Component {
  render() {
    var dataSaleThumb = [
      {
        id: 1,
        categories: "XL",
        qty: 3505
      },
      {
        id: 2,
        categories: "L",
        qty: 3471
      },
      {
        id: 3,
        categories: "M",
        qty: 3276
      },
      {
        id: 4,
        categories: "S",
        qty: 3212
      },
      {
        id: 5,
        categories: "XXL",
        qty: 2343
      },
      {
        id: 6,
        categories: "XS",
        qty: 1775
      },
      {
        id: 7,
        categories: "3XL",
        qty: 137
      },
      {
        id: 8,
        categories: "3XS",
        qty: 60
      },
      {
        id: 9,
        categories: "4XL",
        qty: 26
      },
      {
        id: 10,
        categories: "5XL",
        qty: 13
      },
      {
        id: 11,
        categories: "6XL",
        qty: 12
      },
      {
        id: 12,
        categories: "7XL",
        qty: 8
      },
      {
        id: 13,
        categories: "XXL",
        qty: 3
      },
      {
        id: 14,
        categories: "8XL",
        qty: 2
      }
    ];

    return (
      <Container>
        <Content
          padder
          bounces={true}
          scrollEnabled={true}
          showsVerticalScrollIndicator={false}
          style={{padding: 0, marginBottom: 0}}
        >
          <Grid>
            <Col style={{backgroundColor: "#FFFFFF"}}>
              <View>
                <List
                  contentContainerStyle={{}}
                  dataArray={dataSaleThumb}
                  renderRow={item =>
                    <ListItem button style={{paddingTop: 5, paddingBottom: 5}}>
                      <Left>
                        <Icon
                          active
                          name="checkmark"
                          style={styles.listitemLeftIcon}
                        />
                        <Text style={styles.listitemText}>
                          {item.categories}
                        </Text>
                      </Left>
                      <Right>
                        <Text style={styles.listitemText}>
                          {item.qty}
                        </Text>
                      </Right>
                    </ListItem>}
                />
              </View>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  listitemLeftIcon: {
    color: "#777",
    fontSize: Platform.OS === "android" ? 16 : 24,
    fontWeight: "300"
  },
  listitemText: {
    fontSize: 12,
    fontWeight: "300",
    color: "#777"
  }
});

export default Sizes;
