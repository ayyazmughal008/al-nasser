import React, {Component} from "react";
import {View, StyleSheet, Platform} from "react-native";
import {
  Text,
  Icon,
  Container,
  Content,
  List,
  ListItem,
  Left
} from "native-base";
import {Col, Grid} from "react-native-easy-grid";

class Fit extends Component {
  render() {
    var dataSaleThumb = [
      {
        id: 1,
        categories: "Regular Fit"
      },
      {
        id: 2,
        categories: "Slim fit"
      },
      {
        id: 3,
        categories: "Tailored"
      }
    ];

    return (
      <Container>
        <Content
          padder
          bounces={true}
          scrollEnabled={true}
          showsVerticalScrollIndicator={false}
          style={{padding: 0, marginBottom: 0}}
        >
          <Grid>
            <Col style={{backgroundColor: "#FFFFFF"}}>
              <View>
                <List
                  contentContainerStyle={{}}
                  dataArray={dataSaleThumb}
                  renderRow={item =>
                    <ListItem button style={{paddingTop: 5, paddingBottom: 5}}>
                      <Left>
                        <Icon
                          active
                          name="checkmark"
                          style={styles.listitemLeftIcon}
                        />
                        <Text style={styles.listitemText}>
                          {item.categories}
                        </Text>
                      </Left>
                    </ListItem>}
                />
              </View>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  listitemLeftIcon: {
    color: "#777",
    fontSize: Platform.OS === "android" ? 16 : 24,
    fontWeight: "300"
  },
  listitemText: {
    fontSize: 12,
    fontWeight: "300",
    color: "#777"
  }
});

export default Fit;
