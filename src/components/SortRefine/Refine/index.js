import React, { Component } from "react";
import {
  View,
  Dimensions,
  Modal,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity
} from "react-native";
import {
  Text,
  Icon,
  Button,
  Footer,
  Container,
  List,
  ListItem
} from "native-base";
import { Col, Grid } from "react-native-easy-grid";
import CommonRefine from "./CommonRefine.js";
import commonColor from "../../../theme/variables/commonColor.js";

var deviceWidth = Dimensions.get("window").width;
var deviceHeight = Dimensions.get("window").height;

class Refine extends Component {
  state = {
    component: "Brand",
    modalVisible: false,
    color: "#000"
  };

  currentChild = null;

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {
    return (
      <View style={styles.containerView}>
        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => null}
        >
          <View style={styles.modalHeaderView}>
            <View style={{ flexDirection: "row" }}>
              <Button
                transparent
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
              >
                <Icon style={{ color: "#555" }} name="ios-close" />
              </Button>

              <View style={styles.modalHeaderTitleView}>
                <Text style={styles.modalHeaderTitle}>FILTER BY</Text>
              </View>
            </View>

            <View style={{ flexDirection: "row" }}>
              <Button
                transparent
                style={styles.footerBtn}
                onPress={() => {
                  this.props.clearSavedData();
                  this.setModalVisible(!this.state.modalVisible);
                }}
              >
                <Text
                  style={{ fontSize: 12, fontWeight: "500", color: "#777" }}
                >
                  CLEAR ALL
                </Text>
              </Button>
              <Button
                transparent
                style={styles.footerBtn}
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                  this.props.hashesVariable(
                    "",
                    "",
                    this.currentChild.applyFunction()
                  );
                  // this.props.DATAToBeSave( returnData.stringTobeSend,returnData.CurrentRefine)
                }}
              >
                <Text style={{ fontSize: 12, fontWeight: "500" }}>APPLY</Text>
              </Button>
            </View>
          </View>

          <Grid>
            <Col size={1} style={styles.gridLeftCol}>
              <View>
                {this.props.filterVariable.map((item, index) =>
                  <View key={"sort" + index}>
                    <Button
                      transparent
                      block
                      style={{
                        backgroundColor:
                          this.state.component === item.description
                            ? "#C0C0C0"
                            : "#fff"
                      }}
                      onPress={() => {
                        this.setState({ component: item.description });
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 17,
                          fontWeight: "300",
                          color:
                            this.state.component === item.description
                              ? "#fff"
                              : "#000"
                        }}
                      >
                        {item.description}
                      </Text>
                    </Button>
                  </View>
                )}
              </View>
            </Col>
            <Col size={2} style={{ backgroundColor: "#FFFFFF" }}>
              <CommonRefine
                ref={child => {
                  this.currentChild = child;
                }}
                filterVariable={this.props.filterVariable}
                comp={this.state.component}
                DATAToBeSave={this.props.DATAToBeSave}
                RefineType={this.props.RefineType}
                RefineData={this.props.RefineData}
              />
            </Col>
          </Grid>
        </Modal>

        <View style={styles.modalInitView}>
          <Button
            transparent
            onPress={() => {
              this.setModalVisible(true);
            }}
          >
            <Icon style={{ color: "#555" }} name="ios-arrow-down" />
            <Text style={styles.modalInitBtnText}>REFINE</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerView: {
    height: "100%",
    width: "49%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  modalHeaderView: {
    justifyContent: "space-between",
    flexDirection: "row",
    backgroundColor: "#F9F9F9",
    // borderBottomColor: "#aaa",
    // borderBottomWidth: 0.5,
    paddingTop: 12
  },
  modalHeaderIcon: {
    color: "#000",
    fontSize: 15,
    marginLeft: 0,
    marginBottom: -3,
    justifyContent: "center"
  },
  modalHeaderTitleView: {
    justifyContent: "center"
  },
  modalHeaderTitle: {
    fontSize: 14,
    fontWeight: "300",
    color: "#777",
    textAlign: "center"
  },
  gridLeftCol: {
    backgroundColor: "#fff",
    borderRightColor: "#C0C0C0",
    // borderRightWidth: 1
  },
  footer: {
    shadowColor: "#999",
    shadowRadius: 1,
    shadowOpacity: 0.2
  },
  footerBtn: {
    alignSelf: "center",
    justifyContent: "center"
  },
  modalInitView: {
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  modalInitBtnText: {
    color: "#555",
    fontWeight: "300",
    marginLeft: 8,
    fontSize: 12
  }
});

export default Refine;
