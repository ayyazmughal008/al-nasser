import React, { Component } from "react";
import { View, Dimensions, Platform, StyleSheet, Switch } from "react-native";
//import { connect } from "react-redux";
import {
  Text,
  Icon,
  Item,
  Input,
  Container,
  Content,
  List,
  ListItem,
  Left,
  Right,
  CheckBox
} from "native-base";

import { Col, Grid } from "react-native-easy-grid";
//import { ApplyRefine } from "../../../redux/actions";
/*DATAToBeSave={this.props.DATAToBeSave}
DataTobeClear={this.props.clearSavedData}
RefineType={this.props.RefineType}
RefineData={this.props.RefineData}*/
var deviceWidth = Dimensions.get("window").width;
const myId = 13;

class Brand extends Component {
  constructor() {
    super();
    this.state = {
      switch1Array: [],
      ready: false
    };
  }

  componentWillMount() {
    let myTempArray = [];
    const { filterVariable, RefineType, RefineData } = this.props;
    // const ThisOne = RefineType === "Brand" ? true : false;
   // alert(RefineData + " " + RefineType);
    const RefineDataArray = RefineData.split("-");
    let i = 1,
      Leng = RefineDataArray.length;
    let RefineDataArraywithoutFirstMem = [];
    if (Leng > 1)
      for (i; i < Leng; i++) {
        RefineDataArraywithoutFirstMem.push(RefineDataArray[i]);
      }
    filterVariable.map((item, index) => {
      if (item.filter_id == myId)
        item.variants.map((myItem, myIndex) => {
          if (
            RefineDataArraywithoutFirstMem.indexOf("" + myItem.variant_id) !=
              -1 &&
            RefineType == "Brand"
          ) {
            myTempArray.push({ itemId: myItem.variant_id, switchState: true });
          } else {
            myTempArray.push({ itemId: myItem.variant_id, switchState: false });
          }
        });
    });
    this.setState({ switch1Array: myTempArray, ready: true });
  }

  toggleSwitch1 = value => {
    this.setState({ switch1Value: value });
    console.log("Switch 1 is: " + value);
  };

  myApplyFunction = () => {
    let stringTobeSend = "";
    this.state.switch1Array.map((item, index) => {
    if (item.switchState) stringTobeSend = stringTobeSend + "-" + item.itemId;
    });
    if (stringTobeSend) {
    stringTobeSend = myId + stringTobeSend;
    }
    this.props.DATAToBeSave(stringTobeSend, "Brand");
    return stringTobeSend;
    };

  render() {
    //const { check } = this.state;
    var hash = ["13"];
    return (
      <Container>
        <Content
          padder
          bounces={true}
          scrollEnabled={true}
          showsVerticalScrollIndicator={false}
          style={{ padding: 0, marginBottom: 0,backgroundColor:"#C0C0C0" }}
        >
          <Grid>
            <Col>
              <View>
                {this.state.ready &&
                  this.props.filterVariable.map((item, index) => {
                    if (item.filter_id == myId)
                      return item.variants.map((myItem, myIndex) =>
                        <View
                          style={{
                            paddingTop: 5,
                            paddingBottom: 5,
                            flexDirection: "row"
                          }}
                          key={"sort" + myIndex}
                        >
                          <Switch
                            onValueChange={value => {
                              let { switch1Array } = this.state;
                              switch1Array[myIndex].switchState = value;
                              this.setState({ switch1Array });
                            }}
                            value={this.state.switch1Array[myIndex].switchState}
                          />
                          <Text style={styles.listitemText}>
                            {myItem.variant}
                          </Text>
                          {/* </Button> */}
                        </View>
                      );
                  })}
              </View>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  searchItem: {
    borderColor: "transparent",
    borderRadius: 5,
    backgroundColor: "#eee",
    width: deviceWidth * (3 / 4) - 50,
    height: 30,
    margin: 10,
    marginLeft: 8
  },
  searchItemIcon: {
    fontSize: 20,
    color: "#777",
    marginLeft: 10,
    marginTop: 2
  },
  listitemLeftIcon: {
    color: "#777",
    fontSize: Platform.OS === "android" ? 16 : 24,
    fontWeight: "300"
  },
  listitemText: {
    fontSize: 15,
    fontWeight: "300",
    color: "#fff",
    marginLeft: 15,
    marginTop:5
  }
});

// const mapStateToProps = state => ({
//   RefineType: state.refiner.RefineType,
//   RefineData: state.refiner.RefineData
// });

export default Brand;
