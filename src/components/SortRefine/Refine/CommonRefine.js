import React, { Component } from "react";
import Brand from "./Brand.js";
import Color from "./Color.js";
import Price from "./Price";
import Discount from "./Discount";
import Sizes from "./Sizes";
import Pattern from "./Pattern";
import Collar from "./Collar";
import Fabric from "./Fabric";
import Occasion from "./Occasion";
import Fit from "./Fit";
import SetContents from "./SetContents";
import Categories from "./Categories";

class CommonRefine extends Component {
  currentChild = null;
  applyFunction = () => {
    return this.currentChild.myApplyFunction();
  };
  render() {
    switch (this.props.comp) {
      case "Categories":
        return (
          <Categories
            filterVariable={this.props.filterVariable}
            ref={child => {
              this.currentChild = child;
            }}
            DATAToBeSave={this.props.DATAToBeSave}
            RefineType={this.props.RefineType}
            RefineData={this.props.RefineData}
          />
        );
      case "Brand":
        return (
          <Brand
            filterVariable={this.props.filterVariable}
            ref={child => {
              this.currentChild = child;
            }}
            DATAToBeSave={this.props.DATAToBeSave}
            RefineType={this.props.RefineType}
            RefineData={this.props.RefineData}
          />
        );
      case "Color":
        return <Color />;
      case "Price":
        return (
          <Price
            filterVariable={this.props.filterVariable}
            ref={child => {
              this.currentChild = child;
            }}
            DATAToBeSave={this.props.DATAToBeSave}
            RefineType={this.props.RefineType}
            RefineData={this.props.RefineData}
          />
        );
      case "Discount":
        return (
          <Discount
            filterVariable={this.props.filterVariable}
            ref={child => {
              this.currentChild = child;
            }}
            DATAToBeSave={this.props.DATAToBeSave}
            RefineType={this.props.RefineType}
            RefineData={this.props.RefineData}
          />
        );
      case "Sizes":
        return <Sizes />;
      case "Pattern":
        return <Pattern />;
      case "Collar":
        return <Collar />;
      case "Fabric":
        return <Fabric />;
      case "Occasion":
        return <Occasion />;
      case "Fit":
        return <Fit />;
      case "Set Contents":
        return <SetContents />;

      default:
        return <Brand />;
    }
  }
}

export default CommonRefine;
