import React, {Component} from "react";
import {View, StyleSheet, Platform} from "react-native";
import {
  Text,
  Icon,
  Container,
  Content,
  List,
  ListItem,
  Left
} from "native-base";
import {Col, Grid} from "react-native-easy-grid";

class Color extends Component {
  render() {
    var dataSaleThumb = [
      {
        id: 1,
        categories: "Navy",
        color: "#000080"
      },
      {
        id: 2,
        categories: "Navy blue",
        color: "#221188"
      },
      {
        id: 3,
        categories: "Blue",
        color: "#0000ff"
      },
      {
        id: 4,
        categories: "Black",
        color: "#000000"
      },
      {
        id: 5,
        categories: "Charcoal",
        color: "#36454f"
      },
      {
        id: 6,
        categories: "Grey",
        color: "#808080"
      },
      {
        id: 7,
        categories: "Green",
        color: "#008000"
      },
      {
        id: 8,
        categories: "Beige",
        color: "#f5f5dc"
      },
      {
        id: 9,
        categories: "Cream",
        color: "#ffffcc"
      },
      {
        id: 10,
        categories: "Brown",
        color: "#a52a2a"
      },
      {
        id: 11,
        categories: "White",
        color: "#ffffff"
      },
      {
        id: 12,
        categories: "Silver",
        color: "#c0c0c0"
      },
      {
        id: 13,
        categories: "Off White",
        color: "#faebd7"
      }
    ];

    return (
      <Container>
        <Content
          padder
          bounces={true}
          scrollEnabled={true}
          showsVerticalScrollIndicator={false}
          style={{padding: 0, marginBottom: 0}}
        >
          <Grid>
            <Col style={{backgroundColor: "#FFFFFF"}}>
              <View>
                <List
                  contentContainerStyle={{}}
                  dataArray={dataSaleThumb}
                  renderRow={item =>
                    <ListItem button style={{paddingTop: 8, paddingBottom: 8}}>
                      <Left>
                        <Icon
                          active
                          name="checkmark"
                          style={styles.listitemLeftIcon}
                        />
                        <View
                          style={{
                            marginLeft: 20,
                            marginTop: 5,
                            width: 50,
                            height: 20,
                            backgroundColor: item.color
                          }}
                        />
                        <Text style={styles.listitemText}>
                          {item.categories}
                        </Text>
                      </Left>
                    </ListItem>}
                />
              </View>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  listitemLeftIcon: {
    color: "#777",
    fontSize: Platform.OS === "android" ? 16 : 24,
    fontWeight: "300"
  },
  listitemText: {
    fontSize: 12,
    fontWeight: "300",
    color: "#777"
  }
});

export default Color;
