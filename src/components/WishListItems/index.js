import React, { Component } from "react";
import { View, TouchableHighlight } from "react-native";
import {
  Card,
  CardItem,
  Thumbnail,
  Grid,
  Col,
  Button,
  Text,
  Icon,
  Right
} from "native-base";
// import { inject } from "mobx-react/native";
import IconMI from "react-native-vector-icons/MaterialIcons";

// @inject("view.app", "domain.user", "app", "routerActions")
class WishListItem extends Component {
  render() {
    //console.log('ImageSource,',this.props.imageSource)
    const navigate = this.props.navigattion;
    return (
      <Card style={{ flex: 1, marginRight: 10, marginLeft: 10 }}>
        <TouchableHighlight onPress={() => this.clickHandler}>
          <CardItem>
            <Grid>
              <Col size={1}>
                <Thumbnail
                  style={{
                    resizeMode: "cover",
                    marginTop: 5,
                    height: 60,
                    width: 60
                  }}
                  square
                  source={{ uri: this.props.imageSource }}
                />
              </Col>
              <Col size={2} style={{ marginLeft: 10 }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: "300",
                    color: "#333"
                  }}
                  numberOfLines={2}
                  ellipsizeMode="tail"
                >
                  {this.props.product}
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: "300",
                    color: "#333"
                  }}
                >
                  {this.props.detail}
                </Text>
              </Col>

              <Col size={1} style={{ marginLeft: 20, marginBottom: 20 }}>
                <Button
                  transparent
                  //onPress={() => }
                >
                  <Icon name="ios-close" />
                </Button>
              </Col>
            </Grid>
          </CardItem>
        </TouchableHighlight>
      </Card>
    );
  }
}
export default WishListItem;
