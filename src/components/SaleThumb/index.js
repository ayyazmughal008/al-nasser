import React, { Component } from "react";
import { View, Image, Dimensions, StyleSheet } from "react-native";
import { Text, Card, CardItem } from "native-base";
// import { inject } from "mobx-react/native";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
var thumbWidth = deviceWidth / 3 - 10;

// @inject("routerActions")
class SaleThumb extends Component {
  render() {
    const navigation = this.props.navigation;
    return (
      <View
        style={{ height: deviceHeight / 4 - 15, width: deviceWidth / 3 - 10 }}
      >
        <Card style={{ borderColor: "transparent", shadowOpacity: 0 }}>
          <CardItem
            button
            onPress={() => navigation.navigate("ProductList")}
            style={styles.carditem}
          >
            <Image
              style={{ width: thumbWidth, height: deviceHeight / 6 }}
              source={this.props.imageSource}
            />
            <Text style={styles.carditemText}>
              {this.props.saleData}
            </Text>
          </CardItem>
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  carditem: {
    alignSelf: "center",
    flex: 1,
    flexDirection: "column",
    width: thumbWidth,
    borderWidth: 0.5,
    borderColor: "#F3F3F3",
    shadowOpacity: 0,
    paddingTop: 0
  },
  carditemText: {
    textAlign: "center",
    fontSize: 14,
    fontWeight: "500",
    marginTop: 5,
    marginBottom: 5,
    width: thumbWidth
  }
});
export default SaleThumb;
