import React from "react";
import {
  Text,
  View,
  ScrollView,
  ImageBackground,
  TouchableHighlight,
  Linking,
  Share,
  Alert,
  Platform
} from "react-native";
import { Constants } from "expo";
import { DrawerItems } from "react-navigation";
import { Logout } from "../../redux/actions";
import { connect } from "react-redux";
import email from "react-native-email";
class DrawerContent extends React.Component {
  render() {
    let { props } = this;
    const { id, sdkVersion, version } = Constants.manifest;
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
        <View>
          <ImageBackground
            source={require("../../../assets/nav_header.png")}
            style={{
              height: 150,
              justifyContent: "flex-start",
              alignItems: "flex-start"
            }}
          >
            {/* <Image
          source={require("../../../assets/sanket.png")}
          style={{
            height: 80,
            width: 80,
            borderRadius: 60,
            marginTop: 10,
            marginLeft: 10
          }}
        /> */}
          </ImageBackground>
          <DrawerItems {...props} />
          <TouchableHighlight
            // onPress={() =>
            //   props.navigation.navigate("Category")
            // }
            onPress={() =>
              email("shahzad@clicky.pk", {
                // Optional additional arguments
                // cc: ["bazzy@moo.com", "doooo@daaa.com"], // string or array of email addresses
                // bcc: "mee@mee.com", // string or array of email addresses
                subject: "From Clicky.pk app",
                body: "Send feedback"
              }).catch(console.error)}
          >
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  marginLeft: 70,
                  marginTop: 8,
                  fontSize: 14,
                  fontWeight: "700"
                }}
              >
                Send Feedbacks
              </Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            // onPress={() =>
            //   props.navigation.navigate("Category")
            // }
            style={{ marginTop: 17 }}
            onPress={() => {
              Platform.OS === "ios"
                ? Linking.openURL(
                    `https://itunes.apple.com/sa/app/clicky-online-shopping/id1448160644?mt=8`
                  )
                : Linking.openURL(
                    `https://play.google.com/store/apps/details?id=com.clicky.pk`
                  );
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  marginLeft: 70,
                  marginTop: 8,
                  fontSize: 14,
                  fontWeight: "700"
                }}
              >
                Rate Us
              </Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            // onPress={() =>
            //   props.navigation.navigate("Category")
            // }
            style={{ marginTop: 17, marginBottom: 5 }}
            onPress={() => {
              Platform.OS === "ios"
                ? Share.share({
                    message:
                      "https://itunes.apple.com/sa/app/clicky-online-shopping/id1448160644?mt=8"
                  })
                : Share.share({
                    message:
                      "https://play.google.com/store/apps/details?id=com.clicky.pk&hl=en"
                  });
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  marginLeft: 70,
                  marginTop: 8,
                  fontSize: 14,
                  fontWeight: "700"
                }}
              >
                Share Us
              </Text>
            </View>
          </TouchableHighlight>
          {this.props.isLoggedIn &&
            <TouchableHighlight
              // onPress={() =>
              //   props.navigation.navigate("Category")
              // }
              style={{ marginTop: 17, marginBottom: 5 }}
              onPress={() => {
                // Logout();
                props.navigation.navigate("DrawerClose");
                Alert.alert(
                  "Logout Alert",
                  "Are you sure you want to logout?",
                  [
                    {
                      text: "Cancel",
                      onPress: () => console.log("Cancel Pressed"),
                      style: "cancel"
                    },
                    {
                      text: "OK",
                      onPress: () => {
                        Logout();
                        props.navigation.navigate("HomePage");
                      }
                    }
                  ],
                  { cancelable: false }
                );
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Text
                  style={{
                    marginLeft: 70,

                    fontSize: 14,
                    fontWeight: "700"
                  }}
                >
                  Log Out
                </Text>
              </View>
            </TouchableHighlight>}

          <View style={{ flexDirection: "row" }}>
            <Text
              style={{
                marginLeft: 70,
                marginTop: 8,
                fontSize: 14,
                fontWeight: "700",
                marginBottom: 8
              }}
            >
              Current Version {version}
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}
const mapStateToProps = state => ({
  isLoggedIn: state.user.isLoggedIn
});

export default connect(mapStateToProps)(DrawerContent);
