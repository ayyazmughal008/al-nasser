import React from "react";
import { Image } from "react-native";
import { Icon } from "native-base";

import { DrawerNavigator } from "react-navigation";
import MyTabNavigator from "../../../src/TabNavigator";
// import SaveAddress from "../../screens/SaveAddress";
// import ConfirmAddress from "../../screens/ConfirmAddress";
// import PaymentOptions from "../../screens/PaymentOptions";
// import ProductList from "../../screens/ProductList";
// import ProductPage from "../../screens/ProductPage";
// import Login from "../../screens/Login";
// import LoginHome from "../../screens/LoginHome";
// import SignUp from "../../screens/SignUp";
import Profile from "../../screens/Profile";
// import Shop from "../../screens/Shop";
// import Category from "../../screens/Category";
// import Product from "../../screens/Product";
//import PaymentSuccess from "../../screens/PaymentSuccess";
import Orders from "../../screens/Orders";
 import EasyPay from "../../screens/EasyPay";
// import UpdateAddress from "../../screens/UpdateAddress";
import WishList from "../../screens/WishList";
import Notifications from "../../screens/Notifications";
import CategoryPage from "../../screens/CategoryPage";
import DrawerContent from './DrawerContent'
const Drawer = DrawerNavigator(
  {
    MyTabNavigator: {
      screen: MyTabNavigator,
      navigationOptions: {
        drawerLabel: "Home",
        drawerIcon: <Image
        source={require("../../../assets/nav_home.png")}
        resizeMode = "contain"
        style={{height:33,width:33}}
      />,
        headerStyle: {
          backgroundColor: "transparent"
        },
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff"
        },
        headerTintColor: "#fff",
        animationEnabled: true,
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff",
          zIndex: 1,
          fontSize: 18,
          lineHeight: 23,
          fontFamily: "CircularStd-Bold"
        }
      }
    },

    CategoryPage: {
      screen: CategoryPage,
      navigationOptions: {
        drawerLabel: "Category",
        drawerIcon: <Icon name={"ios-list"} />,
        headerStyle: {
          backgroundColor: "transparent"
        },
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff"
        },
        headerTintColor: "#fff",
        animationEnabled: true,
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff",
          zIndex: 1,
          fontSize: 18,
          lineHeight: 23,
          fontFamily: "CircularStd-Bold"
        }
      }
    },

    WishList: {
      screen: WishList,
      navigationOptions: {
        drawerLabel: "Wish List",
        drawerIcon: (
          <Image
            source={require("../../../assets/navwishlist.png")}
            style={{
              height: 40,
              width: 40
            }}
          />
        ),
        headerStyle: {
          backgroundColor: "transparent"
        },
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff"
        },
        headerTintColor: "#fff",
        animationEnabled: true,
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff",
          zIndex: 1,
          fontSize: 18,
          lineHeight: 23,
          fontFamily: "CircularStd-Bold"
        }
      }
    },

    Orders: {
      screen: Orders,
      navigationOptions: {
        drawerLabel: "My Orders",
        drawerIcon: (
          <Image
            source={require("../../../assets/nav_orders.png")}
            style={{
              height: 40,
              width: 40
            }}
          />
        ),
        headerStyle: {
          backgroundColor: "transparent"
        },
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff"
        },
        headerTintColor: "#fff",
        animationEnabled: true,
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff",
          zIndex: 1,
          fontSize: 18,
          lineHeight: 23,
          fontFamily: "CircularStd-Bold"
        }
      }
    },

    Notifications: {
      screen: Notifications,
      navigationOptions: {
        drawerLabel: "Notifications",
        drawerIcon: (
          <Image
            source={require("../../../assets/nav_notifications.png")}
            style={{
              height: 40,
              width: 40
            }}
          />
        ),
        headerStyle: {
          backgroundColor: "transparent"
        },
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff"
        },
        headerTintColor: "#fff",
        animationEnabled: true,
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff",
          zIndex: 1,
          fontSize: 18,
          lineHeight: 23,
          fontFamily: "CircularStd-Bold"
        }
      }
    },

    Profile: {
      screen: Profile,
      navigationOptions: {
        drawerLabel: "My Account",
        drawerIcon: (
          <Image
            source={require("../../../assets/nav_my_account.png")}
            style={{
              height: 40,
              width: 40
            }}
          />
        ),
        headerStyle: {
          backgroundColor: "transparent"
        },
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff"
        },
        headerTintColor: "#fff",
        animationEnabled: true,
        headerTitleStyle: {
          fontWeight: "bold",
          color: "#fff",
          zIndex: 1,
          fontSize: 18,
          lineHeight: 23,
          fontFamily: "CircularStd-Bold"
        }
      }
    }
    // ,EasyPay : {screen:EasyPay}
  },
  {
    drawerWidth: 250,
    drawerPosition: "left",
    initialRouteName: "MyTabNavigator",
   // contentComponent: DrawerContent
    contentComponent: props => <DrawerContent {...props} />
  }
);

export default Drawer;
