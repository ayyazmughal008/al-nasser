const commonColor = require("../../theme/variables/commonColor");
export default {
  itemDesc: {
    fontSize: 14,
    fontWeight: "300",
    color: "#333"
  },
  soldBy: {
    fontSize: 13,
    fontWeight: "300",
    color: "#888"
  },
  inStock: {
    fontSize: 12,
    fontWeight: "300",
    color: "#f9db2f"
  },
  price: {
    fontSize: 14,
    fontWeight: "500",
    color: "#555",
  },
  discountedText: {
    fontSize: 14,
    fontWeight: "300",
    color: commonColor.brandPrimary,
    marginLeft: 2
  },
  bagCardItem: {
    justifyContent: "space-around",
    borderTopWidth: 0.5,
    borderTopColor: "#ccc",
    marginBottom: 0,
    padding: 0,
    borderBottomWidth: 0
  },
  bagItemButtons: {
    justifyContent: "center",
    flex: 1,
    marginTop: 8,
    marginBottom:8
  },
  bagItemButtonText: {
    textAlign: "center",
    fontSize: 12,
    fontWeight: "500",
    color: "#777"
  },
  btnDivider: {
    height: 30,
    width: 0.5,
    backgroundColor: "#ccc",
    marginTop: 12,
    marginBottom: 12
  }
};
