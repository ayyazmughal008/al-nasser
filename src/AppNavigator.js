import React from "react";
import { Provider } from "react-redux";
import {Store, persistor } from "./redux/store";
import { PersistGate } from "redux-persist/integration/react";
//import store from "./redux/store";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { Root } from "native-base";
import MyTabNavigator from "./TabNavigator";
import SaveAddress from "./screens/SaveAddress";
import ConfirmAddress from "./screens/ConfirmAddress";
import PaymentOptions from "./screens/PaymentOptions";
import ProductList from "./screens/ProductList";
import ProductPage from "./screens/ProductPage";
import Login from "./screens/Login";
import Orders from "./screens/Orders";
import Address from "./screens/Address";
import LoginHome from "./screens/LoginHome";
import SignUp from "./screens/SignUp";
import Profile from "./screens/Profile";
import Shop from "./screens/Shop";
import Category from "./screens/Category";
import Product from "./screens/Product";
import NavigationService from "./NavigationService";
import Drawer from "./components/drawer";
import { SideBar } from "./components/Slider/SideBar";
import HomePage from "./../src/screens/HomePage/index";
import CashonDelivery from "./../src/screens/CashonDelivery";
import EasyPaisaWallet from "./../src/screens/EasyPaisaWallet";
import PaymentSuccess from "./../src/screens/PaymentSuccess";
import SearchProducts from "./../src/screens/SearchProducts";
import WishList from "./../src/screens/WishList";
import EasyPay from "./../src/screens/EasyPay";
import UpdateAddress from "./../src/screens/UpdateAddress";
import OrderDetail from "./../src/screens/OrderDetail";
import CategoryPage from "./../src/screens/CategoryPage";
import Register from "./../src/screens/resetpassword";
//App
const TopLevelNavigator = StackNavigator(
  {
    Drawer: { screen: Drawer },
    MyTabNavigator: { screen: MyTabNavigator },
    Orders: { screen: Orders },
    Address: { screen: Address },
    SaveAddress: { screen: SaveAddress },
    ConfirmAddress: { screen: ConfirmAddress },
    SaveAddress: { screen: SaveAddress },
    // ConfirmAddress: { screen: ConfirmAddress },
    PaymentOptions: { screen: PaymentOptions },
    ProductList: { screen: ProductList },
    ProductPage: { screen: ProductPage },
    Login: { screen: Login },
    LoginHome: { screen: LoginHome },
    CashonDelivery: { screen: CashonDelivery },
    EasyPaisaWallet: { screen: EasyPaisaWallet },
    PaymentSuccess: { screen: PaymentSuccess },
    SearchProducts: { screen: SearchProducts },
    HomePage: { screen: HomePage },
    SignUp: { screen: SignUp },
    Profile: { screen: Profile },
    Shop: { screen: Shop },
    Category: { screen: Category },
    Product: { screen: Product },
    WishList: { screen: WishList },
    EasyPay: { screen: EasyPay },
    UpdateAddress: { screen: UpdateAddress },
    Register: { screen: Register },
    OrderDetail: { screen: OrderDetail },
    CategoryPage: { screen: CategoryPage }
  },
  {
    index: 0,
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);

export default () =>
  <Provider store={Store}>
    <PersistGate loading={null} persistor={persistor}>
      <Root>
        <TopLevelNavigator
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Root>
    </PersistGate>
  </Provider>
