# [3.1.0](http://gitstrap.com/strapmobile/EcommercePro/tags/v3.1.0)


#### Upgraded Features

* Upgraded [React Native](https://github.com/facebook/react-native) from 0.42.3 to 0.52.0
* Upgraded Native Base from 2.1.0-rc.2 to 2.3.10
* Upgraded React from 15.4.0 to 16.2.0
* Upgraded React Navigation from 1.0.0-beta.11 to 1.2.1
* Upgraded Expo from 15.1.0 to 25.0.0